//Version 0.20
//main.cpp

#include "Header.h"
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace std;

#define PORT 12345
#define BUFFER_SIZE 128

int main() {
    int sockfd;
    char buffer[BUFFER_SIZE];
    struct sockaddr_in serverAddr, clientAddr;
    socklen_t addrLen = sizeof(clientAddr);

    // Create UDP socket
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket creation failed");
        return 1;
    }

    // Initialize server address structure
    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddr.sin_port = htons(PORT);

    // Bind socket to address
    if (bind(sockfd, (const struct sockaddr*)&serverAddr, sizeof(serverAddr)) < 0) {
        perror("bind failed");
        return 1;
    }

    std::cout << "UDP Server running on port " << PORT << std::endl;

    // Infinite loop to receive messages
    while (true) {
        int recvLen = recvfrom(sockfd, buffer, BUFFER_SIZE, 0, (struct sockaddr*)&clientAddr, &addrLen);
        if (recvLen < 0) {
            perror("recvfrom failed");
            continue;
        }

        // Instead of relying on null-termination, we process the buffer directly using recvLen
        std::cout << "Received " << recvLen << " bytes from " << inet_ntoa(clientAddr.sin_addr) << ": ";
        for (int i = 0; i < recvLen; ++i) {
            std::cout << std::hex << static_cast<int>(buffer[i] & 0xFF) << " ";
        }
        std::cout << std::dec << std::endl; // Switch back to decimal formatting

        // Reply back to the client with the same data received (optional)
        sendto(sockfd, buffer, recvLen, 0, (const struct sockaddr*)&clientAddr, addrLen);
        std::cout << "Sent ACK message to " << inet_ntoa(clientAddr.sin_addr) << std::endl;

        // Process the received message here...

    }

    // Close the socket
    close(sockfd);

    return 0;
}

/*
    // Specify the file name
    const char* fileName = "ReceivedCommands.txt";

    // Create an ofstream object and open the file
    std::ofstream file(fileName);
    // Check if the file is successfully opened
    if (file.is_open())
    {
        // File is successfully created or cleared
        std::cout << "File is created or cleared successfully: " << fileName << std::endl;

        // Close the file
        file.close();
    }
    else
    {
        std::cerr << "Failed to open or create the file: " << fileName << std::endl;
    }


            // Create an fstream object and open the file without truncating
        std::fstream file(fileName, std::ios::in | std::ios::out | std::ios::app);

        // Check if the file is successfully opened
        if (file.is_open()) {
            // File is successfully opened without erasing its contents
            std::cout << "File is opened successfully without erasing its contents." << std::endl;

            // Optionally, you can write something to the file here
            file << buffer << std::endl;

            // Close the file
            file.close();
        }
        else {
            std::cerr << "Failed to open the file." << std::endl;
        }

*/