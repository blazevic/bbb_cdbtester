//Version 0.20
//Timer.cpp

#include <iostream>
#include <chrono>
#include <thread>

using namespace std;

int timer() 
{
    // Start the timer
    auto start = std::chrono::high_resolution_clock::now();

    // Perform some operations or wait for a certain duration
    std::this_thread::sleep_for(std::chrono::seconds(3)); // Sleep for 3 seconds as an example

    // Stop the timer
    auto end = std::chrono::high_resolution_clock::now();

    // Calculate the elapsed time
    std::chrono::duration<double> elapsed = end - start;

    // Print the elapsed time
    std::cout << "Elapsed time: " << elapsed.count() << " seconds" << std::endl;

    return 0;
}