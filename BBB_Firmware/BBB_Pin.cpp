//Version 0.20
//BBB_Pin.cpp

#include "BBB_Pin.h"
#include <string>
#include <iostream>
#include <fstream>
#include <unistd.h>

using namespace std;

BBB_Pin::BBB_Pin(int GPIOnr) : _GPIOnr(GPIOnr)
{
    _OutValuePath = (_GPIO_PATH + "gpio" + to_string(_GPIOnr) + "/value");
}

BBB_Pin::~BBB_Pin()
{
}

// Method to Export GPIO pins
void BBB_Pin::BBB_PinExport()
{
    ofstream ExportFile(_GPIO_PATH + "export");
    ExportFile << to_string(_GPIOnr);
    ExportFile.close();
}

// Method to UnExport GPIO pin
void BBB_Pin::BBB_PinUnExport()
{
    ofstream UnexportFile(_GPIO_PATH + "unexport");
    UnexportFile << to_string(_GPIOnr);
    UnexportFile.close();
}

// Method to set pin as Output
void BBB_Pin::BBB_PinSetAsOut()
{
    ofstream DirectionFile(_GPIO_PATH + "gpio" + to_string(_GPIOnr) + "/direction");
    DirectionFile << "out";
    DirectionFile.close();
}

// Method to set pin as Input
void BBB_Pin::BBB_PinSetAsIn()
{
    ofstream DirectionFile(_GPIO_PATH + "gpio" + to_string(_GPIOnr) + "/direction");
    DirectionFile << "in";
    DirectionFile.close();
}

// Method to write value to GPIO pin
void BBB_Pin::BBB_PinSetOutVal(int OutVal)
{
    _OutVal = OutVal;
    _OutValueFile.open(_OutValuePath);
    _OutValueFile << to_string(_OutVal);
    _OutValueFile.close();
}