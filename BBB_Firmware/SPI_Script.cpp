//Version 0.20

/*
#!/bin/bash

# Configure SPI pins for spi0
config-pin P9.17 spi_cs
config-pin P9.18 spi
config-pin P9.21 spi
config-pin P9.22 spi_sclk

# Configure SPI pins for spi1
config-pin P9.28 spi_cs
config-pin P9.29 spi
config-pin P9.30 spi
config-pin P9.31 spi_sclk

# Load spidev module
sudo modprobe spidev

exit 0
*/