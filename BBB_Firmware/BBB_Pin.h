//Version 0.20
//BBB_Pin.h

#ifndef BBB_Pin_H_INCLUDED
#define BBB_Pin_H_INCLUDED

#include <string>
#include <fstream>
#include <iostream>

class BBB_Pin
{
private:
    int _GPIOnr;
    std::string _GPIO_PATH = "/sys/class/gpio/";
    std::string _OutValuePath;
    std::ofstream _OutValueFile;

public:
    int _OutVal = 0;

public:
    BBB_Pin(int GPIOnr);

    ~BBB_Pin();

    void BBB_PinExport();

    void BBB_PinUnExport();

    void BBB_PinSetAsOut();

    void BBB_PinSetAsIn();

    void BBB_PinSetOutVal(int OutVal);

};
#endif