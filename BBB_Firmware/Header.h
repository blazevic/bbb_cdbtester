//Version 0.20
//Header.h
#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

#include <string>

// Define the BBB pin numbers
enum BBBGPIOnr
{
    V24_pin = 45, // GPIO45
    V15_pin = 68,
    V24V2_pin = 69,
    CTRL_ON_pin = 66,
    INTF_ON_pin = 67,
    MUX1_pin = 65,
    MUX2_pin = 22,
    MeasA0_pin = 47,
    MeasA1_pin = 46,
    MeasA2_pin = 27,
    MCP_Reset0_pin = 51,
    MCP_Reset1_pin = 115,
};

enum SPI
{
    SPI_buff_size = 3,
};

//Define 8 MCP RAW addresses of 7 bits.
const uint8_t ReadAddrMCP[8] = { 0x41,  0x43, 0x45, 0x47, 0x49, 0x4B, 0x4D, 0x4F };
const uint8_t WriteAddrMCP[8] = { 0x40,  0x42, 0x44, 0x46, 0x48, 0x4A, 0x4C, 0x4E };


// Define Debian SPI device folder
const char* SPI0 = "/dev/spidev0.0";
const char* SPI1 = "/dev/spidev1.0";

#endif