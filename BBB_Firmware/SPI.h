//Version 0.20
//SPI.h

#ifndef SPI_H_INCLUDED
#define SPI_H_INCLUDED

#include <cstdint>
#include <string>

class SPI_Device
{

private:
    std::string _device;
    uint32_t _speed;
    int _spi_fd;

public:
    SPI_Device(const std::string& device, uint32_t speed);
    ~SPI_Device();

    int initSPI();
    int transfer(uint8_t *txBuffer, uint8_t *rxBuffer, size_t length);
    void closeSPI();
};
#endif