//Version 0.20
//ReadADC.cpp

#include "ReadADC.h"
#include "data_m_structure.h"
#include <iostream>
#include <unistd.h>
#include <fstream>
#include <ostream>
#include <string>


using namespace std;

ReadADC::ReadADC(int ch) : _ch(ch)
{
    _filePath = "/sys/bus/iio/devices/iio:device0/in_voltage" + std::to_string(_ch) + "_raw";
};

ReadADC::~ReadADC()
{
    CloseFile();
}

void ReadADC::OpenFile()
{
    if (_adcFile.is_open())
    {
        _adcFile.close();
    }
    _adcFile.open(_filePath);
    if (!_adcFile.is_open())
    {
        std::cerr << "Failed to open ADC file" << std::endl;
    }
}

void ReadADC::CloseFile()
{
    if (_adcFile.is_open())
    {
        _adcFile.close();
    }
}

unsigned int ReadADC::ReadVal()
{
    _iAdcVal = -1;
    OpenFile();
    if (_adcFile.is_open())
    {
        _adcFile >> _sAdcVal;
    }
    else
    {
        std::cerr << "ADC file not open" << std::endl;
        return -2;
    }
    CloseFile();
    _iAdcVal = std::stoi(_sAdcVal);
    if (_iAdcVal >= 0)
    {
        _uAdcVal = _iAdcVal;
        //std::cout << "String, Int, Unsigned " << _sAdcVal << ", " << _iAdcVal << ", " << _uAdcVal << std::endl;

        return _uAdcVal;
    }
    else
    {
        std::cerr << "Failed to read ADC value" << std::endl;
        return -1;
    }
}

int ReadADC::SetMux(uint8_t i, bool EN1, bool EN2, BBB_Pin& MUX1, BBB_Pin& MUX2, BBB_Pin& MeasA0, BBB_Pin& MeasA1, BBB_Pin& MeasA2)
{
    int _A0 = 0;
    int _A1 = 0;
    int _A2 = 0;
    uint8_t _MUXaddrINT = i;
    bool _MuxEN1 = 0;
    bool _MuxEN2 = 0;
    /*
    MeasA0.BBB_PinSetOutVal(_A0);
    MeasA1.BBB_PinSetOutVal(_A1);
    MeasA2.BBB_PinSetOutVal(_A2);
    MUX1.BBB_PinSetOutVal(_MuxEN1);
    MUX2.BBB_PinSetOutVal(_MuxEN2);
    */
    usleep(1000); //1ms

    if (_MUXaddrINT < 0 || _MUXaddrINT > 7)
    {
        std::cerr << "Error: MUX addr must be between 0 and 7 !\n";
        return -1;
    }
    else
    {
        _A0 = (_MUXaddrINT & 0b00000001);
        _MUXaddrINT = _MUXaddrINT >> 1;
        _A1 = (_MUXaddrINT & 0b00000001);
        _MUXaddrINT = _MUXaddrINT >> 1;
        _A2 = (_MUXaddrINT & 0b00000001);
    }


    if ((EN1 != 0) && (EN2 != 0))
    {
        std::cerr << "Error: MUX1 and MUX2 enabled at same time !\n";
        _MuxEN1 = 0;
        _MuxEN2 = 0;
        return -2;
    }
    else 
    {
        _MuxEN1 = EN1;
        _MuxEN2 = EN2;
    }
    //std::cout << "MUX set  " << _A2 << _A1 << _A0 <<" MUX 1 = " << EN1 << " MUX 2 = " << EN2 << std::endl;
    MUX1.BBB_PinSetOutVal(_MuxEN1);
    MUX2.BBB_PinSetOutVal(_MuxEN2);
    MeasA0.BBB_PinSetOutVal(_A0);
    MeasA1.BBB_PinSetOutVal(_A1);
    MeasA2.BBB_PinSetOutVal(_A2);
    usleep(5000); //5ms
    return 0;
}