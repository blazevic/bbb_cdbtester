#pragma once

typedef struct {
    //B0
    unsigned int Connected : 1;
    unsigned int Disconnected : 1;
    unsigned int TesterOn : 1;
    unsigned int TesterOn_change : 1;
    unsigned int SpareB0b4 : 1;
    unsigned int CONTOn : 1;
    unsigned int INTFOn : 1;
    unsigned int DUTPS_change : 1;

    //B1 - U34A
    unsigned int I_PLC_0506 : 1;
    unsigned int I_PLC_0708 : 1;
    unsigned int I_PLC_0910 : 1;
    unsigned int I_PLC_1112 : 1;
    unsigned int I_PLC_1314 : 1;
    unsigned int I_PLC_1516 : 1;
    unsigned int I_PLC_1718 : 1;
    unsigned int U34_Spare1 : 1;
    //B2 - U34B
    unsigned int CDB1_0104 : 1;
    unsigned int CDB2_0104 : 1;
    unsigned int CDB3_0104 : 1;
    unsigned int CDB4_0104 : 1;
    unsigned int CDB5_0104 : 1;
    unsigned int CDB6_0104 : 1;
    unsigned int CDB7_0104 : 1;
    unsigned int I_PLC_1 : 1;

    //B3 - U31A
    unsigned int C_I1_8 : 1;
    unsigned int C_I1_9 : 1;
    unsigned int C_I1_12 : 1;
    unsigned int C_I1_1516 : 1;
    unsigned int C_I1_1718 : 1;
    unsigned int C_I2_2 : 1;
    unsigned int C_I2_3 : 1;
    unsigned int C_I2_4 : 1;
    //B4 - U31B
    unsigned int STOP : 1;
    unsigned int DOOR : 1;
    unsigned int U31_Spare1 : 1;
    unsigned int C_I1_2 : 1;
    unsigned int C_I1_3 : 1;
    unsigned int C_I1_4 : 1;
    unsigned int C_I1_5 : 1;
    unsigned int C_I1_7 : 1;

    //B5 - U29A
    unsigned int C_I3_3 : 1;
    unsigned int C_I3_4 : 1;
    unsigned int C_I3_5 : 1;
    unsigned int C_I3_7 : 1;
    unsigned int C_I3_8 : 1;
    unsigned int C_I3_9 : 1;
    unsigned int C_I3_12 : 1;
    unsigned int C_I3_1516 : 1;
    //B6 - U29B
    unsigned int C_I2_5 : 1;
    unsigned int C_I2_7 : 1;
    unsigned int C_I2_8 : 1;
    unsigned int C_I2_9 : 1;
    unsigned int C_I2_12 : 1;
    unsigned int C_I2_1516 : 1;
    unsigned int C_I2_1718 : 1;
    unsigned int C_I3_2 : 1;

    //B7 - U35A
    unsigned int C_I4_12 : 1;
    unsigned int C_I4_1516 : 1;
    unsigned int C_I4_1718 : 1;
    unsigned int C_I5_2 : 1;
    unsigned int C_I5_3 : 1;
    unsigned int C_I5_4 : 1;
    unsigned int C_I5_5 : 1;
    unsigned int C_I5_7 : 1;
    //B8 - U35B
    unsigned int C_I3_1718 : 1;
    unsigned int C_I4_2 : 1;
    unsigned int C_I4_3 : 1;
    unsigned int C_I4_4 : 1;
    unsigned int C_I4_5 : 1;
    unsigned int C_I4_7 : 1;
    unsigned int C_I4_8 : 1;
    unsigned int C_I4_9 : 1;

    //B9 - U32A
    unsigned int C_I6_5 : 1;
    unsigned int C_I6_7 : 1;
    unsigned int C_I6_8 : 1;
    unsigned int C_I6_9 : 1;
    unsigned int C_I6_12 : 1;
    unsigned int C_I6_1516 : 1;
    unsigned int C_I6_1718 : 1;
    unsigned int U32_Spare1 : 1;
    //B10 - U32B
    unsigned int C_I5_8 : 1;
    unsigned int C_I5_9 : 1;
    unsigned int C_I5_12 : 1;
    unsigned int C_I5_1516 : 1;
    unsigned int C_I5_1718 : 1;
    unsigned int C_I6_2 : 1;
    unsigned int C_I6_3 : 1;
    unsigned int C_I6_4 : 1;

    //B11 - U30A
    unsigned int C_I7_1516 : 1;
    unsigned int C_I7_1718 : 1;
    unsigned int U30_Spare1 : 1;
    unsigned int U32_Spare2 : 1;
    unsigned int U32_Spare3 : 1;
    unsigned int U32_Spare4 : 1;
    unsigned int U32_Spare5 : 1;
    unsigned int U32_Spare6 : 1;
    //B12 - U30B
    unsigned int C_I7_2 : 1;
    unsigned int C_I7_3 : 1;
    unsigned int C_I7_4 : 1;
    unsigned int C_I7_5 : 1;
    unsigned int C_I7_7 : 1;
    unsigned int C_I7_8 : 1;
    unsigned int C_I7_9 : 1;
    unsigned int C_I7_12 : 1;

    //B13 - U33A
    unsigned int CDB1_0506 : 1;
    unsigned int CDB2_0506 : 1;
    unsigned int CDB3_0506 : 1;
    unsigned int CDB4_0506 : 1;
    unsigned int CDB5_0506 : 1;
    unsigned int CDB6_0506 : 1;
    unsigned int CDB7_0506 : 1;
    unsigned int U33_Spare2 : 1;
    //B14 - U33B
    unsigned int CDB1_7 : 1;
    unsigned int CDB2_7 : 1;
    unsigned int CDB3_7 : 1;
    unsigned int CDB4_7 : 1;
    unsigned int CDB5_7 : 1;
    unsigned int CDB6_7 : 1;
    unsigned int CDB7_7 : 1;
    unsigned int U33_Spare1 : 1;

    //B15 - MCP0.8 spare A
    unsigned int MCP8_Spare1 : 1;
    unsigned int MCP8_Spare2 : 1;
    unsigned int MCP8_Spare3 : 1;
    unsigned int MCP8_Spare4 : 1;
    unsigned int MCP8_Spare5 : 1;
    unsigned int MCP8_Spare6 : 1;
    unsigned int MCP8_Spare7 : 1;
    unsigned int MCP8_Spare8 : 1;
    //B16 - MCP0.8 spare B
    unsigned int MCP8_Spare9 : 1;
    unsigned int MCP8_Spare10 : 1;
    unsigned int MCP8_Spare11 : 1;
    unsigned int MCP8_Spare12 : 1;
    unsigned int MCP8_Spare13 : 1;
    unsigned int MCP8_Spare14 : 1;
    unsigned int MCP8_Spare15 : 1;
    unsigned int MCP8_Spare16 : 1;

    //B17 - U28A
    unsigned int U28_Spare1 : 1;
    unsigned int U28_Spare2 : 1;
    unsigned int U28_Spare3 : 1;
    unsigned int U28_Spare4 : 1;
    unsigned int U28_Spare5 : 1;
    unsigned int U28_Spare6 : 1;
    unsigned int U28_Spare7 : 1;
    unsigned int U28_Spare8 : 1;
    //B18 - U28B
    unsigned int S : 1;
    unsigned int R : 1;
    unsigned int N : 1;
    unsigned int T : 1;
    unsigned int U28_Spare9 : 1;
    unsigned int U28_Spare10 : 1;
    unsigned int U28_Spare11 : 1;
    unsigned int U28_Spare12 : 1;

    //B19 - U27A
    unsigned int LS_14 : 1;
    unsigned int LS_15 : 1;
    unsigned int LS_1617 : 1;
    unsigned int C_I_1213 : 1;
    unsigned int C_I_1718 : 1;
    unsigned int C_I_1516 : 1;
    unsigned int C_I_1 : 1;
    unsigned int C_PLC_1 : 1;
    //B20 - U27B
    unsigned int LS_2 : 1;
    unsigned int LS_3 : 1;
    unsigned int LS_4 : 1;
    unsigned int LS_5 : 1;
    unsigned int LS_8 : 1;
    unsigned int LS_9 : 1;
    unsigned int LS_10 : 1;
    unsigned int LS_11 : 1;

    //B21 - U26A
    unsigned int ES2_2 : 1;
    unsigned int ES2_3 : 1;
    unsigned int ES2_4 : 1;
    unsigned int ES2_5 : 1;
    unsigned int ES2_6 : 1;
    unsigned int ES2_7 : 1;
    unsigned int ES2_8 : 1;
    unsigned int ES2_9 : 1;
    //B22 - U26B
    unsigned int ES1_2 : 1;
    unsigned int ES1_3 : 1;
    unsigned int ES1_4 : 1;
    unsigned int ES1_5 : 1;
    unsigned int ES1_6 : 1;
    unsigned int ES1_7 : 1;
    unsigned int ES1_8 : 1;
    unsigned int ES1_9 : 1;

}data_t;