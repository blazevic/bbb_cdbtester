//Version 0.20
//UDP_Server.h

#ifndef UDP_SERVER_H_INCLUDED
#define UDP_SERVER_H_INCLUDED

#define PORT 12345
#define BUFFER_SIZE 1024

#include <sys/socket.h>
#include <netinet/in.h>
#include "data_t_structure.h"
#include "data_m_structure.h"

class UDP_Server
{
private:

public:
    UDP_Server();
    ~UDP_Server();

    int sockfd;
    char buffer[BUFFER_SIZE];

    sockaddr_in serverAddr, clientAddr;
    socklen_t addrLen;

    int StartUDP();
    data_t Receive_Data_t();
    int Send_Data_t(data_t& message);
    int Send_Data_m(data_m& message);
};

#endif