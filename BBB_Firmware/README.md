```
cd existing_repo
git remote add origin https://gitlab.cern.ch/blazevic/bbb_cdbtester.git
git branch -M master
git push -uf origin master
```
Name: Ivan Blazevic
Organic unit: SY-EPC-OMS
e-mail: ivan.blazevic@cern.ch

# BBB_CDBTester
## Name: BBB_CDBTester

## Description
This project contains documentation of CDB_Tester designed by EPC-OMS.
Designed tester (electronics + software) are used for testing of CDB Switch modules installed in HL-LHC. Names of the modules are CDB Control and CDB Interface.

Electronics are based on BeagleBone Black (BBB) and custom made PCB which provides an interface between the tester and DUT.
BBB is based on ARM Cortex-A8 (AM335x) procesor with 512MB of DDR3 and varios other interfaces/peripherals like Ethernet, HDMI, USB, GPIO, ...
BBB plugs directly in to the custom made PCB. BBB uses Linux Debian as home OS.

Software consists of 2 parts:
    -   FIRMWARE of the BBB
    -   Windows APP that allows user acces from the PC

## BBB Debian OS - Installation

Linux Debian Image:

To make all software modules compatible, first we need to instal a specific image of Linux Debian on BBB.
After the Debian OS is installed we will need to apply some additional files and settings to the BBB.

BBB SSH connection:
    -   open command prompt
    -   use command "ssh debian@BeagleBone"
    -   defaul user: debian
    -   default pass: temppwd

Required Items
    -   Micro sd card. 8 GB minimum
    -   Micro sd card reader or a built in sd card reader for your PC
    -   BeagleBone image - am335x-debian-11.7-iot-armhf-2023-09-02-4gb.img
    -   Balena Etcher utility

Steps Overview

1.  Burn the downloaded image onto a micro sd card using the Balena Etcher utility.
2.  Insert the newly flashed Micro SD card to BBB and power it on.
3.  Boot the BBB from Micro SD card (not from internal eMMC).

Windows PCs

1.  Download the BeagleBone OS image - am335x-debian-11.7-iot-armhf-2023-09-02-4gb.img
    https://www.beagleboard.org/distros/am335x-11-7-2023-09-02-4gb-microsd-iot
2.  Use Balena Etcher to burn the image to the Micro SD card.
3.  Insert the Micro SD card to BBB.
4.  Power on your BBB.
5.  Make sure BBB is configured to boot from SD card.
    Normaly BBB should do this automaticaly. Refer to manufacturer documentation in case manual setup is needed.

How to tell if BBB is booting from SD or from eMMC?
Use a command df:
    -   when booted from SD you shuld see - /dev/mmcblk0p1
    -   when booted from eMMC you shuld see - /dev/mmcblk1p1


## BBB Debian OS - Setup

BBB SSH connection:
    -   open command prompt
    -   use command "ssh debian@BeagleBone"
    -   defaul user: debian
    -   default pass: temppwd


Change the passwords:
To make BBB more secure change the password for users: 
    -   root
    -   debian
    -   command "passwd"

Internet connection to BBB:
    -   Check if BBB has acces to internet - "ping www.google.com"
    -   In case there is not internet connection refer to BBB manufacturer doc for tips on Windows settings.

Update the Debian OS
    -   "sudo apt update"
    -   "sudo apt upgrade -y" - can take 5-20 minutes

Debian kernel version
    -   command "uname -a"
    -   should return - Linux BeagleBone 5.10.168-ti-r77 #1bullseye SMP PREEMPT Wed Feb 28 21:05:58 UTC 2024 armv7l GNU/Linux

Enabling 2 SPI channels:
    -   navigate to "cd /boot"
    -   open txt file "sudo nano uEnv.txt"
    -   set: enable_uboot_overlays=0
    -   disable devices by removing "#" in front of lines of: emmc, video, audio, wireles
    -   navigate to "cd /etc/init.d/"
    -   create a txt file "sudo nano 2SPI.sh"
    -   copy the 2SPI.sh code inside of it, save, close
    -   run the following to make the script executable "chmod +x /etc/init.d/2SPI.sh"
    -   apply the script on each boot with command "sudo update-rc.d 2SPI.sh defaults
    -   reboot BBB

    2SPI.sh
    ***************************************
    #!/bin/bash

    # Configure SPI pins for spi0
    config-pin P9.17 spi_cs
    config-pin P9.18 spi
    config-pin P9.21 spi
    config-pin P9.22 spi_sclk

    # Configure SPI pins for spi1
    config-pin P9.28 spi_cs
    config-pin P9.29 spi
    config-pin P9.30 spi
    config-pin P9.31 spi_sclk

    # Load spidev module
    sudo modprobe spidev

    exit 0
    ***************************************

Give SPI permission to non root user
    -   navigate to "cd /etc/udev/rules.d"
    -   create txt file "sudo nano 50-spi.rules"
    -   write: SUBSYSTEM=="spidev", GROUP="spiuser", MODE="0660"
    -   save and close the file
    -   create spi group "sudo groupadd spiuser"
    -   add debian user to the group "sudo adduser "debian" spiuser"
    -   reboot BBB

Give GPIO permission to non root user
    -   user debian already has GPIO access

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos).
Tools like ttygif can help, but check out Asciinema for a more sophisticated method.


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they 
are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

## Authors and acknowledgment

## Project status

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.cern.ch/blazevic/bbb_cdbtester/-/settings/integrations)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)s