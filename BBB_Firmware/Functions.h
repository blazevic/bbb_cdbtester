//Version 0.20
//Functions.h

#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED

#include "BBB_Pin.h"
#include "SPI.h"
#include "MCP23S17.h"
#include "ReadADC.h"

void TesterON (BBB_Pin& V24, BBB_Pin& V15, BBB_Pin& V24V2);

void TesterOFF (BBB_Pin& V24, BBB_Pin& V15, BBB_Pin& V24V2, BBB_Pin& CTRL_ON, BBB_Pin& INTF_ON, BBB_Pin& MUX1, BBB_Pin& MUX2, BBB_Pin& MeasA0, BBB_Pin& MeasA1, BBB_Pin& MeasA2, BBB_Pin& MCP_Reset0, BBB_Pin& MCP_Reset1);

void printBuffer (uint8_t* buffer, int buff_size);

#endif