//Version 0.20
//Junk.cpp

/*
    //Initializes all MCP GPIO as Output LOW
    for (int i = 0; i < 8; i++)
    {
        mcp0[i].MCP_Init(tx_buffer0);
        std::cout << "\nMCP0." << i << "initialized:";
        printBuffer(tx_buffer0, buff_size);

        mcp1[i].MCP_Init(tx_buffer1);
        std::cout << "\nMCP1." << i << "initialized:";
        printBuffer(tx_buffer1, buff_size);

        Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    }
*/

/*
//Set GPIOA and GPIOB as Out
for (int i = 0; i < 8; i++)
{
    mcp0[i].MCP_SetDir_8(IODIRA, SetAsOut, tx_buffer0);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp0[i].MCP_SetDir_8(IODIRB, SetAsOut, tx_buffer0);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp1[i].MCP_SetDir_8(IODIRA, SetAsOut, tx_buffer1);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp1[i].MCP_SetDir_8(IODIRB, SetAsOut, tx_buffer1);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
}
usleep(200000);

for (uint8_t i = 0; i < 8; i++)
{
    mcp0[i].MCP_WriteGPIO_8(GPIOA, 2 ^ i, tx_buffer0);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp0[i].MCP_WriteGPIO_8(GPIOB, 2 ^ i, tx_buffer0);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp1[i].MCP_WriteGPIO_8(GPIOA, 2 ^ i, tx_buffer1);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp1[i].MCP_WriteGPIO_8(GPIOB, 2 ^ i, tx_buffer1);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    usleep(200000);

}
usleep(200000);
*/

//int MCP_Init(unsigned char* tx_buff);

/*
int MCP23S17::MCP_Init(unsigned char* tx_buff) //Sets all MCP GPIO as Output LOW
{
    MCP_SetDir_8(IODIRA, SetAsOut, tx_buff);
    MCP_SetDir_8(IODIRB, SetAsOut, tx_buff);
    MCP_WriteGPIO_8(GPIOA, 0x00, tx_buff);
    MCP_WriteGPIO_8(GPIOB, 0x00, tx_buff);
    return 0;
}
*/

/*
//Set GPIOA and GPIOB as Out
for (int i = 0; i < 8; i++)
{
    mcp0[i].MCP_SetDir_8(IODIRA, SetAsOut, tx_buffer0);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp0[i].MCP_SetDir_8(IODIRB, SetAsOut, tx_buffer0);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp1[i].MCP_SetDir_8(IODIRA, SetAsOut, tx_buffer1);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp1[i].MCP_SetDir_8(IODIRB, SetAsOut, tx_buffer1);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
}
usleep(200000);

for (uint8_t i = 0; i < 8; i++)
{
    mcp0[i].MCP_WriteGPIO_8(GPIOA, 2 ^ i, tx_buffer0);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp0[i].MCP_WriteGPIO_8(GPIOB, 2 ^ i, tx_buffer0);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp1[i].MCP_WriteGPIO_8(GPIOA, 2 ^ i, tx_buffer1);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    mcp1[i].MCP_WriteGPIO_8(GPIOB, 2 ^ i, tx_buffer1);
    Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    usleep(200000);
}
*/

/*
    //Set GPIOA and GPIOB as Out
    for (int i = 0; i < 8; i++)
    {
        mcp0[i].MCP_SetDir_8(IODIRA, SetAsOut, tx_buffer0);
        Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
        mcp0[i].MCP_SetDir_8(IODIRB, SetAsOut, tx_buffer0);
        Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
        mcp1[i].MCP_SetDir_8(IODIRA, SetAsOut, tx_buffer1);
        Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
        mcp1[i].MCP_SetDir_8(IODIRB, SetAsOut, tx_buffer1);
        Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
    }
    usleep(200000);

    for (uint8_t i = 0; i < 8; i++)
    {
        mcp0[i].MCP_WriteGPIO_8(GPIOA, 2 ^ i, tx_buffer0);
        Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
        mcp0[i].MCP_WriteGPIO_8(GPIOB, 2 ^ i, tx_buffer0);
        Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
        mcp1[i].MCP_WriteGPIO_8(GPIOA, 2 ^ i, tx_buffer1);
        Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
        mcp1[i].MCP_WriteGPIO_8(GPIOB, 2 ^ i, tx_buffer1);
        Send_SPI(tx_buffer0, rx_buffer0, tx_buffer1, rx_buffer1, SPIdev0, SPIdev1, buff_size);
        usleep(200000);
    }
*/

/*

  for (int i=0; i<100; i++)
    {
    MCP_SetAll(mcp0, tx_buffer0, rx_buffer0, SPIdev0, mcp1, tx_buffer1, rx_buffer1, SPIdev1, buff_size, 0x01);

    usleep(100000);

    MCP_SetAll(mcp0, tx_buffer0, rx_buffer0, SPIdev0, mcp1, tx_buffer1, rx_buffer1, SPIdev1, buff_size, 0x00);

    usleep(100000);
    }

*/