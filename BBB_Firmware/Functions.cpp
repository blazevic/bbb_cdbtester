//Version 0.20
//Functions.cpp

#include "Functions.h"
#include <unistd.h>
#include <iostream>
#include <fstream>

using namespace std;

void TesterON(BBB_Pin& V24, BBB_Pin& V15, BBB_Pin& V24V2)
{
    V24.BBB_PinSetOutVal(1);
    usleep(5000);
    V15.BBB_PinSetOutVal(1);
    usleep(5000);
    V24V2.BBB_PinSetOutVal(1);
    usleep(5000);
}

void TesterOFF(BBB_Pin& V24, BBB_Pin& V15, BBB_Pin& V24V2, BBB_Pin& CTRL_ON, BBB_Pin& INTF_ON, BBB_Pin& MUX1, BBB_Pin& MUX2,
               BBB_Pin& MeasA0, BBB_Pin& MeasA1, BBB_Pin& MeasA2, BBB_Pin& MCP_Reset0, BBB_Pin& MCP_Reset1)
{
    V24.BBB_PinSetOutVal(0);
    V15.BBB_PinSetOutVal(0);
    V24V2.BBB_PinSetOutVal(0);
    CTRL_ON.BBB_PinSetOutVal(0);
    INTF_ON.BBB_PinSetOutVal(0);
    MUX1.BBB_PinSetOutVal(0);
    MUX2.BBB_PinSetOutVal(0);
    MeasA0.BBB_PinSetOutVal(0);
    MeasA1.BBB_PinSetOutVal(0);
    MeasA2.BBB_PinSetOutVal(0);
    MCP_Reset0.BBB_PinSetOutVal(0);
    MCP_Reset1.BBB_PinSetOutVal(0);
}

void printBuffer(uint8_t* buffer, int buff_size)
{
    bool flag = 0;
    for (int i = 0; (i < buff_size && flag == 0); i++)
    {
        if (buffer[i] != 0xFF)
        {
            if (i % 10 == 0)
            {
                std::cout << "\n";
            }
            std::cout << i << ": " << int(buffer[i]) << "    ";

        }
        if (buffer[i] == 0xFF)
        {
            flag = 1;
        }
    }
    std::cout << "\n";
}