//Version 0.20
//UDP_Server.cpp

#include "UDP_Server.h"
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace std;

UDP_Server::UDP_Server()
{
}

UDP_Server::~UDP_Server()
{
    // Close the socket
    close(sockfd);
}

int UDP_Server::StartUDP()
{

   addrLen = sizeof(clientAddr);
   
   for (int i = 0; i < BUFFER_SIZE; i++)
   {
       buffer[i] = 0x00;
   }
   
   // Create UDP socket

   if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
   {
        perror("socket creation failed");
        return 2;
   }

   // Initialize server address structure

    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddr.sin_port = htons(PORT);

    // Bind socket to address
    if (bind(sockfd, (const struct sockaddr*)&serverAddr, sizeof(serverAddr)) < 0)
    {
        perror("bind failed");
        return 1;
    }

    std::cout << "UDP Server running on port " << PORT << "\n\n";
    return 0;
}


data_t UDP_Server::Receive_Data_t()
{
    data_t message;
    memset(&message, 0, sizeof(message));

    int recvLen;
    // Infinite loop to receive messages
    while (true)
    {
        recvLen = recvfrom(sockfd, buffer, BUFFER_SIZE, 0, (struct sockaddr*)&clientAddr, &addrLen);

        if (recvLen < 0) {
            perror("recvfrom failed");
        }
        if (recvLen > 0)
        {
            memcpy(&message, buffer, sizeof(message));
        break;
        }
    }
    return (message);
}

int UDP_Server::Send_Data_t(data_t& message)
{
    sendto(sockfd, reinterpret_cast<const char*>(&message), sizeof(message), 0, (const struct sockaddr*)&clientAddr, addrLen);
    return 0;
}

int UDP_Server::Send_Data_m(data_m& message)
{
    sendto(sockfd, reinterpret_cast<const char*>(&message), sizeof(message), 0, (const struct sockaddr*)&clientAddr, addrLen);
    return 0;
}