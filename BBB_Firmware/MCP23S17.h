//Version 0.20
//MCP23s17.h


#ifndef MCP23S17_H_INCLUDED
#define MCP23S17_H_INCLUDED

#include "BBB_Pin.h"
#include "SPI.h"
#include <string>

//Defines MCP Values and Ports
enum MCPPorts
{
    SetAsOut = 0x00,
    SetAsIn = 0xFF,
    IODIRA = 0x00,
    IODIRB = 0x01,
    GPIOA = 0x12,
    GPIOB = 0x13,
    IOCONN_A = 0x0A,
    IOCONN_B = 0x0B,
    IOCONNSetting = 0b00101000
};

class MCP23S17
{
private:

    SPI_Device& _SPI_X;
    uint8_t _RD_Address;
    uint8_t _WR_Address;

public:
    MCP23S17(SPI_Device& SPI_X, uint8_t ReadAddrMCP, uint8_t WriteAddrMCP);

    ~MCP23S17();

    static void MCP_Reset(BBB_Pin& MCP_Reset0, BBB_Pin& MCP_Reset1, int delay);

    void MCP_SetDir_8(uint8_t port, uint8_t direction);

    void MCP_SetIOCONN();

    void MCP_WriteGPIO_8(uint8_t port, uint8_t value);

};
#endif