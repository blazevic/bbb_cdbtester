//Version 0.20
//SPI.cpp

#include "SPI.h"
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <cstring>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

SPI_Device::SPI_Device(const std::string& device, uint32_t speed)
    : _device(device), _speed(speed), _spi_fd(-1) {
}

SPI_Device::~SPI_Device()
{
    SPI_Device::closeSPI();
}

int SPI_Device::initSPI() {
    _spi_fd = open(_device.c_str(), O_RDWR);
    if (_spi_fd < 0) {
        std::cerr << "Failed to open SPI device: " << _device << std::endl;
        return -1;
    }

    // Set SPI mode
    uint8_t mode = SPI_MODE_0;
    if (ioctl(_spi_fd, SPI_IOC_WR_MODE, &mode) == -1) {
        std::cerr << "Failed to set SPI mode" << std::endl;
        return -1;
    }
    /*
    // Set 8 bits per word
    uint8_t bits = 8;
    if (ioctl(_spi_fd, SPI_IOC_WR_BITS_PER_WORD, &bits) == -1) {
        std::cerr << "Failed to set SPI bits per word" << std::endl;
        return -1;
    }
    */

    // Set SPI speed
    if (ioctl(_spi_fd, SPI_IOC_WR_MAX_SPEED_HZ, &_speed) == -1) {
        std::cerr << "Failed to set SPI speed" << std::endl;
        return -1;
    }

    return 0;
}

int SPI_Device::transfer(uint8_t *txBuffer, uint8_t *rxBuffer, size_t length) {
    struct spi_ioc_transfer spiTransfer;
    std::memset(&spiTransfer, 0, sizeof(spiTransfer));

    spiTransfer.tx_buf = (unsigned long)txBuffer;
    spiTransfer.rx_buf = (unsigned long)rxBuffer;
    spiTransfer.len = length;

    int ret = ioctl(_spi_fd, SPI_IOC_MESSAGE(1), &spiTransfer);
    if (ret < 0) {
        std::cerr << "SPI transfer failed" << std::endl;
        return -1;
    }

    return ret;
}

void SPI_Device::closeSPI() {
    if (_spi_fd >= 0) {
        ::close(_spi_fd);
        _spi_fd = -1;
    }
}