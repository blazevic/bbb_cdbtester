//Version 0.20
//main.cpp

#include "Header.h"
#include "BBB_Pin.h"
#include "MCP23S17.h"
#include "SPI.h"
#include "ReadADC.h"
#include "Functions.h"
#include "MCP23S17.h"
#include "data_t_structure.h"
#include "data_m_structure.h"
#include "UDP_Server.h"
#include <string>
#include <unistd.h>
#include <iostream>
#include <cstring>


using namespace std;

int main()
{
    data_t myData;
    data_m Measurements;

    memset(&Measurements, 0, sizeof(Measurements));
    memset(&myData, 0, sizeof(myData));

    //Create object for each used BBB pin, and set as Out LOW
    BBB_Pin V24(V24_pin);                  V24.BBB_PinSetAsOut();
    BBB_Pin V15(V15_pin);                  V15.BBB_PinSetAsOut();
    BBB_Pin V24v2(V24V2_pin);              V24v2.BBB_PinSetAsOut();
    BBB_Pin CTRL_ON(CTRL_ON_pin);          CTRL_ON.BBB_PinSetAsOut();
    BBB_Pin INTF_ON(INTF_ON_pin);          INTF_ON.BBB_PinSetAsOut();
    BBB_Pin MUX1(MUX1_pin);                MUX1.BBB_PinSetAsOut();
    BBB_Pin MUX2(MUX2_pin);                MUX2.BBB_PinSetAsOut();
    BBB_Pin MeasA0(MeasA0_pin);            MeasA0.BBB_PinSetAsOut();
    BBB_Pin MeasA1(MeasA1_pin);            MeasA1.BBB_PinSetAsOut();
    BBB_Pin MeasA2(MeasA2_pin);            MeasA2.BBB_PinSetAsOut();
    BBB_Pin MCP_Reset0(MCP_Reset0_pin);    MCP_Reset0.BBB_PinSetAsOut();
    BBB_Pin MCP_Reset1(MCP_Reset1_pin);    MCP_Reset1.BBB_PinSetAsOut();

    // Create SPI devices
    SPI_Device SPIdev0(SPI0, 50000);
    SPI_Device SPIdev1(SPI1, 50000);

    // Initialize SPI devices
    if (SPIdev0.initSPI() != 0)
    {
        std::cerr << "Failed to initialize SPI device spi.0" << std::endl;
        return 1;
    }
    if (SPIdev1.initSPI() != 0)
    {
        std::cerr << "Failed to initialize SPI device spi.1" << std::endl;
        return 1;
    }

    // Create objects for 16 MCP23S17 chips using raw addresses
    MCP23S17 mcp0[8] = { MCP23S17(SPIdev0, ReadAddrMCP[0], WriteAddrMCP[0]), MCP23S17(SPIdev0, ReadAddrMCP[1], WriteAddrMCP[1]), MCP23S17(SPIdev0, ReadAddrMCP[2], WriteAddrMCP[2]), MCP23S17(SPIdev0, ReadAddrMCP[3], WriteAddrMCP[3]),
                         MCP23S17(SPIdev0, ReadAddrMCP[4], WriteAddrMCP[4]), MCP23S17(SPIdev0, ReadAddrMCP[5], WriteAddrMCP[5]), MCP23S17(SPIdev0, ReadAddrMCP[6], WriteAddrMCP[6]), MCP23S17(SPIdev0, ReadAddrMCP[7], WriteAddrMCP[7]) };

    MCP23S17 mcp1[8] = { MCP23S17(SPIdev1, ReadAddrMCP[0], WriteAddrMCP[0]), MCP23S17(SPIdev1, ReadAddrMCP[1], WriteAddrMCP[1]), MCP23S17(SPIdev1, ReadAddrMCP[2], WriteAddrMCP[2]), MCP23S17(SPIdev1, ReadAddrMCP[3], WriteAddrMCP[3]),
                         MCP23S17(SPIdev1, ReadAddrMCP[4], WriteAddrMCP[4]), MCP23S17(SPIdev1, ReadAddrMCP[5], WriteAddrMCP[5]), MCP23S17(SPIdev1, ReadAddrMCP[6], WriteAddrMCP[6]), MCP23S17(SPIdev1, ReadAddrMCP[7], WriteAddrMCP[7]) };

    //Resets all 16 MCPs
    MCP23S17::MCP_Reset(MCP_Reset0, MCP_Reset1, 5000); // 5ms

    //Set IOCONN for all MCP
    for (int i = 0; i < 8; i++)
    {
        mcp0[i].MCP_SetIOCONN();
    }
    for (int i = 0; i < 8; i++)
    {
        mcp1[i].MCP_SetIOCONN();
    }


    //Initializes all 16x16 MCP pins as Out LOW
    for (int i = 0; i < 8; i++)
    {
        mcp0[i].MCP_SetDir_8(IODIRA, SetAsOut);
        mcp0[i].MCP_SetDir_8(IODIRB, SetAsOut);
    }

    for (int i = 0; i < 8; i++)
    {
        mcp1[i].MCP_SetDir_8(IODIRA, SetAsOut);
        mcp1[i].MCP_SetDir_8(IODIRB, SetAsOut);
    }

    //Create 7 ADC objects for each channel
    //ReadADC ADC[7] = { ReadADC(0), ReadADC(1), ReadADC(2), ReadADC(3), ReadADC(4), ReadADC(5), ReadADC(6) };

    ReadADC ADC0(0);
    ReadADC ADC1(1);
    ReadADC ADC2(2);
    ReadADC ADC3(3);
    ReadADC ADC4(4);
    ReadADC ADC5(5);
    ReadADC ADC6(6);

    //Create and start UDP server
    UDP_Server UDP;
    UDP.StartUDP();

    while (true)
    {
        std::cout << "New !" << std::endl;
        memset (&Measurements, 0, sizeof(Measurements));
        memset (&myData, 0, sizeof(myData));
        myData = UDP.Receive_Data_t();
        UDP.Send_Data_t(myData);

        if (myData.TesterOn_change == 1)
        {
                usleep(5000);
                V24.BBB_PinSetOutVal(myData.TesterOn);
                usleep(5000);
                V15.BBB_PinSetOutVal(myData.TesterOn);
                usleep(5000);
                V24v2.BBB_PinSetOutVal(myData.TesterOn);
                usleep(5000);
        }
        if (myData.DUTPS_change == 1)
        {
            usleep(5000);
            CTRL_ON.BBB_PinSetOutVal(myData.CONTOn);
            usleep(5000);
            INTF_ON.BBB_PinSetOutVal(myData.INTFOn);
            usleep(5000);
        }

        uint8_t* bytePointer = reinterpret_cast<uint8_t*>(&myData);

        int j = 1;
        for (int i = 0; i < 8; i++)
        {
            mcp0[i].MCP_WriteGPIO_8(GPIOA, bytePointer[j]);
            j++;
            mcp0[i].MCP_WriteGPIO_8(GPIOB, bytePointer[j]);
            j++;
        }
        for (int i = 0; i < 8; i++)
        {
            mcp1[i].MCP_WriteGPIO_8(GPIOA, bytePointer[j]);
            j++;
            mcp1[i].MCP_WriteGPIO_8(GPIOB, bytePointer[j]);
            j++;
        }

        unsigned int* bytePointer2 = reinterpret_cast<unsigned int*>(&Measurements);
        j = 0;
        ADC0.SetMux(0, 0, 0, MUX1, MUX2, MeasA0, MeasA1, MeasA2);
        bytePointer2[j] = ADC0.ReadVal();
        j++;
        bytePointer2[j] = ADC1.ReadVal();
        j++;
        bytePointer2[j] = ADC2.ReadVal();
        j++;
        
        for (uint8_t i = 0; i < 8; i++)
        {
            ADC0.SetMux(i, 1, 0, MUX1, MUX2, MeasA0, MeasA1, MeasA2);
            bytePointer2[j] = ADC3.ReadVal();
            j++;
            bytePointer2[j] = ADC4.ReadVal();
            j++;
            bytePointer2[j] = ADC5.ReadVal();
            j++;
            bytePointer2[j] = ADC6.ReadVal();
            j++;
        }
        for (uint8_t i = 0; i < 8; i++)
        {
            ADC0.SetMux(i, 0, 1, MUX1, MUX2, MeasA0, MeasA1, MeasA2);
            bytePointer2[j] = ADC3.ReadVal();
            j++;
            bytePointer2[j] = ADC4.ReadVal();
            j++;
            bytePointer2[j] = ADC5.ReadVal();
            j++;
            bytePointer2[j] = ADC6.ReadVal();
            j++;
        }
        UDP.Send_Data_m(Measurements);
        ADC0.SetMux(0, 0, 0, MUX1, MUX2, MeasA0, MeasA1, MeasA2);
    }
    return 0;
}