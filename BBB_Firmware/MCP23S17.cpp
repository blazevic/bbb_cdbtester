//Version 0.20
//MCP23s17.cpp

#include "MCP23S17.h"
#include <unistd.h>
#include <string>
#include <iostream>

using namespace std;

MCP23S17::MCP23S17(SPI_Device& SPI_X, uint8_t ReadAddrMCP, uint8_t WriteAddrMCP) : _SPI_X(SPI_X), _RD_Address(ReadAddrMCP), _WR_Address(WriteAddrMCP)
{
};

MCP23S17::~MCP23S17()
{
};

void MCP23S17::MCP_SetDir_8(uint8_t port, uint8_t direction)
{
    uint8_t tx_buffer[3];
    uint8_t rx_buffer[3];

    tx_buffer[0] = _WR_Address;
    tx_buffer[1] = port;
    tx_buffer[2] = direction;

    _SPI_X.transfer(tx_buffer, rx_buffer, 3);
}

void MCP23S17::MCP_WriteGPIO_8(uint8_t port, uint8_t value)
{
    uint8_t tx_buffer[3];
    uint8_t rx_buffer[3];

    tx_buffer[0] = _WR_Address;
    tx_buffer[1] = port;
    tx_buffer[2] = value;

    _SPI_X.transfer(tx_buffer, rx_buffer, 3);
}

void MCP23S17::MCP_SetIOCONN()
{
    uint8_t tx_buffer[3];
    uint8_t rx_buffer[3];

    tx_buffer[0] = _WR_Address;
    tx_buffer[1] = IOCONN_A;
    tx_buffer[2] = IOCONNSetting;

    _SPI_X.transfer(tx_buffer, rx_buffer, 3);

    tx_buffer[0] = _WR_Address;
    tx_buffer[1] = IOCONN_B;
    tx_buffer[2] = IOCONNSetting;

    _SPI_X.transfer(tx_buffer, rx_buffer, 3);
}


void MCP23S17::MCP_Reset(BBB_Pin& MCP_Reset0, BBB_Pin& MCP_Reset1, int delay)
{
    MCP_Reset0.BBB_PinSetOutVal(0);
    MCP_Reset1.BBB_PinSetOutVal(0);
    usleep(delay);
    MCP_Reset0.BBB_PinSetOutVal(1);
    MCP_Reset1.BBB_PinSetOutVal(1);
}