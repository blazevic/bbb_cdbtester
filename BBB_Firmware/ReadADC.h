//Version 0.20
//ReadADC.h

#ifndef READADC_H_INCLUDED
#define READADC_H_INCLUDED

#include "BBB_Pin.h"
#include "data_m_structure.h"
#include <string>
#include <fstream>
#include <iostream>

class ReadADC
{
private:

    int _ch;
    std::ifstream _adcFile;
    std::string _filePath;

public:

    int _iAdcVal;
    int _uAdcVal;
    std::string _sAdcVal;

public:

    ReadADC(int ch);

    ~ReadADC();

    void OpenFile();

    void CloseFile();

    unsigned int ReadVal();
    
    int SetMux(uint8_t MUXaddrINT, bool MuxEN1, bool MuxEN2, BBB_Pin& MUX1, BBB_Pin& MUX2, BBB_Pin& MeasA0, BBB_Pin& MeasA1, BBB_Pin& MeasA2);

};
#endif