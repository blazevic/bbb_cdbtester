#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include <QMainWindow>
#include <QByteArray>
#include <QString>
#include <QtGlobal>
#include <QTimer>
#include "Ethernet.h"
#include "data_t_structure.h"
#include "data_m_structure.h"

QT_BEGIN_NAMESPACE
namespace Ui {class MainWindow;}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    unsigned int Responding :1;
    unsigned int ACK_OK :1;
    unsigned int Connected :1;
    unsigned int Disconnected :1;
    unsigned int Connect :1;
    unsigned int Disconnect :1;

    unsigned int INTFOn :1;
    unsigned int CONTOn :1;
    unsigned int DUTPS_change :1;
    unsigned int TesterOn :1;
    unsigned int TesterOn_change :1;

    unsigned int time_ms;

    data_t myData;
    data_t myData_ACK;
    data_m Measurements;

    ETH ethernet;

    void getIP();
    void getPort();
    bool areEqual(const data_t &a, const data_t &b);

private slots:

    void update_data();
    void handlePB_Connect();
    void handlePB_Disconnect();
    void handlePBC_ContOn();
    void handlePBC_ContOff();
    void handlePBI_IntfOn();
    void handlePBI_IntfOff();
    void handlePB_TesterOn();
    void handlePB_TesterOff();
    void handlePBI_IntfTest();
    void handlePBC_ContTest();
    void handlePB_SignalsReset();
    void RefreshUI();
    void onTimeout();

private:
    Ui::MainWindow *ui;
    QTimer *timer;  // Pointer to the timer

};

#endif // MAINWINDOW_H_
