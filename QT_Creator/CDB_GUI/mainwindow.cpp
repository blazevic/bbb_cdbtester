#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Ethernet.h"
#include <QDebug>
#include <QPushButton>
#include <QObject>
#include <QCheckBox>
#include <QLineEdit>
#include <QByteArray>
#include <QTimer>
#include <cstring>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)

{
    ui->setupUi(this);

    // Connect the signals and slots
    connect(ui->PBConnect, &QPushButton::clicked, this, &MainWindow::handlePB_Connect);
    connect(ui->PBDisconnect, &QPushButton::clicked, this, &MainWindow::handlePB_Disconnect);
    connect(ui->PBCONTOn, &QPushButton::clicked, this, &MainWindow::handlePBC_ContOn);
    connect(ui->PBCONTOff, &QPushButton::clicked, this, &MainWindow::handlePBC_ContOff);
    connect(ui->PBINTFOn, &QPushButton::clicked, this, &MainWindow::handlePBI_IntfOn);
    connect(ui->PBINTFOff, &QPushButton::clicked, this, &MainWindow::handlePBI_IntfOff);
    connect(ui->PBTesterON, &QPushButton::clicked, this, &MainWindow::handlePB_TesterOn);
    connect(ui->PBTesterOFF, &QPushButton::clicked, this, &MainWindow::handlePB_TesterOff);
    connect(ui->PBCONTtest, &QPushButton::clicked, this, &MainWindow::handlePBC_ContTest);
    connect(ui->PBINTFtest, &QPushButton::clicked, this, &MainWindow::handlePBI_IntfTest);
    connect(ui->PBSignalsReset, &QPushButton::clicked, this, &MainWindow::handlePB_SignalsReset);

    ui->DutTestTabs->setTabEnabled(0, false);
    ui->DutTestTabs->setTabEnabled(1, false);
    ui->DutTestTabs->setTabEnabled(2, false);
    ui->DutTestTabs->setTabEnabled(3, false);
    ui->DutTestTabs->setTabEnabled(4, false);
    ui->DutTestTabs->setCurrentIndex(4);



    Responding = 0;
    ACK_OK = 0;
    Connected = 0;
    Disconnected = 1;
    Connect = 0;
    Disconnect = 1;
    INTFOn = 0;
    CONTOn = 0;
    DUTPS_change = 0;
    TesterOn = 0;
    TesterOn_change = 0;

    time_ms = 500;

    memset (&myData, 0, sizeof(myData));
    memset (&myData_ACK, 0, sizeof(myData_ACK));
    memset (&Measurements, 0, sizeof(Measurements));

    // Create a QTimer instance
    timer = new QTimer(this);

    // Connect the timer's timeout signal to the slot
    connect(timer, &QTimer::timeout, this, &MainWindow::onTimeout);

    timer->start(time_ms);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handlePBC_ContTest()
{
    INTFOn = 0;
    CONTOn = 0;
    DUTPS_change = 1;
    MainWindow::handlePB_SignalsReset();
    ui->DutTestTabs->setTabEnabled(0, true);
    ui->DutTestTabs->setTabEnabled(1, false);
    ui->DutTestTabs->setCurrentIndex(0);
}

void MainWindow::handlePBI_IntfTest()
{
    INTFOn = 0;
    CONTOn = 0;
    DUTPS_change = 1;
    MainWindow::handlePB_SignalsReset();
    ui->DutTestTabs->setTabEnabled(1, true);
    ui->DutTestTabs->setTabEnabled(0, false);
    ui->DutTestTabs->setCurrentIndex(1);
}

void MainWindow::onTimeout()
{
    timer->stop();
    ACK_OK = 0;
    Responding = 0;
    memset (&myData, 0, sizeof(myData));
    memset (&myData_ACK, 0, sizeof(myData_ACK));
    memset (&Measurements, 0, sizeof(Measurements));
    update_data();
    TesterOn_change = 0;
    DUTPS_change = 0;
    if (Connected == 1 && Disconnected == 0)
    {
        ethernet.Send_Data_t(myData);
        myData_ACK = ethernet.Receive_Data_t();
        Responding = myData_ACK.Connected;
        if (Responding == 1)
        {
            int j;
            j=areEqual(myData, myData_ACK);
            if (j == 0)
            {
                ACK_OK = 1;
            }
            if (j != 0)
            {
                ACK_OK = 0;
            }
        }

    }
    if (Responding == 1 && ACK_OK == 1)
    {
        Measurements = ethernet.Receive_Data_m();
    }
    RefreshUI();
    timer->start(time_ms);
 }

void MainWindow::getIP()
{
    ethernet.servAddrSTR = ui->TextIP->text(); // Return IP from QLineEdit
}

void MainWindow::getPort()
{
    QString stringPort = ui->TextPort->text(); // Return Port from QLineEdit
    ethernet.servPort = static_cast<quint16>(stringPort.toUInt());
}

void MainWindow::handlePB_Connect()
{
    timer->stop();
    handlePB_SignalsReset();
    INTFOn = 0;
    CONTOn = 0;
    DUTPS_change = 1;
    TesterOn = 0;
    TesterOn_change = 1;
    qDebug() << "\nButton Connect clicked!";
    Connect = 1;
    Disconnect = 0;

    getIP();
    getPort();
    int i = ethernet.OpenUDP();
    if (i == 0)
    {
        Connected = 1;
        Disconnected = 0;
        timer->start(time_ms);
    }
    else
    {
        Connected = 0; Disconnected = 1;
        qDebug() << "Failed to Connect to BBB!\n";
        timer->start(time_ms);
    }
}

void MainWindow::handlePB_Disconnect()
{
    qDebug() << "\nButton Disconnect clicked!";
    timer->stop();
    handlePB_SignalsReset();
    INTFOn = 0;
    CONTOn = 0;
    DUTPS_change = 1;
    TesterOn = 0;
    TesterOn_change = 1;
    onTimeout();
    Connect = 0;
    Disconnect = 1;
    int i = ethernet.CloseUDP();
    if (i == 0) {Connected = 0; Disconnected = 1;}
    timer->start(time_ms);
}

void MainWindow::handlePBC_ContOn()
{
    qDebug() << "\nButton IntfOn clicked!";
    if (TesterOn == 1)
    {
        INTFOn = 0;
        CONTOn = 1;
        DUTPS_change = 1;
    }
    else
    {
        INTFOn = 0;
        CONTOn = 0;
        DUTPS_change = 1;
    }
}

void MainWindow::handlePBC_ContOff()
{
    qDebug() << "\nButton ContOff clicked!";
    INTFOn = 0;
    CONTOn = 0;
    DUTPS_change = 1;
}

void MainWindow::handlePBI_IntfOn()
{
    qDebug() << "\nButton IntfOn clicked!";
    if (TesterOn == 1)
    {
        INTFOn = 1;
        CONTOn = 0;
        DUTPS_change = 1;
    }
    else
    {
        INTFOn = 0;
        CONTOn = 0;
        DUTPS_change = 1;
    }
}

void MainWindow::handlePBI_IntfOff()
{
    qDebug() << "\nButton IntfOff clicked!";
    INTFOn = 0;
    CONTOn = 0;
    DUTPS_change = 1;
}

void MainWindow::handlePB_TesterOn()
{
    qDebug() << "\nButton TesterON clicked!";
    TesterOn = 1;
    TesterOn_change = 1;
    INTFOn = 0;
    CONTOn = 0;
    DUTPS_change = 1;
}

void MainWindow::handlePB_TesterOff()
{
    qDebug() << "\nButton TesterOFF clicked!";
    TesterOn = 0;
    TesterOn_change = 1;
    INTFOn = 0;
    CONTOn = 0;
    DUTPS_change = 1;
}

bool MainWindow::areEqual(const data_t &a, const data_t &b)
{
    int n;
    n=std::memcmp(&a, &b, sizeof(data_t));
    return n;
}

void MainWindow::RefreshUI()
{
    if (Responding == 1 && ACK_OK == 1)
    {
        ui->L_Connected->setText("Connected");
        ui->L_Connected->setStyleSheet("QLabel { background-color : green; color : black; }");
    }
    if (Responding == 0 && ACK_OK == 0)
    {
        ui->L_Connected->setText("Disconnected");
        ui->L_Connected->setStyleSheet("QLabel { background-color : red; color : black; }");
    }
    if (Responding == 1 && ACK_OK == 0)
    {
        ui->L_Connected->setText("COMM error");
        ui->L_Connected->setStyleSheet("QLabel { background-color : orange; color : black; }");
    }

    if (ACK_OK == 1 && TesterOn == 1)
    {
        ui->L_TesterON->setText("ON");
        ui->L_TesterON->setStyleSheet("QLabel { background-color : green; color : black; }");
    }
    if (ACK_OK == 1 && TesterOn == 0)
    {
        ui->L_TesterON->setText("OFF");
        ui->L_TesterON->setStyleSheet("QLabel { background-color : red; color : black; }");
    }

    if (ACK_OK == 1 && myData_ACK.CONTOn == 1)
    {
        ui->L_CONTON->setText("ON");
        ui->L_CONTON->setStyleSheet("QLabel { background-color : green; color : black; }");
    }
    if (ACK_OK == 1 && myData_ACK.CONTOn == 0)
    {
        ui->L_CONTON->setText("OFF");
        ui->L_CONTON->setStyleSheet("QLabel { background-color : red; color : black; }");
    }

    if (ACK_OK == 1 && myData_ACK.INTFOn == 1)
    {
        ui->L_INTFON->setText("ON");
        ui->L_INTFON->setStyleSheet("QLabel { background-color : green; color : black; }");
    }
    if (ACK_OK == 1 && myData_ACK.INTFOn == 0)
    {
        ui->L_INTFON->setText("OFF");
        ui->L_INTFON->setStyleSheet("QLabel { background-color : red; color : black; }");
    }

    if (Responding == 0)
    {
        ui->L_INTFON->setText("");
        ui->L_INTFON->setStyleSheet("QLabel { background-color : gray; color : black; }");
        ui->L_CONTON->setText("");
        ui->L_CONTON->setStyleSheet("QLabel { background-color : gray; color : black; }");
        ui->L_TesterON->setText("");
        ui->L_TesterON->setStyleSheet("QLabel { background-color : gray; color : black; }");
    }

    ui->M_V24V2_I->setText(QString::number(Measurements.meas0_0_0));
    ui->M_UPSOUT->setText(QString::number(Measurements.meas0_0_1));
    ui->M_UPCOUT->setText(QString::number(Measurements.meas0_0_2));

    ui->M_CI_2->setText(QString::number(Measurements.meas0_1_3));
    ui->M_MOT_2->setText(QString::number(Measurements.meas0_1_4));
    ui->M_I_PLC_2->setText(QString::number(Measurements.meas0_1_5));
    ui->M_CDB_1_3->setText(QString::number(Measurements.meas0_1_6));

    ui->M_CI_3->setText(QString::number(Measurements.meas1_1_3));
    ui->M_MOT_3->setText(QString::number(Measurements.meas1_1_4));
    ui->M_CDB_1_2->setText(QString::number(Measurements.meas1_1_5));
    ui->M_CDB_2_3->setText(QString::number(Measurements.meas1_1_6));

    ui->M_CI_4->setText(QString::number(Measurements.meas2_1_3));
    ui->M_MOT_6->setText(QString::number(Measurements.meas2_1_4));
    ui->M_CDB_2_2->setText(QString::number(Measurements.meas2_1_5));
    ui->M_CDB_3_3->setText(QString::number(Measurements.meas2_1_6));

    ui->M_CI_5->setText(QString::number(Measurements.meas3_1_3));
    ui->M_400V_OUT_2->setText(QString::number(Measurements.meas3_1_4));
    ui->M_CDB_3_2->setText(QString::number(Measurements.meas0_1_5));
    ui->M_CDB_4_3->setText(QString::number(Measurements.meas3_1_6));

    ui->M_CI_7->setText(QString::number(Measurements.meas4_1_3));
    ui->M_400V_OUT_3->setText(QString::number(Measurements.meas4_1_4));
    ui->M_CDB_4_2->setText(QString::number(Measurements.meas4_1_5));
    ui->M_CDB_5_3->setText(QString::number(Measurements.meas4_1_6));

    ui->M_CI_8->setText(QString::number(Measurements.meas5_1_3));
    ui->M_400V_OUT_5->setText(QString::number(Measurements.meas5_1_4));
    ui->M_CDB_5_2->setText(QString::number(Measurements.meas5_1_5));
    ui->M_CDB_6_3->setText(QString::number(Measurements.meas5_1_6));

    ui->M_CI_9->setText(QString::number(Measurements.meas6_1_3));
    ui->M_400V_OUT_6->setText(QString::number(Measurements.meas6_1_4));
    ui->M_CDB_6_2->setText(QString::number(Measurements.meas6_1_5));
    ui->M_CDB_7_3->setText(QString::number(Measurements.meas6_1_6));

    ui->M_C_PLC_2->setText(QString::number(Measurements.meas7_1_3));
    ////////
    ui->M_CDB_7_2->setText(QString::number(Measurements.meas7_1_5));
    ////////

    ui->M_C_PLC_3->setText(QString::number(Measurements.meas0_2_3));
    ////////
    ui->M_C_I_1_13->setText(QString::number(Measurements.meas0_2_5));
    ui->M_CDB_1_8->setText(QString::number(Measurements.meas0_2_6));

    ui->M_C_PLC_4->setText(QString::number(Measurements.meas1_2_3));
    ////////
    ui->M_C_I_2_13->setText(QString::number(Measurements.meas1_2_5));
    ui->M_CDB_2_8->setText(QString::number(Measurements.meas1_2_6));

    ui->M_C_PLC_5->setText(QString::number(Measurements.meas2_2_3));
    ////////
    ui->M_C_I_3_13->setText(QString::number(Measurements.meas2_2_5));
    ui->M_CDB_3_8->setText(QString::number(Measurements.meas2_2_6));

    ui->M_C_PLC_6->setText(QString::number(Measurements.meas3_2_3));
    ////////
    ui->M_C_I_4_13->setText(QString::number(Measurements.meas3_2_5));
    ui->M_CDB_4_8->setText(QString::number(Measurements.meas3_2_6));

    ui->M_C_PLC_7->setText(QString::number(Measurements.meas4_2_3));
    ////////
    ui->M_C_I_5_13->setText(QString::number(Measurements.meas4_2_5));
    ui->M_CDB_5_8->setText(QString::number(Measurements.meas4_2_6));

    ui->M_C_PLC_8->setText(QString::number(Measurements.meas5_2_3));
    ////////
    ui->M_C_I_6_13->setText(QString::number(Measurements.meas5_2_5));
    ui->M_CDB_6_8->setText(QString::number(Measurements.meas5_2_6));

    ui->M_C_PLC_9->setText(QString::number(Measurements.meas6_2_3));
    ////////
    ui->M_C_I_7_13->setText(QString::number(Measurements.meas6_2_5));
    ui->M_CDB_7_8->setText(QString::number(Measurements.meas6_2_6));

    ui->M_C_PLC_10->setText(QString::number(Measurements.meas7_2_3));
    ////////
    ////////
    ////////
}

void MainWindow::handlePB_SignalsReset()
{
    ui->LS_2->setChecked(false);
    ui->LS_3->setChecked(false);
    ui->LS_4->setChecked(false);
    ui->LS_5->setChecked(false);
    ui->LS_8->setChecked(false);
    ui->LS_9->setChecked(false);
    ui->LS_10->setChecked(false);
    ui->LS_11->setChecked(false);
    ui->LS_14->setChecked(false);
    ui->LS_15->setChecked(false);
    ui->LS_1617->setChecked(false);

    ui->ES1_2->setChecked(false);
    ui->ES1_3->setChecked(false);
    ui->ES1_4->setChecked(false);
    ui->ES1_5->setChecked(false);
    ui->ES1_6->setChecked(false);
    ui->ES1_7->setChecked(false);
    ui->ES1_8->setChecked(false);
    ui->ES1_9->setChecked(false);

    ui->ES2_2->setChecked(false);
    ui->ES2_3->setChecked(false);
    ui->ES2_4->setChecked(false);
    ui->ES2_5->setChecked(false);
    ui->ES2_6->setChecked(false);
    ui->ES2_7->setChecked(false);
    ui->ES2_8->setChecked(false);
    ui->ES2_9->setChecked(false);

    ui->C_I_1->setChecked(false);
    ui->C_I_1213->setChecked(false);
    ui->C_I_1516->setChecked(false);
    ui->C_I_1718->setChecked(false);

    ui->C_PLC_1->setChecked(false);

    ui->R->setChecked(false);
    ui->S->setChecked(false);
    ui->T->setChecked(false);
    ui->N->setChecked(false);


    ui->C_I1_2->setChecked(false);
    ui->C_I1_3->setChecked(false);
    ui->C_I1_4->setChecked(false);
    ui->C_I1_5->setChecked(false);
    ui->C_I1_7->setChecked(false);
    ui->C_I1_8->setChecked(false);
    ui->C_I1_9->setChecked(false);
    ui->C_I1_12->setChecked(false);
    ui->C_I1_1516->setChecked(false);
    ui->C_I1_1718->setChecked(false);

    ui->C_I2_2->setChecked(false);
    ui->C_I2_3->setChecked(false);
    ui->C_I2_4->setChecked(false);
    ui->C_I2_5->setChecked(false);
    ui->C_I2_7->setChecked(false);
    ui->C_I2_8->setChecked(false);
    ui->C_I2_9->setChecked(false);
    ui->C_I2_12->setChecked(false);
    ui->C_I2_1516->setChecked(false);
    ui->C_I2_1718->setChecked(false);

    ui->C_I3_2->setChecked(false);
    ui->C_I3_3->setChecked(false);
    ui->C_I3_4->setChecked(false);
    ui->C_I3_5->setChecked(false);
    ui->C_I3_7->setChecked(false);
    ui->C_I3_8->setChecked(false);
    ui->C_I3_9->setChecked(false);
    ui->C_I3_12->setChecked(false);
    ui->C_I3_1516->setChecked(false);
    ui->C_I3_1718->setChecked(false);

    ui->C_I4_2->setChecked(false);
    ui->C_I4_3->setChecked(false);
    ui->C_I4_4->setChecked(false);
    ui->C_I4_5->setChecked(false);
    ui->C_I4_7->setChecked(false);
    ui->C_I4_8->setChecked(false);
    ui->C_I4_9->setChecked(false);
    ui->C_I4_12->setChecked(false);
    ui->C_I4_1516->setChecked(false);
    ui->C_I4_1718->setChecked(false);

    ui->C_I5_2->setChecked(false);
    ui->C_I5_3->setChecked(false);
    ui->C_I5_4->setChecked(false);
    ui->C_I5_5->setChecked(false);
    ui->C_I5_7->setChecked(false);
    ui->C_I5_8->setChecked(false);
    ui->C_I5_9->setChecked(false);
    ui->C_I5_12->setChecked(false);
    ui->C_I5_1516->setChecked(false);
    ui->C_I5_1718->setChecked(false);

    ui->C_I6_2->setChecked(false);
    ui->C_I6_3->setChecked(false);
    ui->C_I6_4->setChecked(false);
    ui->C_I6_5->setChecked(false);
    ui->C_I6_7->setChecked(false);
    ui->C_I6_8->setChecked(false);
    ui->C_I6_9->setChecked(false);
    ui->C_I6_12->setChecked(false);
    ui->C_I6_1516->setChecked(false);
    ui->C_I6_1718->setChecked(false);

    ui->C_I7_2->setChecked(false);
    ui->C_I7_3->setChecked(false);
    ui->C_I7_4->setChecked(false);
    ui->C_I7_5->setChecked(false);
    ui->C_I7_7->setChecked(false);
    ui->C_I7_8->setChecked(false);
    ui->C_I7_9->setChecked(false);
    ui->C_I7_12->setChecked(false);
    ui->C_I7_1516->setChecked(false);
    ui->C_I7_1718->setChecked(false);

    ui->CDB1_0104->setChecked(false);
    ui->CDB1_7->setChecked(false);
    ui->CDB1_0506->setChecked(false);
    ui->CDB2_0104->setChecked(false);
    ui->CDB2_7->setChecked(false);
    ui->CDB2_0506->setChecked(false);
    ui->CDB3_0104->setChecked(false);
    ui->CDB3_7->setChecked(false);
    ui->CDB3_0506->setChecked(false);
    ui->CDB4_0104->setChecked(false);
    ui->CDB4_7->setChecked(false);
    ui->CDB4_0506->setChecked(false);
    ui->CDB5_0104->setChecked(false);
    ui->CDB5_7->setChecked(false);
    ui->CDB5_0506->setChecked(false);
    ui->CDB6_0104->setChecked(false);
    ui->CDB6_7->setChecked(false);
    ui->CDB6_0506->setChecked(false);
    ui->CDB7_0104->setChecked(false);
    ui->CDB7_7->setChecked(false);
    ui->CDB7_0506->setChecked(false);

    ui->I_PLC_1->setChecked(false);
    ui->I_PLC_0506->setChecked(false);
    ui->I_PLC_0708->setChecked(false);
    ui->I_PLC_0910->setChecked(false);
    ui->I_PLC_1112->setChecked(false);
    ui->I_PLC_1314->setChecked(false);
    ui->I_PLC_1516->setChecked(false);
    ui->I_PLC_1718->setChecked(false);

    ui->DOOR->setChecked(false);
    ui->STOP->setChecked(false);
}

void MainWindow::update_data()
{
    myData.INTFOn = INTFOn;
    myData.CONTOn = CONTOn;
    myData.DUTPS_change = DUTPS_change;
    myData.Connected = Connected;
    myData.Disconnected = Disconnected;
    myData.TesterOn = TesterOn;
    myData.TesterOn_change = TesterOn_change;

    myData.LS_2 = ui->LS_2->isChecked();
    myData.LS_3 = ui->LS_3->isChecked();
    myData.LS_4 = ui->LS_4->isChecked();
    myData.LS_5 = ui->LS_5->isChecked();
    myData.LS_8 = ui->LS_8->isChecked();
    myData.LS_9 = ui->LS_9->isChecked();
    myData.LS_10 = ui->LS_10->isChecked();
    myData.LS_11 = ui->LS_11->isChecked();
    myData.LS_14 = ui->LS_14->isChecked();
    myData.LS_15 = ui->LS_15->isChecked();
    myData.LS_1617 = ui->LS_1617->isChecked();

    myData.ES1_2 = ui->ES1_2->isChecked();
    myData.ES1_3 = ui->ES1_3->isChecked();
    myData.ES1_4 = ui->ES1_4->isChecked();
    myData.ES1_5 = ui->ES1_5->isChecked();
    myData.ES1_6 = ui->ES1_6->isChecked();
    myData.ES1_7 = ui->ES1_7->isChecked();
    myData.ES1_8 = ui->ES1_8->isChecked();
    myData.ES1_9 = ui->ES1_9->isChecked();

    myData.ES2_2 = ui->ES2_2->isChecked();
    myData.ES2_3 = ui->ES2_3->isChecked();
    myData.ES2_4 = ui->ES2_4->isChecked();
    myData.ES2_5 = ui->ES2_5->isChecked();
    myData.ES2_6 = ui->ES2_6->isChecked();
    myData.ES2_7 = ui->ES2_7->isChecked();
    myData.ES2_8 = ui->ES2_8->isChecked();
    myData.ES2_9 = ui->ES2_9->isChecked();

    myData.C_I_1 = ui->C_I_1->isChecked();
    myData.C_I_1213 = ui->C_I_1213->isChecked();
    myData.C_I_1516 = ui->C_I_1516->isChecked();
    myData.C_I_1718 = ui->C_I_1718->isChecked();

    myData.C_PLC_1 = ui->C_PLC_1->isChecked();

    myData.R = ui->R->isChecked();
    myData.S = ui->S->isChecked();
    myData.T = ui->T->isChecked();
    myData.N = ui->N->isChecked();


    myData.C_I1_2 = ui->C_I1_2->isChecked();
    myData.C_I1_3 = ui->C_I1_3->isChecked();
    myData.C_I1_4 = ui->C_I1_4->isChecked();
    myData.C_I1_5 = ui->C_I1_5->isChecked();
    myData.C_I1_7 = ui->C_I1_7->isChecked();
    myData.C_I1_8 = ui->C_I1_8->isChecked();
    myData.C_I1_9 = ui->C_I1_9->isChecked();
    myData.C_I1_12 = ui->C_I1_12->isChecked();
    myData.C_I1_1516 = ui->C_I1_1516->isChecked();
    myData.C_I1_1718 = ui->C_I1_1718->isChecked();

    myData.C_I2_2 = ui->C_I2_2->isChecked();
    myData.C_I2_3 = ui->C_I2_3->isChecked();
    myData.C_I2_4 = ui->C_I2_4->isChecked();
    myData.C_I2_5 = ui->C_I2_5->isChecked();
    myData.C_I2_7 = ui->C_I2_7->isChecked();
    myData.C_I2_8 = ui->C_I2_8->isChecked();
    myData.C_I2_9 = ui->C_I2_9->isChecked();
    myData.C_I2_12 = ui->C_I2_12->isChecked();
    myData.C_I2_1516 = ui->C_I2_1516->isChecked();
    myData.C_I2_1718 = ui->C_I2_1718->isChecked();

    myData.C_I3_2 = ui->C_I3_2->isChecked();
    myData.C_I3_3 = ui->C_I3_3->isChecked();
    myData.C_I3_4 = ui->C_I3_4->isChecked();
    myData.C_I3_5 = ui->C_I3_5->isChecked();
    myData.C_I3_7 = ui->C_I3_7->isChecked();
    myData.C_I3_8 = ui->C_I3_8->isChecked();
    myData.C_I3_9 = ui->C_I3_9->isChecked();
    myData.C_I3_12 = ui->C_I3_12->isChecked();
    myData.C_I3_1516 = ui->C_I3_1516->isChecked();
    myData.C_I3_1718 = ui->C_I3_1718->isChecked();

    myData.C_I4_2 = ui->C_I4_2->isChecked();
    myData.C_I4_3 = ui->C_I4_3->isChecked();
    myData.C_I4_4 = ui->C_I4_4->isChecked();
    myData.C_I4_5 = ui->C_I4_5->isChecked();
    myData.C_I4_7 = ui->C_I4_7->isChecked();
    myData.C_I4_8 = ui->C_I4_8->isChecked();
    myData.C_I4_9 = ui->C_I4_9->isChecked();
    myData.C_I4_12 = ui->C_I4_12->isChecked();
    myData.C_I4_1516 = ui->C_I4_1516->isChecked();
    myData.C_I4_1718 = ui->C_I4_1718->isChecked();

    myData.C_I5_2 = ui->C_I5_2->isChecked();
    myData.C_I5_3 = ui->C_I5_3->isChecked();
    myData.C_I5_4 = ui->C_I5_4->isChecked();
    myData.C_I5_5 = ui->C_I5_5->isChecked();
    myData.C_I5_7 = ui->C_I5_7->isChecked();
    myData.C_I5_8 = ui->C_I5_8->isChecked();
    myData.C_I5_9 = ui->C_I5_9->isChecked();
    myData.C_I5_12 = ui->C_I5_12->isChecked();
    myData.C_I5_1516 = ui->C_I5_1516->isChecked();
    myData.C_I5_1718 = ui->C_I5_1718->isChecked();

    myData.C_I6_2 = ui->C_I6_2->isChecked();
    myData.C_I6_3 = ui->C_I6_3->isChecked();
    myData.C_I6_4 = ui->C_I6_4->isChecked();
    myData.C_I6_5 = ui->C_I6_5->isChecked();
    myData.C_I6_7 = ui->C_I6_7->isChecked();
    myData.C_I6_8 = ui->C_I6_8->isChecked();
    myData.C_I6_9 = ui->C_I6_9->isChecked();
    myData.C_I6_12 = ui->C_I6_12->isChecked();
    myData.C_I6_1516 = ui->C_I6_1516->isChecked();
    myData.C_I6_1718 = ui->C_I6_1718->isChecked();

    myData.C_I7_2 = ui->C_I7_2->isChecked();
    myData.C_I7_3 = ui->C_I7_3->isChecked();
    myData.C_I7_4 = ui->C_I7_4->isChecked();
    myData.C_I7_5 = ui->C_I7_5->isChecked();
    myData.C_I7_7 = ui->C_I7_7->isChecked();
    myData.C_I7_8 = ui->C_I7_8->isChecked();
    myData.C_I7_9 = ui->C_I7_9->isChecked();
    myData.C_I7_12 = ui->C_I7_12->isChecked();
    myData.C_I7_1516 = ui->C_I7_1516->isChecked();
    myData.C_I7_1718 = ui->C_I7_1718->isChecked();

    myData.CDB1_0104 = ui->CDB1_0104->isChecked();
    myData.CDB1_7 = ui->CDB1_7->isChecked();
    myData.CDB1_0506 = ui->CDB1_0506->isChecked();
    myData.CDB2_0104 = ui->CDB2_0104->isChecked();
    myData.CDB2_7 = ui->CDB2_7->isChecked();
    myData.CDB2_0506 = ui->CDB2_0506->isChecked();
    myData.CDB3_0104 = ui->CDB3_0104->isChecked();
    myData.CDB3_7 = ui->CDB3_7->isChecked();
    myData.CDB3_0506 = ui->CDB3_0506->isChecked();
    myData.CDB4_0104 = ui->CDB4_0104->isChecked();
    myData.CDB4_7 = ui->CDB4_7->isChecked();
    myData.CDB4_0506 = ui->CDB4_0506->isChecked();
    myData.CDB5_0104 = ui->CDB5_0104->isChecked();
    myData.CDB5_7 = ui->CDB5_7->isChecked();
    myData.CDB5_0506 = ui->CDB5_0506->isChecked();
    myData.CDB6_0104 = ui->CDB6_0104->isChecked();
    myData.CDB6_7 = ui->CDB6_7->isChecked();
    myData.CDB6_0506 = ui->CDB6_0506->isChecked();
    myData.CDB7_0104 = ui->CDB7_0104->isChecked();
    myData.CDB7_7 = ui->CDB7_7->isChecked();
    myData.CDB7_0506 = ui->CDB7_0506->isChecked();

    myData.I_PLC_1 = ui->I_PLC_1->isChecked();
    myData.I_PLC_0506 = ui->I_PLC_0506->isChecked();
    myData.I_PLC_0708 = ui->I_PLC_0708->isChecked();
    myData.I_PLC_0910 = ui->I_PLC_0910->isChecked();
    myData.I_PLC_1112 = ui->I_PLC_1112->isChecked();
    myData.I_PLC_1314 = ui->I_PLC_1314->isChecked();
    myData.I_PLC_1516 = ui->I_PLC_1516->isChecked();
    myData.I_PLC_1718 = ui->I_PLC_1718->isChecked();

    myData.DOOR = ui->DOOR->isChecked();
    myData.STOP = ui->STOP->isChecked();
}
