#ifndef ETHERNET_H_
#define ETHERNET_H_

#include <QUdpSocket>
#include <QTimer>
#include "data_t_structure.h"
#include "data_m_structure.h"


class ETH : public QObject
{
    Q_OBJECT

private:
    QUdpSocket *udpSocket;  // Pointer to QUdpSocket

public:
    ETH();
    ~ETH();

    int OpenUDP();
    int CloseUDP();
    int Send_Data_t(data_t &message);
    data_t Receive_Data_t();
    data_m Receive_Data_m();

    void EthDelay(int milliseconds);

    QHostAddress sender;
    quint16 senderPort;
    QByteArray receivedData;

    QString servAddrSTR;
    quint16 servPort;
    QHostAddress servAddr;

};

#endif // ETHERNET_H_
