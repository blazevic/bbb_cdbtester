#ifndef DATA_M_STRUCTURE_H
#define DATA_M_STRUCTURE_H

typedef struct {

    unsigned int meas0_0_0;
    unsigned int meas0_0_1;
    unsigned int meas0_0_2;

    unsigned int meas0_1_3;
    unsigned int meas0_1_4;
    unsigned int meas0_1_5;
    unsigned int meas0_1_6;

    unsigned int meas1_1_3;
    unsigned int meas1_1_4;
    unsigned int meas1_1_5;
    unsigned int meas1_1_6;

    unsigned int meas2_1_3;
    unsigned int meas2_1_4;
    unsigned int meas2_1_5;
    unsigned int meas2_1_6;

    unsigned int meas3_1_3;
    unsigned int meas3_1_4;
    unsigned int meas3_1_5;
    unsigned int meas3_1_6;

    unsigned int meas4_1_3;
    unsigned int meas4_1_4;
    unsigned int meas4_1_5;
    unsigned int meas4_1_6;

    unsigned int meas5_1_3;
    unsigned int meas5_1_4;
    unsigned int meas5_1_5;
    unsigned int meas5_1_6;

    unsigned int meas6_1_3;
    unsigned int meas6_1_4;
    unsigned int meas6_1_5;
    unsigned int meas6_1_6;

    unsigned int meas7_1_3;
    unsigned int meas7_1_4;
    unsigned int meas7_1_5;
    unsigned int meas7_1_6;

    unsigned int meas0_2_3;
    unsigned int meas0_2_4;
    unsigned int meas0_2_5;
    unsigned int meas0_2_6;

    unsigned int meas1_2_3;
    unsigned int meas1_2_4;
    unsigned int meas1_2_5;
    unsigned int meas1_2_6;

    unsigned int meas2_2_3;
    unsigned int meas2_2_4;
    unsigned int meas2_2_5;
    unsigned int meas2_2_6;

    unsigned int meas3_2_3;
    unsigned int meas3_2_4;
    unsigned int meas3_2_5;
    unsigned int meas3_2_6;

    unsigned int meas4_2_3;
    unsigned int meas4_2_4;
    unsigned int meas4_2_5;
    unsigned int meas4_2_6;

    unsigned int meas5_2_3;
    unsigned int meas5_2_4;
    unsigned int meas5_2_5;
    unsigned int meas5_2_6;

    unsigned int meas6_2_3;
    unsigned int meas6_2_4;
    unsigned int meas6_2_5;
    unsigned int meas6_2_6;

    unsigned int meas7_2_3;
    unsigned int meas7_2_4;
    unsigned int meas7_2_5;
    unsigned int meas7_2_6;

}data_m;

#endif // DATA_M_STRUCTURE_H
