#include "Ethernet.h"
#include <QApplication>
#include <QDebug>
#include <QTimer>
#include <QEventLoop>
#include <QUdpSocket>


// Create UDP socket for sending and receiving messages

ETH::ETH()
{
    udpSocket = new QUdpSocket; // Allocate QUdpSocket dynamically
}

ETH::~ETH()
{
    CloseUDP();
    delete udpSocket; // Free the memory
}


int ETH::OpenUDP()
{
    CloseUDP();

    if (servAddr.setAddress(servAddrSTR)) {
        qDebug() << "Valid IP address:" << servAddr;
    } else {
        qDebug() << "Invalid IP address\nUDP socket not bound.";
        return 1;
    }

    qDebug() << "Attempting to bind UDP socket to address:" << QHostAddress::Any << "port:" << servPort;

    // Bind to any address
    if (!udpSocket->bind(QHostAddress::Any, servPort))
    {
        qDebug() << "Failed to bind UDP socket.\nUDP socket not bound.";
        return 1;
    }
    qDebug() << "UDP socket successfully bound to address:" << QHostAddress::Any << "port:" << servPort;
    return 0;
}

int ETH::CloseUDP()
{
    if (udpSocket->state() == QAbstractSocket::BoundState)
    {
        udpSocket->close();
        qDebug() << "UDP socket closed.";
    }
    else {qDebug() << "UDP socket doesnt exist.";}
    return 0;
}

int ETH::Send_Data_t(data_t &message)
{
    qDebug() << "\n\n" << udpSocket->errorString();
    while (udpSocket->hasPendingDatagrams())
    {
        qint64 bytesRead = udpSocket->readDatagram(receivedData.data(), receivedData.size(), &sender, &senderPort);
    }

    QByteArray byteArray(reinterpret_cast<const char*>(&message), sizeof(message));
    qint64 bytesWritten = udpSocket->writeDatagram(byteArray, servAddr, servPort);
    if (bytesWritten == -1)
    {
        qDebug() << "Failed to send UDP message. Error:" << udpSocket->errorString();
    }
    else
    {
        qDebug() << "Data_t sent: " << byteArray;
    }
    return 0;
}

data_t ETH::Receive_Data_t()
{
    data_t message;
    memset (&message, 0, sizeof(message));
    qint64 bytesRead = -1;
    if(!udpSocket->waitForReadyRead(50))
    {
        qDebug() << "Failed to receive Data_t. Timeout !";
        return message;
    }

    if (udpSocket->hasPendingDatagrams())
    {
    receivedData.resize(udpSocket->pendingDatagramSize());
    bytesRead = udpSocket->readDatagram(receivedData.data(), receivedData.size(), &sender, &senderPort);

    if (bytesRead == -1)
    {
        qDebug() << "Failed to receive Data_t. Error:" << udpSocket->errorString();
    }
    else
    {
        qDebug() << "Data_t recv: " << receivedData;
    }
    memcpy(&message, receivedData.constData(), sizeof(receivedData));
    }
    return message;
}

data_m ETH::Receive_Data_m()
{
    data_m message;
    memset (&message, 0, sizeof(message));
    qint64 bytesRead = -1;
    if(!udpSocket->waitForReadyRead(250))
    {
        qDebug() << "Failed to receive data_m. Timeout !";
        return message;
    }

    if (udpSocket->hasPendingDatagrams())
    {
        receivedData.resize(udpSocket->pendingDatagramSize());
        bytesRead = udpSocket->readDatagram(receivedData.data(), receivedData.size(), &sender, &senderPort);

        if (bytesRead == -1)
        {
            qDebug() << "Failed to receive data_m. Error:" << udpSocket->errorString();
        }
        else
        {
            qDebug() << "Data_m recv: " << receivedData;
        }
        memcpy(&message, receivedData.constData(), sizeof(receivedData));
    }
    return message;
}

void ETH::EthDelay(int milliseconds)
{
    QEventLoop loop;
    QTimer::singleShot(milliseconds, &loop, &QEventLoop::quit);
    loop.exec();
}
