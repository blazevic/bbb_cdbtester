# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "RelWithDebInfo")
  file(REMOVE_RECURSE
  "CDB_GUI_autogen"
  "CMakeFiles\\CDB_GUI_autogen.dir\\AutogenUsed.txt"
  "CMakeFiles\\CDB_GUI_autogen.dir\\ParseCache.txt"
  )
endif()
