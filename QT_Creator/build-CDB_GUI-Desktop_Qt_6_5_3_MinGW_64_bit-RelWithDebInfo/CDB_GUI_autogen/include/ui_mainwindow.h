/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.5.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actiondada;
    QWidget *centralwidget;
    QLabel *ProgramName;
    QGroupBox *Ethernet;
    QPushButton *PBConnect;
    QPushButton *PBDisconnect;
    QLineEdit *TextIP;
    QLabel *label_2;
    QLabel *label;
    QLineEdit *TextPort;
    QLabel *L_Connected;
    QTabWidget *DutTestTabs;
    QWidget *ControlTab;
    QGroupBox *C_PLC;
    QVBoxLayout *verticalLayout_23;
    QLabel *label_48;
    QLabel *label_55;
    QCheckBox *C_PLC_1;
    QGroupBox *MatrixRSTN;
    QGridLayout *gridLayout_4;
    QCheckBox *N;
    QCheckBox *T;
    QCheckBox *S;
    QCheckBox *R;
    QLabel *label_56;
    QPushButton *PBCONTOn;
    QGroupBox *C_I;
    QVBoxLayout *verticalLayout_24;
    QLabel *label_58;
    QLabel *label_60;
    QCheckBox *C_I_1;
    QLabel *label_59;
    QCheckBox *C_I_1213;
    QLabel *label_61;
    QCheckBox *C_I_1516;
    QCheckBox *C_I_1718;
    QGroupBox *LS;
    QGridLayout *gridLayout_3;
    QLabel *label_54;
    QGroupBox *LSSS;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_62;
    QCheckBox *LS_9;
    QCheckBox *LS_10;
    QCheckBox *LS_11;
    QCheckBox *LS_14;
    QCheckBox *LS_15;
    QGroupBox *LSS;
    QVBoxLayout *verticalLayout_10;
    QLabel *label_53;
    QCheckBox *LS_2;
    QCheckBox *LS_3;
    QCheckBox *LS_4;
    QCheckBox *LS_5;
    QCheckBox *LS_8;
    QGroupBox *LSSSS;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_57;
    QCheckBox *LS_1617;
    QGroupBox *SwitchES;
    QHBoxLayout *horizontalLayout_4;
    QGroupBox *ES1;
    QVBoxLayout *verticalLayout_9;
    QLabel *label_50;
    QLabel *label_49;
    QCheckBox *ES1_2;
    QCheckBox *ES1_3;
    QCheckBox *ES1_4;
    QCheckBox *ES1_5;
    QCheckBox *ES1_6;
    QCheckBox *ES1_7;
    QCheckBox *ES1_8;
    QCheckBox *ES1_9;
    QGroupBox *ES2;
    QVBoxLayout *verticalLayout_19;
    QLabel *label_51;
    QLabel *label_52;
    QCheckBox *ES2_2;
    QCheckBox *ES2_3;
    QCheckBox *ES2_4;
    QCheckBox *ES2_5;
    QCheckBox *ES2_6;
    QCheckBox *ES2_7;
    QCheckBox *ES2_8;
    QCheckBox *ES2_9;
    QPushButton *PBCONTOff;
    QLabel *L_CONTON;
    QLabel *label_64;
    QLabel *M_UPSOUT;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_142;
    QLabel *label_143;
    QLabel *label_138;
    QLabel *label_144;
    QLabel *label_145;
    QLabel *label_140;
    QLabel *label_137;
    QLabel *label_139;
    QLabel *label_141;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_11;
    QLabel *M_C_PLC_2;
    QLabel *M_C_PLC_3;
    QLabel *M_C_PLC_4;
    QLabel *M_C_PLC_5;
    QLabel *M_C_PLC_6;
    QLabel *M_C_PLC_7;
    QLabel *M_C_PLC_8;
    QLabel *M_C_PLC_9;
    QLabel *M_C_PLC_10;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_26;
    QLabel *label_146;
    QLabel *label_147;
    QLabel *label_148;
    QLabel *label_149;
    QLabel *label_150;
    QLabel *label_151;
    QLabel *label_152;
    QWidget *layoutWidget3;
    QVBoxLayout *verticalLayout_27;
    QLabel *M_MOT_2;
    QLabel *M_MOT_3;
    QLabel *M_MOT_6;
    QLabel *M_400V_OUT_2;
    QLabel *M_400V_OUT_3;
    QLabel *M_400V_OUT_5;
    QLabel *M_400V_OUT_6;
    QWidget *layoutWidget4;
    QVBoxLayout *verticalLayout_13;
    QLabel *label_129;
    QLabel *label_131;
    QLabel *label_132;
    QLabel *label_133;
    QLabel *label_134;
    QLabel *label_135;
    QLabel *label_136;
    QWidget *layoutWidget5;
    QVBoxLayout *verticalLayout_12;
    QLabel *M_CI_2;
    QLabel *M_CI_3;
    QLabel *M_CI_4;
    QLabel *M_CI_5;
    QLabel *M_CI_7;
    QLabel *M_CI_8;
    QLabel *M_CI_9;
    QWidget *InterfaceTab;
    QGroupBox *I_PLC;
    QVBoxLayout *verticalLayout_22;
    QLabel *label_34;
    QLabel *label_35;
    QCheckBox *I_PLC_1;
    QLabel *label_36;
    QCheckBox *I_PLC_0506;
    QCheckBox *I_PLC_0708;
    QCheckBox *I_PLC_0910;
    QCheckBox *I_PLC_1112;
    QCheckBox *I_PLC_1314;
    QCheckBox *I_PLC_1516;
    QCheckBox *I_PLC_1718;
    QGroupBox *CII;
    QHBoxLayout *horizontalLayout;
    QGroupBox *C_I1;
    QVBoxLayout *verticalLayout;
    QLabel *label_3;
    QLabel *label_41;
    QCheckBox *C_I1_2;
    QCheckBox *C_I1_3;
    QCheckBox *C_I1_4;
    QCheckBox *C_I1_5;
    QCheckBox *C_I1_7;
    QCheckBox *C_I1_8;
    QCheckBox *C_I1_9;
    QCheckBox *C_I1_12;
    QLabel *label_25;
    QCheckBox *C_I1_1516;
    QCheckBox *C_I1_1718;
    QGroupBox *C_I2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_4;
    QLabel *label_42;
    QCheckBox *C_I2_2;
    QCheckBox *C_I2_3;
    QCheckBox *C_I2_4;
    QCheckBox *C_I2_5;
    QCheckBox *C_I2_7;
    QCheckBox *C_I2_8;
    QCheckBox *C_I2_9;
    QCheckBox *C_I2_12;
    QLabel *label_26;
    QCheckBox *C_I2_1516;
    QCheckBox *C_I2_1718;
    QGroupBox *C_I3;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_5;
    QLabel *label_43;
    QCheckBox *C_I3_2;
    QCheckBox *C_I3_3;
    QCheckBox *C_I3_4;
    QCheckBox *C_I3_5;
    QCheckBox *C_I3_7;
    QCheckBox *C_I3_8;
    QCheckBox *C_I3_9;
    QCheckBox *C_I3_12;
    QLabel *label_27;
    QCheckBox *C_I3_1516;
    QCheckBox *C_I3_1718;
    QGroupBox *C_I4;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_6;
    QLabel *label_46;
    QCheckBox *C_I4_2;
    QCheckBox *C_I4_3;
    QCheckBox *C_I4_4;
    QCheckBox *C_I4_5;
    QCheckBox *C_I4_7;
    QCheckBox *C_I4_8;
    QCheckBox *C_I4_9;
    QCheckBox *C_I4_12;
    QLabel *label_39;
    QCheckBox *C_I4_1516;
    QCheckBox *C_I4_1718;
    QGroupBox *C_I5;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_7;
    QLabel *label_44;
    QCheckBox *C_I5_2;
    QCheckBox *C_I5_3;
    QCheckBox *C_I5_4;
    QCheckBox *C_I5_5;
    QCheckBox *C_I5_7;
    QCheckBox *C_I5_8;
    QCheckBox *C_I5_9;
    QCheckBox *C_I5_12;
    QLabel *label_37;
    QCheckBox *C_I5_1516;
    QCheckBox *C_I5_1718;
    QGroupBox *C_I6;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_8;
    QLabel *label_45;
    QCheckBox *C_I6_2;
    QCheckBox *C_I6_3;
    QCheckBox *C_I6_4;
    QCheckBox *C_I6_5;
    QCheckBox *C_I6_7;
    QCheckBox *C_I6_8;
    QCheckBox *C_I6_9;
    QCheckBox *C_I6_12;
    QLabel *label_38;
    QCheckBox *C_I6_1516;
    QCheckBox *C_I6_1718;
    QGroupBox *C_I7;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_9;
    QLabel *label_47;
    QCheckBox *C_I7_2;
    QCheckBox *C_I7_3;
    QCheckBox *C_I7_4;
    QCheckBox *C_I7_5;
    QCheckBox *C_I7_7;
    QCheckBox *C_I7_8;
    QCheckBox *C_I7_9;
    QCheckBox *C_I7_12;
    QLabel *label_40;
    QCheckBox *C_I7_1516;
    QCheckBox *C_I7_1718;
    QPushButton *PBINTFOn;
    QGroupBox *UserINTK;
    QVBoxLayout *verticalLayout_25;
    QCheckBox *DOOR;
    QCheckBox *STOP;
    QGroupBox *CDB;
    QHBoxLayout *horizontalLayout_2;
    QGroupBox *CDB1;
    QVBoxLayout *verticalLayout_14;
    QLabel *label_10;
    QLabel *label_11;
    QCheckBox *CDB1_0104;
    QCheckBox *CDB1_7;
    QLabel *label_12;
    QCheckBox *CDB1_0506;
    QGroupBox *CDB2;
    QVBoxLayout *verticalLayout_15;
    QLabel *label_13;
    QLabel *label_14;
    QCheckBox *CDB2_0104;
    QCheckBox *CDB2_7;
    QLabel *label_15;
    QCheckBox *CDB2_0506;
    QGroupBox *CDB3;
    QVBoxLayout *verticalLayout_16;
    QLabel *label_16;
    QLabel *label_17;
    QCheckBox *CDB3_0104;
    QCheckBox *CDB3_7;
    QLabel *label_18;
    QCheckBox *CDB3_0506;
    QGroupBox *CDB4;
    QVBoxLayout *verticalLayout_17;
    QLabel *label_19;
    QLabel *label_20;
    QCheckBox *CDB4_0104;
    QCheckBox *CDB4_7;
    QLabel *label_21;
    QCheckBox *CDB4_0506;
    QGroupBox *CDB5;
    QVBoxLayout *verticalLayout_18;
    QLabel *label_22;
    QLabel *label_23;
    QCheckBox *CDB5_0104;
    QCheckBox *CDB5_7;
    QLabel *label_24;
    QCheckBox *CDB5_0506;
    QGroupBox *CDB6;
    QVBoxLayout *verticalLayout_20;
    QLabel *label_28;
    QLabel *label_29;
    QCheckBox *CDB6_0104;
    QCheckBox *CDB6_7;
    QLabel *label_30;
    QCheckBox *CDB6_0506;
    QGroupBox *CDB7;
    QVBoxLayout *verticalLayout_21;
    QLabel *label_31;
    QLabel *label_32;
    QCheckBox *CDB7_0104;
    QCheckBox *CDB7_7;
    QLabel *label_33;
    QCheckBox *CDB7_0506;
    QPushButton *PBINTFOff;
    QLabel *L_INTFON;
    QWidget *layoutWidget6;
    QVBoxLayout *verticalLayout_29;
    QLabel *label_154;
    QLabel *label_155;
    QLabel *label_156;
    QLabel *label_157;
    QLabel *label_158;
    QLabel *label_159;
    QLabel *label_160;
    QWidget *layoutWidget7;
    QVBoxLayout *verticalLayout_28;
    QLabel *M_C_I_1_13;
    QLabel *M_C_I_2_13;
    QLabel *M_C_I_3_13;
    QLabel *M_C_I_4_13;
    QLabel *M_C_I_5_13;
    QLabel *M_C_I_6_13;
    QLabel *M_C_I_7_13;
    QWidget *layoutWidget8;
    QVBoxLayout *verticalLayout_30;
    QLabel *M_CDB_1_2;
    QLabel *M_CDB_2_2;
    QLabel *M_CDB_3_2;
    QLabel *M_CDB_4_2;
    QLabel *M_CDB_5_2;
    QLabel *M_CDB_6_2;
    QLabel *M_CDB_7_2;
    QWidget *layoutWidget_2;
    QVBoxLayout *verticalLayout_31;
    QLabel *label_161;
    QLabel *label_162;
    QLabel *label_163;
    QLabel *label_164;
    QLabel *label_165;
    QLabel *label_166;
    QLabel *label_167;
    QWidget *layoutWidget_4;
    QVBoxLayout *verticalLayout_33;
    QLabel *label_168;
    QLabel *label_169;
    QLabel *label_170;
    QLabel *label_171;
    QLabel *label_172;
    QLabel *label_173;
    QLabel *label_174;
    QWidget *layoutWidget_3;
    QVBoxLayout *verticalLayout_32;
    QLabel *M_CDB_1_3;
    QLabel *M_CDB_2_3;
    QLabel *M_CDB_3_3;
    QLabel *M_CDB_4_3;
    QLabel *M_CDB_5_3;
    QLabel *M_CDB_6_3;
    QLabel *M_CDB_7_3;
    QWidget *layoutWidget_5;
    QVBoxLayout *verticalLayout_34;
    QLabel *label_175;
    QLabel *label_176;
    QLabel *label_177;
    QLabel *label_178;
    QLabel *label_179;
    QLabel *label_180;
    QLabel *label_181;
    QWidget *layoutWidget_6;
    QVBoxLayout *verticalLayout_35;
    QLabel *M_CDB_1_8;
    QLabel *M_CDB_2_8;
    QLabel *M_CDB_3_8;
    QLabel *M_CDB_4_8;
    QLabel *M_CDB_5_8;
    QLabel *M_CDB_6_8;
    QLabel *M_CDB_7_8;
    QLabel *label_153;
    QLabel *M_UPCOUT;
    QLabel *M_I_PLC_2;
    QLabel *label_130;
    QWidget *tab_3;
    QGroupBox *groupBox_3;
    QPushButton *pushButton_11;
    QPushButton *pushButton_12;
    QPushButton *pushButton_13;
    QPushButton *pushButton_14;
    QPushButton *pushButton_15;
    QPushButton *pushButton_16;
    QPushButton *pushButton_17;
    QPushButton *pushButton_18;
    QProgressBar *progressBar_9;
    QProgressBar *progressBar_10;
    QProgressBar *progressBar_11;
    QProgressBar *progressBar_12;
    QProgressBar *progressBar_13;
    QProgressBar *progressBar_14;
    QProgressBar *progressBar_15;
    QProgressBar *progressBar_16;
    QWidget *tab_4;
    QGroupBox *groupBox;
    QPushButton *pushButton_6;
    QPushButton *pushButton_3;
    QPushButton *pushButton_5;
    QPushButton *pushButton_4;
    QPushButton *pushButton_10;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QPushButton *pushButton_7;
    QProgressBar *progressBar;
    QProgressBar *progressBar_2;
    QProgressBar *progressBar_3;
    QProgressBar *progressBar_4;
    QProgressBar *progressBar_5;
    QProgressBar *progressBar_6;
    QProgressBar *progressBar_7;
    QProgressBar *progressBar_8;
    QWidget *tab;
    QLabel *label_65;
    QGroupBox *Power;
    QPushButton *PBTesterOFF;
    QPushButton *PBTesterON;
    QLabel *L_TesterON;
    QGroupBox *TestSelect;
    QPushButton *PBINTFtest;
    QPushButton *PBCONTtest;
    QPushButton *PBSignalsReset;
    QLabel *label_63;
    QLabel *M_V24V2_I;
    QStatusBar *statusbar;
    QMenuBar *menubar;
    QMenu *menuMain;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(1524, 675);
        actiondada = new QAction(MainWindow);
        actiondada->setObjectName("actiondada");
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName("centralwidget");
        ProgramName = new QLabel(centralwidget);
        ProgramName->setObjectName("ProgramName");
        ProgramName->setGeometry(QRect(320, 0, 81, 20));
        Ethernet = new QGroupBox(centralwidget);
        Ethernet->setObjectName("Ethernet");
        Ethernet->setGeometry(QRect(10, 30, 381, 81));
        PBConnect = new QPushButton(Ethernet);
        PBConnect->setObjectName("PBConnect");
        PBConnect->setGeometry(QRect(20, 20, 75, 24));
        PBDisconnect = new QPushButton(Ethernet);
        PBDisconnect->setObjectName("PBDisconnect");
        PBDisconnect->setGeometry(QRect(20, 50, 75, 24));
        TextIP = new QLineEdit(Ethernet);
        TextIP->setObjectName("TextIP");
        TextIP->setGeometry(QRect(130, 30, 91, 22));
        label_2 = new QLabel(Ethernet);
        label_2->setObjectName("label_2");
        label_2->setGeometry(QRect(100, 50, 21, 16));
        label = new QLabel(Ethernet);
        label->setObjectName("label");
        label->setGeometry(QRect(100, 30, 21, 16));
        TextPort = new QLineEdit(Ethernet);
        TextPort->setObjectName("TextPort");
        TextPort->setGeometry(QRect(130, 50, 91, 22));
        L_Connected = new QLabel(Ethernet);
        L_Connected->setObjectName("L_Connected");
        L_Connected->setGeometry(QRect(230, 30, 141, 41));
        QFont font;
        font.setPointSize(18);
        font.setBold(true);
        L_Connected->setFont(font);
        L_Connected->setStyleSheet(QString::fromUtf8(""));
        L_Connected->setFrameShape(QFrame::Box);
        L_Connected->setAlignment(Qt::AlignCenter);
        DutTestTabs = new QTabWidget(centralwidget);
        DutTestTabs->setObjectName("DutTestTabs");
        DutTestTabs->setGeometry(QRect(0, 110, 1491, 511));
        ControlTab = new QWidget();
        ControlTab->setObjectName("ControlTab");
        C_PLC = new QGroupBox(ControlTab);
        C_PLC->setObjectName("C_PLC");
        C_PLC->setGeometry(QRect(330, 10, 71, 81));
        verticalLayout_23 = new QVBoxLayout(C_PLC);
        verticalLayout_23->setObjectName("verticalLayout_23");
        label_48 = new QLabel(C_PLC);
        label_48->setObjectName("label_48");

        verticalLayout_23->addWidget(label_48);

        label_55 = new QLabel(C_PLC);
        label_55->setObjectName("label_55");

        verticalLayout_23->addWidget(label_55);

        C_PLC_1 = new QCheckBox(C_PLC);
        C_PLC_1->setObjectName("C_PLC_1");

        verticalLayout_23->addWidget(C_PLC_1);

        MatrixRSTN = new QGroupBox(ControlTab);
        MatrixRSTN->setObjectName("MatrixRSTN");
        MatrixRSTN->setGeometry(QRect(420, 10, 101, 81));
        gridLayout_4 = new QGridLayout(MatrixRSTN);
        gridLayout_4->setObjectName("gridLayout_4");
        N = new QCheckBox(MatrixRSTN);
        N->setObjectName("N");

        gridLayout_4->addWidget(N, 2, 1, 1, 1);

        T = new QCheckBox(MatrixRSTN);
        T->setObjectName("T");

        gridLayout_4->addWidget(T, 1, 1, 1, 1);

        S = new QCheckBox(MatrixRSTN);
        S->setObjectName("S");

        gridLayout_4->addWidget(S, 2, 0, 1, 1);

        R = new QCheckBox(MatrixRSTN);
        R->setObjectName("R");

        gridLayout_4->addWidget(R, 1, 0, 1, 1);

        label_56 = new QLabel(MatrixRSTN);
        label_56->setObjectName("label_56");

        gridLayout_4->addWidget(label_56, 0, 0, 1, 1);

        PBCONTOn = new QPushButton(ControlTab);
        PBCONTOn->setObjectName("PBCONTOn");
        PBCONTOn->setGeometry(QRect(30, 30, 100, 41));
        PBCONTOn->setMinimumSize(QSize(100, 20));
        C_I = new QGroupBox(ControlTab);
        C_I->setObjectName("C_I");
        C_I->setGeometry(QRect(452, 110, 101, 232));
        verticalLayout_24 = new QVBoxLayout(C_I);
        verticalLayout_24->setObjectName("verticalLayout_24");
        label_58 = new QLabel(C_I);
        label_58->setObjectName("label_58");

        verticalLayout_24->addWidget(label_58);

        label_60 = new QLabel(C_I);
        label_60->setObjectName("label_60");

        verticalLayout_24->addWidget(label_60);

        C_I_1 = new QCheckBox(C_I);
        C_I_1->setObjectName("C_I_1");

        verticalLayout_24->addWidget(C_I_1);

        label_59 = new QLabel(C_I);
        label_59->setObjectName("label_59");

        verticalLayout_24->addWidget(label_59);

        C_I_1213 = new QCheckBox(C_I);
        C_I_1213->setObjectName("C_I_1213");

        verticalLayout_24->addWidget(C_I_1213);

        label_61 = new QLabel(C_I);
        label_61->setObjectName("label_61");

        verticalLayout_24->addWidget(label_61);

        C_I_1516 = new QCheckBox(C_I);
        C_I_1516->setObjectName("C_I_1516");

        verticalLayout_24->addWidget(C_I_1516);

        C_I_1718 = new QCheckBox(C_I);
        C_I_1718->setObjectName("C_I_1718");

        verticalLayout_24->addWidget(C_I_1718);

        LS = new QGroupBox(ControlTab);
        LS->setObjectName("LS");
        LS->setGeometry(QRect(30, 100, 161, 251));
        gridLayout_3 = new QGridLayout(LS);
        gridLayout_3->setObjectName("gridLayout_3");
        label_54 = new QLabel(LS);
        label_54->setObjectName("label_54");

        gridLayout_3->addWidget(label_54, 0, 0, 1, 2);

        LSSS = new QGroupBox(LS);
        LSSS->setObjectName("LSSS");
        verticalLayout_8 = new QVBoxLayout(LSSS);
        verticalLayout_8->setObjectName("verticalLayout_8");
        label_62 = new QLabel(LSSS);
        label_62->setObjectName("label_62");

        verticalLayout_8->addWidget(label_62);

        LS_9 = new QCheckBox(LSSS);
        LS_9->setObjectName("LS_9");

        verticalLayout_8->addWidget(LS_9);

        LS_10 = new QCheckBox(LSSS);
        LS_10->setObjectName("LS_10");

        verticalLayout_8->addWidget(LS_10);

        LS_11 = new QCheckBox(LSSS);
        LS_11->setObjectName("LS_11");

        verticalLayout_8->addWidget(LS_11);

        LS_14 = new QCheckBox(LSSS);
        LS_14->setObjectName("LS_14");

        verticalLayout_8->addWidget(LS_14);

        LS_15 = new QCheckBox(LSSS);
        LS_15->setObjectName("LS_15");

        verticalLayout_8->addWidget(LS_15);


        gridLayout_3->addWidget(LSSS, 1, 1, 1, 1);

        LSS = new QGroupBox(LS);
        LSS->setObjectName("LSS");
        verticalLayout_10 = new QVBoxLayout(LSS);
        verticalLayout_10->setObjectName("verticalLayout_10");
        label_53 = new QLabel(LSS);
        label_53->setObjectName("label_53");

        verticalLayout_10->addWidget(label_53);

        LS_2 = new QCheckBox(LSS);
        LS_2->setObjectName("LS_2");

        verticalLayout_10->addWidget(LS_2);

        LS_3 = new QCheckBox(LSS);
        LS_3->setObjectName("LS_3");

        verticalLayout_10->addWidget(LS_3);

        LS_4 = new QCheckBox(LSS);
        LS_4->setObjectName("LS_4");

        verticalLayout_10->addWidget(LS_4);

        LS_5 = new QCheckBox(LSS);
        LS_5->setObjectName("LS_5");

        verticalLayout_10->addWidget(LS_5);

        LS_8 = new QCheckBox(LSS);
        LS_8->setObjectName("LS_8");

        verticalLayout_10->addWidget(LS_8);


        gridLayout_3->addWidget(LSS, 1, 0, 1, 1);

        LSSSS = new QGroupBox(LS);
        LSSSS->setObjectName("LSSSS");
        horizontalLayout_3 = new QHBoxLayout(LSSSS);
        horizontalLayout_3->setObjectName("horizontalLayout_3");
        label_57 = new QLabel(LSSSS);
        label_57->setObjectName("label_57");

        horizontalLayout_3->addWidget(label_57);

        LS_1617 = new QCheckBox(LSSSS);
        LS_1617->setObjectName("LS_1617");

        horizontalLayout_3->addWidget(LS_1617);


        gridLayout_3->addWidget(LSSSS, 2, 0, 1, 2);

        SwitchES = new QGroupBox(ControlTab);
        SwitchES->setObjectName("SwitchES");
        SwitchES->setGeometry(QRect(230, 90, 174, 268));
        horizontalLayout_4 = new QHBoxLayout(SwitchES);
        horizontalLayout_4->setObjectName("horizontalLayout_4");
        ES1 = new QGroupBox(SwitchES);
        ES1->setObjectName("ES1");
        verticalLayout_9 = new QVBoxLayout(ES1);
        verticalLayout_9->setObjectName("verticalLayout_9");
        label_50 = new QLabel(ES1);
        label_50->setObjectName("label_50");

        verticalLayout_9->addWidget(label_50);

        label_49 = new QLabel(ES1);
        label_49->setObjectName("label_49");

        verticalLayout_9->addWidget(label_49);

        ES1_2 = new QCheckBox(ES1);
        ES1_2->setObjectName("ES1_2");

        verticalLayout_9->addWidget(ES1_2);

        ES1_3 = new QCheckBox(ES1);
        ES1_3->setObjectName("ES1_3");

        verticalLayout_9->addWidget(ES1_3);

        ES1_4 = new QCheckBox(ES1);
        ES1_4->setObjectName("ES1_4");

        verticalLayout_9->addWidget(ES1_4);

        ES1_5 = new QCheckBox(ES1);
        ES1_5->setObjectName("ES1_5");

        verticalLayout_9->addWidget(ES1_5);

        ES1_6 = new QCheckBox(ES1);
        ES1_6->setObjectName("ES1_6");

        verticalLayout_9->addWidget(ES1_6);

        ES1_7 = new QCheckBox(ES1);
        ES1_7->setObjectName("ES1_7");

        verticalLayout_9->addWidget(ES1_7);

        ES1_8 = new QCheckBox(ES1);
        ES1_8->setObjectName("ES1_8");

        verticalLayout_9->addWidget(ES1_8);

        ES1_9 = new QCheckBox(ES1);
        ES1_9->setObjectName("ES1_9");

        verticalLayout_9->addWidget(ES1_9);


        horizontalLayout_4->addWidget(ES1);

        ES2 = new QGroupBox(SwitchES);
        ES2->setObjectName("ES2");
        verticalLayout_19 = new QVBoxLayout(ES2);
        verticalLayout_19->setObjectName("verticalLayout_19");
        label_51 = new QLabel(ES2);
        label_51->setObjectName("label_51");

        verticalLayout_19->addWidget(label_51);

        label_52 = new QLabel(ES2);
        label_52->setObjectName("label_52");

        verticalLayout_19->addWidget(label_52);

        ES2_2 = new QCheckBox(ES2);
        ES2_2->setObjectName("ES2_2");

        verticalLayout_19->addWidget(ES2_2);

        ES2_3 = new QCheckBox(ES2);
        ES2_3->setObjectName("ES2_3");

        verticalLayout_19->addWidget(ES2_3);

        ES2_4 = new QCheckBox(ES2);
        ES2_4->setObjectName("ES2_4");

        verticalLayout_19->addWidget(ES2_4);

        ES2_5 = new QCheckBox(ES2);
        ES2_5->setObjectName("ES2_5");

        verticalLayout_19->addWidget(ES2_5);

        ES2_6 = new QCheckBox(ES2);
        ES2_6->setObjectName("ES2_6");

        verticalLayout_19->addWidget(ES2_6);

        ES2_7 = new QCheckBox(ES2);
        ES2_7->setObjectName("ES2_7");

        verticalLayout_19->addWidget(ES2_7);

        ES2_8 = new QCheckBox(ES2);
        ES2_8->setObjectName("ES2_8");

        verticalLayout_19->addWidget(ES2_8);

        ES2_9 = new QCheckBox(ES2);
        ES2_9->setObjectName("ES2_9");

        verticalLayout_19->addWidget(ES2_9, 0, Qt::AlignBottom);


        horizontalLayout_4->addWidget(ES2);

        PBCONTOff = new QPushButton(ControlTab);
        PBCONTOff->setObjectName("PBCONTOff");
        PBCONTOff->setGeometry(QRect(130, 30, 100, 41));
        PBCONTOff->setMinimumSize(QSize(100, 20));
        L_CONTON = new QLabel(ControlTab);
        L_CONTON->setObjectName("L_CONTON");
        L_CONTON->setGeometry(QRect(230, 30, 81, 41));
        L_CONTON->setFont(font);
        L_CONTON->setStyleSheet(QString::fromUtf8(""));
        L_CONTON->setFrameShape(QFrame::Box);
        L_CONTON->setAlignment(Qt::AlignCenter);
        label_64 = new QLabel(ControlTab);
        label_64->setObjectName("label_64");
        label_64->setGeometry(QRect(630, 20, 71, 16));
        M_UPSOUT = new QLabel(ControlTab);
        M_UPSOUT->setObjectName("M_UPSOUT");
        M_UPSOUT->setGeometry(QRect(710, 20, 81, 21));
        M_UPSOUT->setFont(font);
        M_UPSOUT->setStyleSheet(QString::fromUtf8(""));
        M_UPSOUT->setFrameShape(QFrame::Box);
        M_UPSOUT->setAlignment(Qt::AlignCenter);
        layoutWidget = new QWidget(ControlTab);
        layoutWidget->setObjectName("layoutWidget");
        layoutWidget->setGeometry(QRect(630, 70, 53, 351));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName("gridLayout");
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_142 = new QLabel(layoutWidget);
        label_142->setObjectName("label_142");

        gridLayout->addWidget(label_142, 3, 0, 1, 1);

        label_143 = new QLabel(layoutWidget);
        label_143->setObjectName("label_143");

        gridLayout->addWidget(label_143, 5, 0, 1, 1);

        label_138 = new QLabel(layoutWidget);
        label_138->setObjectName("label_138");

        gridLayout->addWidget(label_138, 1, 0, 1, 1);

        label_144 = new QLabel(layoutWidget);
        label_144->setObjectName("label_144");

        gridLayout->addWidget(label_144, 7, 0, 1, 1);

        label_145 = new QLabel(layoutWidget);
        label_145->setObjectName("label_145");

        gridLayout->addWidget(label_145, 8, 0, 1, 1);

        label_140 = new QLabel(layoutWidget);
        label_140->setObjectName("label_140");

        gridLayout->addWidget(label_140, 0, 0, 1, 1);

        label_137 = new QLabel(layoutWidget);
        label_137->setObjectName("label_137");

        gridLayout->addWidget(label_137, 4, 0, 1, 1);

        label_139 = new QLabel(layoutWidget);
        label_139->setObjectName("label_139");

        gridLayout->addWidget(label_139, 6, 0, 1, 1);

        label_141 = new QLabel(layoutWidget);
        label_141->setObjectName("label_141");

        gridLayout->addWidget(label_141, 2, 0, 1, 1);

        layoutWidget1 = new QWidget(ControlTab);
        layoutWidget1->setObjectName("layoutWidget1");
        layoutWidget1->setGeometry(QRect(690, 70, 141, 356));
        verticalLayout_11 = new QVBoxLayout(layoutWidget1);
        verticalLayout_11->setObjectName("verticalLayout_11");
        verticalLayout_11->setContentsMargins(0, 0, 0, 0);
        M_C_PLC_2 = new QLabel(layoutWidget1);
        M_C_PLC_2->setObjectName("M_C_PLC_2");
        M_C_PLC_2->setFont(font);
        M_C_PLC_2->setStyleSheet(QString::fromUtf8(""));
        M_C_PLC_2->setFrameShape(QFrame::Box);
        M_C_PLC_2->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(M_C_PLC_2);

        M_C_PLC_3 = new QLabel(layoutWidget1);
        M_C_PLC_3->setObjectName("M_C_PLC_3");
        M_C_PLC_3->setFont(font);
        M_C_PLC_3->setStyleSheet(QString::fromUtf8(""));
        M_C_PLC_3->setFrameShape(QFrame::Box);
        M_C_PLC_3->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(M_C_PLC_3);

        M_C_PLC_4 = new QLabel(layoutWidget1);
        M_C_PLC_4->setObjectName("M_C_PLC_4");
        M_C_PLC_4->setFont(font);
        M_C_PLC_4->setStyleSheet(QString::fromUtf8(""));
        M_C_PLC_4->setFrameShape(QFrame::Box);
        M_C_PLC_4->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(M_C_PLC_4);

        M_C_PLC_5 = new QLabel(layoutWidget1);
        M_C_PLC_5->setObjectName("M_C_PLC_5");
        M_C_PLC_5->setFont(font);
        M_C_PLC_5->setStyleSheet(QString::fromUtf8(""));
        M_C_PLC_5->setFrameShape(QFrame::Box);
        M_C_PLC_5->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(M_C_PLC_5);

        M_C_PLC_6 = new QLabel(layoutWidget1);
        M_C_PLC_6->setObjectName("M_C_PLC_6");
        M_C_PLC_6->setFont(font);
        M_C_PLC_6->setStyleSheet(QString::fromUtf8(""));
        M_C_PLC_6->setFrameShape(QFrame::Box);
        M_C_PLC_6->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(M_C_PLC_6);

        M_C_PLC_7 = new QLabel(layoutWidget1);
        M_C_PLC_7->setObjectName("M_C_PLC_7");
        M_C_PLC_7->setFont(font);
        M_C_PLC_7->setStyleSheet(QString::fromUtf8(""));
        M_C_PLC_7->setFrameShape(QFrame::Box);
        M_C_PLC_7->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(M_C_PLC_7);

        M_C_PLC_8 = new QLabel(layoutWidget1);
        M_C_PLC_8->setObjectName("M_C_PLC_8");
        M_C_PLC_8->setFont(font);
        M_C_PLC_8->setStyleSheet(QString::fromUtf8(""));
        M_C_PLC_8->setFrameShape(QFrame::Box);
        M_C_PLC_8->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(M_C_PLC_8);

        M_C_PLC_9 = new QLabel(layoutWidget1);
        M_C_PLC_9->setObjectName("M_C_PLC_9");
        M_C_PLC_9->setFont(font);
        M_C_PLC_9->setStyleSheet(QString::fromUtf8(""));
        M_C_PLC_9->setFrameShape(QFrame::Box);
        M_C_PLC_9->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(M_C_PLC_9);

        M_C_PLC_10 = new QLabel(layoutWidget1);
        M_C_PLC_10->setObjectName("M_C_PLC_10");
        M_C_PLC_10->setFont(font);
        M_C_PLC_10->setStyleSheet(QString::fromUtf8(""));
        M_C_PLC_10->setFrameShape(QFrame::Box);
        M_C_PLC_10->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(M_C_PLC_10);

        layoutWidget2 = new QWidget(ControlTab);
        layoutWidget2->setObjectName("layoutWidget2");
        layoutWidget2->setGeometry(QRect(840, 70, 71, 271));
        verticalLayout_26 = new QVBoxLayout(layoutWidget2);
        verticalLayout_26->setObjectName("verticalLayout_26");
        verticalLayout_26->setContentsMargins(0, 0, 0, 0);
        label_146 = new QLabel(layoutWidget2);
        label_146->setObjectName("label_146");

        verticalLayout_26->addWidget(label_146);

        label_147 = new QLabel(layoutWidget2);
        label_147->setObjectName("label_147");

        verticalLayout_26->addWidget(label_147);

        label_148 = new QLabel(layoutWidget2);
        label_148->setObjectName("label_148");

        verticalLayout_26->addWidget(label_148);

        label_149 = new QLabel(layoutWidget2);
        label_149->setObjectName("label_149");

        verticalLayout_26->addWidget(label_149);

        label_150 = new QLabel(layoutWidget2);
        label_150->setObjectName("label_150");

        verticalLayout_26->addWidget(label_150);

        label_151 = new QLabel(layoutWidget2);
        label_151->setObjectName("label_151");

        verticalLayout_26->addWidget(label_151);

        label_152 = new QLabel(layoutWidget2);
        label_152->setObjectName("label_152");

        verticalLayout_26->addWidget(label_152);

        layoutWidget3 = new QWidget(ControlTab);
        layoutWidget3->setObjectName("layoutWidget3");
        layoutWidget3->setGeometry(QRect(920, 70, 111, 276));
        verticalLayout_27 = new QVBoxLayout(layoutWidget3);
        verticalLayout_27->setObjectName("verticalLayout_27");
        verticalLayout_27->setContentsMargins(0, 0, 0, 0);
        M_MOT_2 = new QLabel(layoutWidget3);
        M_MOT_2->setObjectName("M_MOT_2");
        M_MOT_2->setFont(font);
        M_MOT_2->setStyleSheet(QString::fromUtf8(""));
        M_MOT_2->setFrameShape(QFrame::Box);
        M_MOT_2->setAlignment(Qt::AlignCenter);

        verticalLayout_27->addWidget(M_MOT_2);

        M_MOT_3 = new QLabel(layoutWidget3);
        M_MOT_3->setObjectName("M_MOT_3");
        M_MOT_3->setFont(font);
        M_MOT_3->setStyleSheet(QString::fromUtf8(""));
        M_MOT_3->setFrameShape(QFrame::Box);
        M_MOT_3->setAlignment(Qt::AlignCenter);

        verticalLayout_27->addWidget(M_MOT_3);

        M_MOT_6 = new QLabel(layoutWidget3);
        M_MOT_6->setObjectName("M_MOT_6");
        M_MOT_6->setFont(font);
        M_MOT_6->setStyleSheet(QString::fromUtf8(""));
        M_MOT_6->setFrameShape(QFrame::Box);
        M_MOT_6->setAlignment(Qt::AlignCenter);

        verticalLayout_27->addWidget(M_MOT_6);

        M_400V_OUT_2 = new QLabel(layoutWidget3);
        M_400V_OUT_2->setObjectName("M_400V_OUT_2");
        M_400V_OUT_2->setFont(font);
        M_400V_OUT_2->setStyleSheet(QString::fromUtf8(""));
        M_400V_OUT_2->setFrameShape(QFrame::Box);
        M_400V_OUT_2->setAlignment(Qt::AlignCenter);

        verticalLayout_27->addWidget(M_400V_OUT_2);

        M_400V_OUT_3 = new QLabel(layoutWidget3);
        M_400V_OUT_3->setObjectName("M_400V_OUT_3");
        M_400V_OUT_3->setFont(font);
        M_400V_OUT_3->setStyleSheet(QString::fromUtf8(""));
        M_400V_OUT_3->setFrameShape(QFrame::Box);
        M_400V_OUT_3->setAlignment(Qt::AlignCenter);

        verticalLayout_27->addWidget(M_400V_OUT_3);

        M_400V_OUT_5 = new QLabel(layoutWidget3);
        M_400V_OUT_5->setObjectName("M_400V_OUT_5");
        M_400V_OUT_5->setFont(font);
        M_400V_OUT_5->setStyleSheet(QString::fromUtf8(""));
        M_400V_OUT_5->setFrameShape(QFrame::Box);
        M_400V_OUT_5->setAlignment(Qt::AlignCenter);

        verticalLayout_27->addWidget(M_400V_OUT_5);

        M_400V_OUT_6 = new QLabel(layoutWidget3);
        M_400V_OUT_6->setObjectName("M_400V_OUT_6");
        M_400V_OUT_6->setFont(font);
        M_400V_OUT_6->setStyleSheet(QString::fromUtf8(""));
        M_400V_OUT_6->setFrameShape(QFrame::Box);
        M_400V_OUT_6->setAlignment(Qt::AlignCenter);

        verticalLayout_27->addWidget(M_400V_OUT_6);

        layoutWidget4 = new QWidget(ControlTab);
        layoutWidget4->setObjectName("layoutWidget4");
        layoutWidget4->setGeometry(QRect(1050, 70, 41, 271));
        verticalLayout_13 = new QVBoxLayout(layoutWidget4);
        verticalLayout_13->setObjectName("verticalLayout_13");
        verticalLayout_13->setContentsMargins(0, 0, 0, 0);
        label_129 = new QLabel(layoutWidget4);
        label_129->setObjectName("label_129");

        verticalLayout_13->addWidget(label_129);

        label_131 = new QLabel(layoutWidget4);
        label_131->setObjectName("label_131");

        verticalLayout_13->addWidget(label_131);

        label_132 = new QLabel(layoutWidget4);
        label_132->setObjectName("label_132");

        verticalLayout_13->addWidget(label_132);

        label_133 = new QLabel(layoutWidget4);
        label_133->setObjectName("label_133");

        verticalLayout_13->addWidget(label_133);

        label_134 = new QLabel(layoutWidget4);
        label_134->setObjectName("label_134");

        verticalLayout_13->addWidget(label_134);

        label_135 = new QLabel(layoutWidget4);
        label_135->setObjectName("label_135");

        verticalLayout_13->addWidget(label_135);

        label_136 = new QLabel(layoutWidget4);
        label_136->setObjectName("label_136");

        verticalLayout_13->addWidget(label_136);

        layoutWidget5 = new QWidget(ControlTab);
        layoutWidget5->setObjectName("layoutWidget5");
        layoutWidget5->setGeometry(QRect(1110, 70, 141, 276));
        verticalLayout_12 = new QVBoxLayout(layoutWidget5);
        verticalLayout_12->setObjectName("verticalLayout_12");
        verticalLayout_12->setContentsMargins(0, 0, 0, 0);
        M_CI_2 = new QLabel(layoutWidget5);
        M_CI_2->setObjectName("M_CI_2");
        M_CI_2->setFont(font);
        M_CI_2->setStyleSheet(QString::fromUtf8(""));
        M_CI_2->setFrameShape(QFrame::Box);
        M_CI_2->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(M_CI_2);

        M_CI_3 = new QLabel(layoutWidget5);
        M_CI_3->setObjectName("M_CI_3");
        M_CI_3->setFont(font);
        M_CI_3->setStyleSheet(QString::fromUtf8(""));
        M_CI_3->setFrameShape(QFrame::Box);
        M_CI_3->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(M_CI_3);

        M_CI_4 = new QLabel(layoutWidget5);
        M_CI_4->setObjectName("M_CI_4");
        M_CI_4->setFont(font);
        M_CI_4->setStyleSheet(QString::fromUtf8(""));
        M_CI_4->setFrameShape(QFrame::Box);
        M_CI_4->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(M_CI_4);

        M_CI_5 = new QLabel(layoutWidget5);
        M_CI_5->setObjectName("M_CI_5");
        M_CI_5->setFont(font);
        M_CI_5->setStyleSheet(QString::fromUtf8(""));
        M_CI_5->setFrameShape(QFrame::Box);
        M_CI_5->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(M_CI_5);

        M_CI_7 = new QLabel(layoutWidget5);
        M_CI_7->setObjectName("M_CI_7");
        M_CI_7->setFont(font);
        M_CI_7->setStyleSheet(QString::fromUtf8(""));
        M_CI_7->setFrameShape(QFrame::Box);
        M_CI_7->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(M_CI_7);

        M_CI_8 = new QLabel(layoutWidget5);
        M_CI_8->setObjectName("M_CI_8");
        M_CI_8->setFont(font);
        M_CI_8->setStyleSheet(QString::fromUtf8(""));
        M_CI_8->setFrameShape(QFrame::Box);
        M_CI_8->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(M_CI_8);

        M_CI_9 = new QLabel(layoutWidget5);
        M_CI_9->setObjectName("M_CI_9");
        M_CI_9->setFont(font);
        M_CI_9->setStyleSheet(QString::fromUtf8(""));
        M_CI_9->setFrameShape(QFrame::Box);
        M_CI_9->setAlignment(Qt::AlignCenter);

        verticalLayout_12->addWidget(M_CI_9);

        DutTestTabs->addTab(ControlTab, QString());
        InterfaceTab = new QWidget();
        InterfaceTab->setObjectName("InterfaceTab");
        I_PLC = new QGroupBox(InterfaceTab);
        I_PLC->setObjectName("I_PLC");
        I_PLC->setGeometry(QRect(1370, 10, 101, 270));
        verticalLayout_22 = new QVBoxLayout(I_PLC);
        verticalLayout_22->setObjectName("verticalLayout_22");
        label_34 = new QLabel(I_PLC);
        label_34->setObjectName("label_34");

        verticalLayout_22->addWidget(label_34);

        label_35 = new QLabel(I_PLC);
        label_35->setObjectName("label_35");

        verticalLayout_22->addWidget(label_35);

        I_PLC_1 = new QCheckBox(I_PLC);
        I_PLC_1->setObjectName("I_PLC_1");

        verticalLayout_22->addWidget(I_PLC_1);

        label_36 = new QLabel(I_PLC);
        label_36->setObjectName("label_36");

        verticalLayout_22->addWidget(label_36);

        I_PLC_0506 = new QCheckBox(I_PLC);
        I_PLC_0506->setObjectName("I_PLC_0506");

        verticalLayout_22->addWidget(I_PLC_0506);

        I_PLC_0708 = new QCheckBox(I_PLC);
        I_PLC_0708->setObjectName("I_PLC_0708");

        verticalLayout_22->addWidget(I_PLC_0708);

        I_PLC_0910 = new QCheckBox(I_PLC);
        I_PLC_0910->setObjectName("I_PLC_0910");

        verticalLayout_22->addWidget(I_PLC_0910);

        I_PLC_1112 = new QCheckBox(I_PLC);
        I_PLC_1112->setObjectName("I_PLC_1112");

        verticalLayout_22->addWidget(I_PLC_1112);

        I_PLC_1314 = new QCheckBox(I_PLC);
        I_PLC_1314->setObjectName("I_PLC_1314");

        verticalLayout_22->addWidget(I_PLC_1314);

        I_PLC_1516 = new QCheckBox(I_PLC);
        I_PLC_1516->setObjectName("I_PLC_1516");

        verticalLayout_22->addWidget(I_PLC_1516);

        I_PLC_1718 = new QCheckBox(I_PLC);
        I_PLC_1718->setObjectName("I_PLC_1718");

        verticalLayout_22->addWidget(I_PLC_1718);

        CII = new QGroupBox(InterfaceTab);
        CII->setObjectName("CII");
        CII->setGeometry(QRect(20, 100, 601, 326));
        horizontalLayout = new QHBoxLayout(CII);
        horizontalLayout->setObjectName("horizontalLayout");
        C_I1 = new QGroupBox(CII);
        C_I1->setObjectName("C_I1");
        verticalLayout = new QVBoxLayout(C_I1);
        verticalLayout->setObjectName("verticalLayout");
        label_3 = new QLabel(C_I1);
        label_3->setObjectName("label_3");

        verticalLayout->addWidget(label_3);

        label_41 = new QLabel(C_I1);
        label_41->setObjectName("label_41");

        verticalLayout->addWidget(label_41);

        C_I1_2 = new QCheckBox(C_I1);
        C_I1_2->setObjectName("C_I1_2");

        verticalLayout->addWidget(C_I1_2);

        C_I1_3 = new QCheckBox(C_I1);
        C_I1_3->setObjectName("C_I1_3");

        verticalLayout->addWidget(C_I1_3);

        C_I1_4 = new QCheckBox(C_I1);
        C_I1_4->setObjectName("C_I1_4");

        verticalLayout->addWidget(C_I1_4);

        C_I1_5 = new QCheckBox(C_I1);
        C_I1_5->setObjectName("C_I1_5");

        verticalLayout->addWidget(C_I1_5);

        C_I1_7 = new QCheckBox(C_I1);
        C_I1_7->setObjectName("C_I1_7");

        verticalLayout->addWidget(C_I1_7);

        C_I1_8 = new QCheckBox(C_I1);
        C_I1_8->setObjectName("C_I1_8");

        verticalLayout->addWidget(C_I1_8);

        C_I1_9 = new QCheckBox(C_I1);
        C_I1_9->setObjectName("C_I1_9");

        verticalLayout->addWidget(C_I1_9);

        C_I1_12 = new QCheckBox(C_I1);
        C_I1_12->setObjectName("C_I1_12");

        verticalLayout->addWidget(C_I1_12);

        label_25 = new QLabel(C_I1);
        label_25->setObjectName("label_25");

        verticalLayout->addWidget(label_25);

        C_I1_1516 = new QCheckBox(C_I1);
        C_I1_1516->setObjectName("C_I1_1516");

        verticalLayout->addWidget(C_I1_1516);

        C_I1_1718 = new QCheckBox(C_I1);
        C_I1_1718->setObjectName("C_I1_1718");

        verticalLayout->addWidget(C_I1_1718);


        horizontalLayout->addWidget(C_I1);

        C_I2 = new QGroupBox(CII);
        C_I2->setObjectName("C_I2");
        verticalLayout_2 = new QVBoxLayout(C_I2);
        verticalLayout_2->setObjectName("verticalLayout_2");
        label_4 = new QLabel(C_I2);
        label_4->setObjectName("label_4");

        verticalLayout_2->addWidget(label_4);

        label_42 = new QLabel(C_I2);
        label_42->setObjectName("label_42");

        verticalLayout_2->addWidget(label_42);

        C_I2_2 = new QCheckBox(C_I2);
        C_I2_2->setObjectName("C_I2_2");

        verticalLayout_2->addWidget(C_I2_2);

        C_I2_3 = new QCheckBox(C_I2);
        C_I2_3->setObjectName("C_I2_3");

        verticalLayout_2->addWidget(C_I2_3);

        C_I2_4 = new QCheckBox(C_I2);
        C_I2_4->setObjectName("C_I2_4");

        verticalLayout_2->addWidget(C_I2_4);

        C_I2_5 = new QCheckBox(C_I2);
        C_I2_5->setObjectName("C_I2_5");

        verticalLayout_2->addWidget(C_I2_5);

        C_I2_7 = new QCheckBox(C_I2);
        C_I2_7->setObjectName("C_I2_7");

        verticalLayout_2->addWidget(C_I2_7);

        C_I2_8 = new QCheckBox(C_I2);
        C_I2_8->setObjectName("C_I2_8");

        verticalLayout_2->addWidget(C_I2_8);

        C_I2_9 = new QCheckBox(C_I2);
        C_I2_9->setObjectName("C_I2_9");

        verticalLayout_2->addWidget(C_I2_9);

        C_I2_12 = new QCheckBox(C_I2);
        C_I2_12->setObjectName("C_I2_12");

        verticalLayout_2->addWidget(C_I2_12);

        label_26 = new QLabel(C_I2);
        label_26->setObjectName("label_26");

        verticalLayout_2->addWidget(label_26);

        C_I2_1516 = new QCheckBox(C_I2);
        C_I2_1516->setObjectName("C_I2_1516");

        verticalLayout_2->addWidget(C_I2_1516);

        C_I2_1718 = new QCheckBox(C_I2);
        C_I2_1718->setObjectName("C_I2_1718");

        verticalLayout_2->addWidget(C_I2_1718);


        horizontalLayout->addWidget(C_I2);

        C_I3 = new QGroupBox(CII);
        C_I3->setObjectName("C_I3");
        verticalLayout_3 = new QVBoxLayout(C_I3);
        verticalLayout_3->setObjectName("verticalLayout_3");
        label_5 = new QLabel(C_I3);
        label_5->setObjectName("label_5");

        verticalLayout_3->addWidget(label_5);

        label_43 = new QLabel(C_I3);
        label_43->setObjectName("label_43");

        verticalLayout_3->addWidget(label_43);

        C_I3_2 = new QCheckBox(C_I3);
        C_I3_2->setObjectName("C_I3_2");

        verticalLayout_3->addWidget(C_I3_2);

        C_I3_3 = new QCheckBox(C_I3);
        C_I3_3->setObjectName("C_I3_3");

        verticalLayout_3->addWidget(C_I3_3);

        C_I3_4 = new QCheckBox(C_I3);
        C_I3_4->setObjectName("C_I3_4");

        verticalLayout_3->addWidget(C_I3_4);

        C_I3_5 = new QCheckBox(C_I3);
        C_I3_5->setObjectName("C_I3_5");

        verticalLayout_3->addWidget(C_I3_5);

        C_I3_7 = new QCheckBox(C_I3);
        C_I3_7->setObjectName("C_I3_7");

        verticalLayout_3->addWidget(C_I3_7);

        C_I3_8 = new QCheckBox(C_I3);
        C_I3_8->setObjectName("C_I3_8");

        verticalLayout_3->addWidget(C_I3_8);

        C_I3_9 = new QCheckBox(C_I3);
        C_I3_9->setObjectName("C_I3_9");

        verticalLayout_3->addWidget(C_I3_9);

        C_I3_12 = new QCheckBox(C_I3);
        C_I3_12->setObjectName("C_I3_12");

        verticalLayout_3->addWidget(C_I3_12);

        label_27 = new QLabel(C_I3);
        label_27->setObjectName("label_27");

        verticalLayout_3->addWidget(label_27);

        C_I3_1516 = new QCheckBox(C_I3);
        C_I3_1516->setObjectName("C_I3_1516");

        verticalLayout_3->addWidget(C_I3_1516);

        C_I3_1718 = new QCheckBox(C_I3);
        C_I3_1718->setObjectName("C_I3_1718");

        verticalLayout_3->addWidget(C_I3_1718);


        horizontalLayout->addWidget(C_I3);

        C_I4 = new QGroupBox(CII);
        C_I4->setObjectName("C_I4");
        verticalLayout_4 = new QVBoxLayout(C_I4);
        verticalLayout_4->setObjectName("verticalLayout_4");
        label_6 = new QLabel(C_I4);
        label_6->setObjectName("label_6");

        verticalLayout_4->addWidget(label_6);

        label_46 = new QLabel(C_I4);
        label_46->setObjectName("label_46");

        verticalLayout_4->addWidget(label_46);

        C_I4_2 = new QCheckBox(C_I4);
        C_I4_2->setObjectName("C_I4_2");

        verticalLayout_4->addWidget(C_I4_2);

        C_I4_3 = new QCheckBox(C_I4);
        C_I4_3->setObjectName("C_I4_3");

        verticalLayout_4->addWidget(C_I4_3);

        C_I4_4 = new QCheckBox(C_I4);
        C_I4_4->setObjectName("C_I4_4");

        verticalLayout_4->addWidget(C_I4_4);

        C_I4_5 = new QCheckBox(C_I4);
        C_I4_5->setObjectName("C_I4_5");

        verticalLayout_4->addWidget(C_I4_5);

        C_I4_7 = new QCheckBox(C_I4);
        C_I4_7->setObjectName("C_I4_7");

        verticalLayout_4->addWidget(C_I4_7);

        C_I4_8 = new QCheckBox(C_I4);
        C_I4_8->setObjectName("C_I4_8");

        verticalLayout_4->addWidget(C_I4_8);

        C_I4_9 = new QCheckBox(C_I4);
        C_I4_9->setObjectName("C_I4_9");

        verticalLayout_4->addWidget(C_I4_9);

        C_I4_12 = new QCheckBox(C_I4);
        C_I4_12->setObjectName("C_I4_12");

        verticalLayout_4->addWidget(C_I4_12);

        label_39 = new QLabel(C_I4);
        label_39->setObjectName("label_39");

        verticalLayout_4->addWidget(label_39);

        C_I4_1516 = new QCheckBox(C_I4);
        C_I4_1516->setObjectName("C_I4_1516");

        verticalLayout_4->addWidget(C_I4_1516);

        C_I4_1718 = new QCheckBox(C_I4);
        C_I4_1718->setObjectName("C_I4_1718");

        verticalLayout_4->addWidget(C_I4_1718);


        horizontalLayout->addWidget(C_I4);

        C_I5 = new QGroupBox(CII);
        C_I5->setObjectName("C_I5");
        verticalLayout_5 = new QVBoxLayout(C_I5);
        verticalLayout_5->setObjectName("verticalLayout_5");
        label_7 = new QLabel(C_I5);
        label_7->setObjectName("label_7");

        verticalLayout_5->addWidget(label_7);

        label_44 = new QLabel(C_I5);
        label_44->setObjectName("label_44");

        verticalLayout_5->addWidget(label_44);

        C_I5_2 = new QCheckBox(C_I5);
        C_I5_2->setObjectName("C_I5_2");

        verticalLayout_5->addWidget(C_I5_2);

        C_I5_3 = new QCheckBox(C_I5);
        C_I5_3->setObjectName("C_I5_3");

        verticalLayout_5->addWidget(C_I5_3);

        C_I5_4 = new QCheckBox(C_I5);
        C_I5_4->setObjectName("C_I5_4");

        verticalLayout_5->addWidget(C_I5_4);

        C_I5_5 = new QCheckBox(C_I5);
        C_I5_5->setObjectName("C_I5_5");

        verticalLayout_5->addWidget(C_I5_5);

        C_I5_7 = new QCheckBox(C_I5);
        C_I5_7->setObjectName("C_I5_7");

        verticalLayout_5->addWidget(C_I5_7);

        C_I5_8 = new QCheckBox(C_I5);
        C_I5_8->setObjectName("C_I5_8");

        verticalLayout_5->addWidget(C_I5_8);

        C_I5_9 = new QCheckBox(C_I5);
        C_I5_9->setObjectName("C_I5_9");

        verticalLayout_5->addWidget(C_I5_9);

        C_I5_12 = new QCheckBox(C_I5);
        C_I5_12->setObjectName("C_I5_12");

        verticalLayout_5->addWidget(C_I5_12);

        label_37 = new QLabel(C_I5);
        label_37->setObjectName("label_37");

        verticalLayout_5->addWidget(label_37);

        C_I5_1516 = new QCheckBox(C_I5);
        C_I5_1516->setObjectName("C_I5_1516");

        verticalLayout_5->addWidget(C_I5_1516);

        C_I5_1718 = new QCheckBox(C_I5);
        C_I5_1718->setObjectName("C_I5_1718");

        verticalLayout_5->addWidget(C_I5_1718);


        horizontalLayout->addWidget(C_I5);

        C_I6 = new QGroupBox(CII);
        C_I6->setObjectName("C_I6");
        verticalLayout_6 = new QVBoxLayout(C_I6);
        verticalLayout_6->setObjectName("verticalLayout_6");
        label_8 = new QLabel(C_I6);
        label_8->setObjectName("label_8");

        verticalLayout_6->addWidget(label_8);

        label_45 = new QLabel(C_I6);
        label_45->setObjectName("label_45");

        verticalLayout_6->addWidget(label_45);

        C_I6_2 = new QCheckBox(C_I6);
        C_I6_2->setObjectName("C_I6_2");

        verticalLayout_6->addWidget(C_I6_2);

        C_I6_3 = new QCheckBox(C_I6);
        C_I6_3->setObjectName("C_I6_3");

        verticalLayout_6->addWidget(C_I6_3);

        C_I6_4 = new QCheckBox(C_I6);
        C_I6_4->setObjectName("C_I6_4");

        verticalLayout_6->addWidget(C_I6_4);

        C_I6_5 = new QCheckBox(C_I6);
        C_I6_5->setObjectName("C_I6_5");

        verticalLayout_6->addWidget(C_I6_5);

        C_I6_7 = new QCheckBox(C_I6);
        C_I6_7->setObjectName("C_I6_7");

        verticalLayout_6->addWidget(C_I6_7);

        C_I6_8 = new QCheckBox(C_I6);
        C_I6_8->setObjectName("C_I6_8");

        verticalLayout_6->addWidget(C_I6_8);

        C_I6_9 = new QCheckBox(C_I6);
        C_I6_9->setObjectName("C_I6_9");

        verticalLayout_6->addWidget(C_I6_9);

        C_I6_12 = new QCheckBox(C_I6);
        C_I6_12->setObjectName("C_I6_12");

        verticalLayout_6->addWidget(C_I6_12);

        label_38 = new QLabel(C_I6);
        label_38->setObjectName("label_38");

        verticalLayout_6->addWidget(label_38);

        C_I6_1516 = new QCheckBox(C_I6);
        C_I6_1516->setObjectName("C_I6_1516");

        verticalLayout_6->addWidget(C_I6_1516);

        C_I6_1718 = new QCheckBox(C_I6);
        C_I6_1718->setObjectName("C_I6_1718");

        verticalLayout_6->addWidget(C_I6_1718);


        horizontalLayout->addWidget(C_I6);

        C_I7 = new QGroupBox(CII);
        C_I7->setObjectName("C_I7");
        verticalLayout_7 = new QVBoxLayout(C_I7);
        verticalLayout_7->setObjectName("verticalLayout_7");
        label_9 = new QLabel(C_I7);
        label_9->setObjectName("label_9");

        verticalLayout_7->addWidget(label_9);

        label_47 = new QLabel(C_I7);
        label_47->setObjectName("label_47");

        verticalLayout_7->addWidget(label_47);

        C_I7_2 = new QCheckBox(C_I7);
        C_I7_2->setObjectName("C_I7_2");

        verticalLayout_7->addWidget(C_I7_2);

        C_I7_3 = new QCheckBox(C_I7);
        C_I7_3->setObjectName("C_I7_3");

        verticalLayout_7->addWidget(C_I7_3);

        C_I7_4 = new QCheckBox(C_I7);
        C_I7_4->setObjectName("C_I7_4");

        verticalLayout_7->addWidget(C_I7_4);

        C_I7_5 = new QCheckBox(C_I7);
        C_I7_5->setObjectName("C_I7_5");

        verticalLayout_7->addWidget(C_I7_5);

        C_I7_7 = new QCheckBox(C_I7);
        C_I7_7->setObjectName("C_I7_7");

        verticalLayout_7->addWidget(C_I7_7);

        C_I7_8 = new QCheckBox(C_I7);
        C_I7_8->setObjectName("C_I7_8");

        verticalLayout_7->addWidget(C_I7_8);

        C_I7_9 = new QCheckBox(C_I7);
        C_I7_9->setObjectName("C_I7_9");

        verticalLayout_7->addWidget(C_I7_9);

        C_I7_12 = new QCheckBox(C_I7);
        C_I7_12->setObjectName("C_I7_12");

        verticalLayout_7->addWidget(C_I7_12);

        label_40 = new QLabel(C_I7);
        label_40->setObjectName("label_40");

        verticalLayout_7->addWidget(label_40);

        C_I7_1516 = new QCheckBox(C_I7);
        C_I7_1516->setObjectName("C_I7_1516");

        verticalLayout_7->addWidget(C_I7_1516);

        C_I7_1718 = new QCheckBox(C_I7);
        C_I7_1718->setObjectName("C_I7_1718");

        verticalLayout_7->addWidget(C_I7_1718);


        horizontalLayout->addWidget(C_I7);

        PBINTFOn = new QPushButton(InterfaceTab);
        PBINTFOn->setObjectName("PBINTFOn");
        PBINTFOn->setGeometry(QRect(50, 40, 100, 41));
        PBINTFOn->setMinimumSize(QSize(100, 20));
        UserINTK = new QGroupBox(InterfaceTab);
        UserINTK->setObjectName("UserINTK");
        UserINTK->setGeometry(QRect(410, 20, 101, 71));
        verticalLayout_25 = new QVBoxLayout(UserINTK);
        verticalLayout_25->setObjectName("verticalLayout_25");
        DOOR = new QCheckBox(UserINTK);
        DOOR->setObjectName("DOOR");

        verticalLayout_25->addWidget(DOOR);

        STOP = new QCheckBox(UserINTK);
        STOP->setObjectName("STOP");

        verticalLayout_25->addWidget(STOP);

        CDB = new QGroupBox(InterfaceTab);
        CDB->setObjectName("CDB");
        CDB->setGeometry(QRect(870, 10, 491, 171));
        horizontalLayout_2 = new QHBoxLayout(CDB);
        horizontalLayout_2->setObjectName("horizontalLayout_2");
        CDB1 = new QGroupBox(CDB);
        CDB1->setObjectName("CDB1");
        verticalLayout_14 = new QVBoxLayout(CDB1);
        verticalLayout_14->setObjectName("verticalLayout_14");
        label_10 = new QLabel(CDB1);
        label_10->setObjectName("label_10");

        verticalLayout_14->addWidget(label_10);

        label_11 = new QLabel(CDB1);
        label_11->setObjectName("label_11");

        verticalLayout_14->addWidget(label_11);

        CDB1_0104 = new QCheckBox(CDB1);
        CDB1_0104->setObjectName("CDB1_0104");

        verticalLayout_14->addWidget(CDB1_0104);

        CDB1_7 = new QCheckBox(CDB1);
        CDB1_7->setObjectName("CDB1_7");

        verticalLayout_14->addWidget(CDB1_7);

        label_12 = new QLabel(CDB1);
        label_12->setObjectName("label_12");

        verticalLayout_14->addWidget(label_12);

        CDB1_0506 = new QCheckBox(CDB1);
        CDB1_0506->setObjectName("CDB1_0506");

        verticalLayout_14->addWidget(CDB1_0506);


        horizontalLayout_2->addWidget(CDB1);

        CDB2 = new QGroupBox(CDB);
        CDB2->setObjectName("CDB2");
        verticalLayout_15 = new QVBoxLayout(CDB2);
        verticalLayout_15->setObjectName("verticalLayout_15");
        label_13 = new QLabel(CDB2);
        label_13->setObjectName("label_13");

        verticalLayout_15->addWidget(label_13);

        label_14 = new QLabel(CDB2);
        label_14->setObjectName("label_14");

        verticalLayout_15->addWidget(label_14);

        CDB2_0104 = new QCheckBox(CDB2);
        CDB2_0104->setObjectName("CDB2_0104");

        verticalLayout_15->addWidget(CDB2_0104);

        CDB2_7 = new QCheckBox(CDB2);
        CDB2_7->setObjectName("CDB2_7");

        verticalLayout_15->addWidget(CDB2_7);

        label_15 = new QLabel(CDB2);
        label_15->setObjectName("label_15");

        verticalLayout_15->addWidget(label_15);

        CDB2_0506 = new QCheckBox(CDB2);
        CDB2_0506->setObjectName("CDB2_0506");

        verticalLayout_15->addWidget(CDB2_0506);


        horizontalLayout_2->addWidget(CDB2);

        CDB3 = new QGroupBox(CDB);
        CDB3->setObjectName("CDB3");
        verticalLayout_16 = new QVBoxLayout(CDB3);
        verticalLayout_16->setObjectName("verticalLayout_16");
        label_16 = new QLabel(CDB3);
        label_16->setObjectName("label_16");

        verticalLayout_16->addWidget(label_16);

        label_17 = new QLabel(CDB3);
        label_17->setObjectName("label_17");

        verticalLayout_16->addWidget(label_17);

        CDB3_0104 = new QCheckBox(CDB3);
        CDB3_0104->setObjectName("CDB3_0104");

        verticalLayout_16->addWidget(CDB3_0104);

        CDB3_7 = new QCheckBox(CDB3);
        CDB3_7->setObjectName("CDB3_7");

        verticalLayout_16->addWidget(CDB3_7);

        label_18 = new QLabel(CDB3);
        label_18->setObjectName("label_18");

        verticalLayout_16->addWidget(label_18);

        CDB3_0506 = new QCheckBox(CDB3);
        CDB3_0506->setObjectName("CDB3_0506");

        verticalLayout_16->addWidget(CDB3_0506);


        horizontalLayout_2->addWidget(CDB3);

        CDB4 = new QGroupBox(CDB);
        CDB4->setObjectName("CDB4");
        verticalLayout_17 = new QVBoxLayout(CDB4);
        verticalLayout_17->setObjectName("verticalLayout_17");
        label_19 = new QLabel(CDB4);
        label_19->setObjectName("label_19");

        verticalLayout_17->addWidget(label_19);

        label_20 = new QLabel(CDB4);
        label_20->setObjectName("label_20");

        verticalLayout_17->addWidget(label_20);

        CDB4_0104 = new QCheckBox(CDB4);
        CDB4_0104->setObjectName("CDB4_0104");

        verticalLayout_17->addWidget(CDB4_0104);

        CDB4_7 = new QCheckBox(CDB4);
        CDB4_7->setObjectName("CDB4_7");

        verticalLayout_17->addWidget(CDB4_7);

        label_21 = new QLabel(CDB4);
        label_21->setObjectName("label_21");

        verticalLayout_17->addWidget(label_21);

        CDB4_0506 = new QCheckBox(CDB4);
        CDB4_0506->setObjectName("CDB4_0506");

        verticalLayout_17->addWidget(CDB4_0506);


        horizontalLayout_2->addWidget(CDB4);

        CDB5 = new QGroupBox(CDB);
        CDB5->setObjectName("CDB5");
        verticalLayout_18 = new QVBoxLayout(CDB5);
        verticalLayout_18->setObjectName("verticalLayout_18");
        label_22 = new QLabel(CDB5);
        label_22->setObjectName("label_22");

        verticalLayout_18->addWidget(label_22);

        label_23 = new QLabel(CDB5);
        label_23->setObjectName("label_23");

        verticalLayout_18->addWidget(label_23);

        CDB5_0104 = new QCheckBox(CDB5);
        CDB5_0104->setObjectName("CDB5_0104");

        verticalLayout_18->addWidget(CDB5_0104);

        CDB5_7 = new QCheckBox(CDB5);
        CDB5_7->setObjectName("CDB5_7");

        verticalLayout_18->addWidget(CDB5_7);

        label_24 = new QLabel(CDB5);
        label_24->setObjectName("label_24");

        verticalLayout_18->addWidget(label_24);

        CDB5_0506 = new QCheckBox(CDB5);
        CDB5_0506->setObjectName("CDB5_0506");

        verticalLayout_18->addWidget(CDB5_0506);


        horizontalLayout_2->addWidget(CDB5);

        CDB6 = new QGroupBox(CDB);
        CDB6->setObjectName("CDB6");
        verticalLayout_20 = new QVBoxLayout(CDB6);
        verticalLayout_20->setObjectName("verticalLayout_20");
        label_28 = new QLabel(CDB6);
        label_28->setObjectName("label_28");

        verticalLayout_20->addWidget(label_28);

        label_29 = new QLabel(CDB6);
        label_29->setObjectName("label_29");

        verticalLayout_20->addWidget(label_29);

        CDB6_0104 = new QCheckBox(CDB6);
        CDB6_0104->setObjectName("CDB6_0104");

        verticalLayout_20->addWidget(CDB6_0104);

        CDB6_7 = new QCheckBox(CDB6);
        CDB6_7->setObjectName("CDB6_7");

        verticalLayout_20->addWidget(CDB6_7);

        label_30 = new QLabel(CDB6);
        label_30->setObjectName("label_30");

        verticalLayout_20->addWidget(label_30);

        CDB6_0506 = new QCheckBox(CDB6);
        CDB6_0506->setObjectName("CDB6_0506");

        verticalLayout_20->addWidget(CDB6_0506);


        horizontalLayout_2->addWidget(CDB6);

        CDB7 = new QGroupBox(CDB);
        CDB7->setObjectName("CDB7");
        verticalLayout_21 = new QVBoxLayout(CDB7);
        verticalLayout_21->setObjectName("verticalLayout_21");
        label_31 = new QLabel(CDB7);
        label_31->setObjectName("label_31");

        verticalLayout_21->addWidget(label_31);

        label_32 = new QLabel(CDB7);
        label_32->setObjectName("label_32");

        verticalLayout_21->addWidget(label_32);

        CDB7_0104 = new QCheckBox(CDB7);
        CDB7_0104->setObjectName("CDB7_0104");

        verticalLayout_21->addWidget(CDB7_0104);

        CDB7_7 = new QCheckBox(CDB7);
        CDB7_7->setObjectName("CDB7_7");

        verticalLayout_21->addWidget(CDB7_7);

        label_33 = new QLabel(CDB7);
        label_33->setObjectName("label_33");

        verticalLayout_21->addWidget(label_33);

        CDB7_0506 = new QCheckBox(CDB7);
        CDB7_0506->setObjectName("CDB7_0506");

        verticalLayout_21->addWidget(CDB7_0506);


        horizontalLayout_2->addWidget(CDB7);

        PBINTFOff = new QPushButton(InterfaceTab);
        PBINTFOff->setObjectName("PBINTFOff");
        PBINTFOff->setGeometry(QRect(150, 40, 100, 41));
        PBINTFOff->setMinimumSize(QSize(100, 20));
        L_INTFON = new QLabel(InterfaceTab);
        L_INTFON->setObjectName("L_INTFON");
        L_INTFON->setGeometry(QRect(250, 40, 81, 41));
        L_INTFON->setFont(font);
        L_INTFON->setStyleSheet(QString::fromUtf8(""));
        L_INTFON->setFrameShape(QFrame::Box);
        L_INTFON->setAlignment(Qt::AlignCenter);
        layoutWidget6 = new QWidget(InterfaceTab);
        layoutWidget6->setObjectName("layoutWidget6");
        layoutWidget6->setGeometry(QRect(660, 200, 61, 271));
        verticalLayout_29 = new QVBoxLayout(layoutWidget6);
        verticalLayout_29->setObjectName("verticalLayout_29");
        verticalLayout_29->setContentsMargins(0, 0, 0, 0);
        label_154 = new QLabel(layoutWidget6);
        label_154->setObjectName("label_154");

        verticalLayout_29->addWidget(label_154);

        label_155 = new QLabel(layoutWidget6);
        label_155->setObjectName("label_155");

        verticalLayout_29->addWidget(label_155);

        label_156 = new QLabel(layoutWidget6);
        label_156->setObjectName("label_156");

        verticalLayout_29->addWidget(label_156);

        label_157 = new QLabel(layoutWidget6);
        label_157->setObjectName("label_157");

        verticalLayout_29->addWidget(label_157);

        label_158 = new QLabel(layoutWidget6);
        label_158->setObjectName("label_158");

        verticalLayout_29->addWidget(label_158);

        label_159 = new QLabel(layoutWidget6);
        label_159->setObjectName("label_159");

        verticalLayout_29->addWidget(label_159);

        label_160 = new QLabel(layoutWidget6);
        label_160->setObjectName("label_160");

        verticalLayout_29->addWidget(label_160);

        layoutWidget7 = new QWidget(InterfaceTab);
        layoutWidget7->setObjectName("layoutWidget7");
        layoutWidget7->setGeometry(QRect(730, 200, 61, 276));
        verticalLayout_28 = new QVBoxLayout(layoutWidget7);
        verticalLayout_28->setObjectName("verticalLayout_28");
        verticalLayout_28->setContentsMargins(0, 0, 0, 0);
        M_C_I_1_13 = new QLabel(layoutWidget7);
        M_C_I_1_13->setObjectName("M_C_I_1_13");
        M_C_I_1_13->setFont(font);
        M_C_I_1_13->setStyleSheet(QString::fromUtf8(""));
        M_C_I_1_13->setFrameShape(QFrame::Box);
        M_C_I_1_13->setAlignment(Qt::AlignCenter);

        verticalLayout_28->addWidget(M_C_I_1_13);

        M_C_I_2_13 = new QLabel(layoutWidget7);
        M_C_I_2_13->setObjectName("M_C_I_2_13");
        M_C_I_2_13->setFont(font);
        M_C_I_2_13->setStyleSheet(QString::fromUtf8(""));
        M_C_I_2_13->setFrameShape(QFrame::Box);
        M_C_I_2_13->setAlignment(Qt::AlignCenter);

        verticalLayout_28->addWidget(M_C_I_2_13);

        M_C_I_3_13 = new QLabel(layoutWidget7);
        M_C_I_3_13->setObjectName("M_C_I_3_13");
        M_C_I_3_13->setFont(font);
        M_C_I_3_13->setStyleSheet(QString::fromUtf8(""));
        M_C_I_3_13->setFrameShape(QFrame::Box);
        M_C_I_3_13->setAlignment(Qt::AlignCenter);

        verticalLayout_28->addWidget(M_C_I_3_13);

        M_C_I_4_13 = new QLabel(layoutWidget7);
        M_C_I_4_13->setObjectName("M_C_I_4_13");
        M_C_I_4_13->setFont(font);
        M_C_I_4_13->setStyleSheet(QString::fromUtf8(""));
        M_C_I_4_13->setFrameShape(QFrame::Box);
        M_C_I_4_13->setAlignment(Qt::AlignCenter);

        verticalLayout_28->addWidget(M_C_I_4_13);

        M_C_I_5_13 = new QLabel(layoutWidget7);
        M_C_I_5_13->setObjectName("M_C_I_5_13");
        M_C_I_5_13->setFont(font);
        M_C_I_5_13->setStyleSheet(QString::fromUtf8(""));
        M_C_I_5_13->setFrameShape(QFrame::Box);
        M_C_I_5_13->setAlignment(Qt::AlignCenter);

        verticalLayout_28->addWidget(M_C_I_5_13);

        M_C_I_6_13 = new QLabel(layoutWidget7);
        M_C_I_6_13->setObjectName("M_C_I_6_13");
        M_C_I_6_13->setFont(font);
        M_C_I_6_13->setStyleSheet(QString::fromUtf8(""));
        M_C_I_6_13->setFrameShape(QFrame::Box);
        M_C_I_6_13->setAlignment(Qt::AlignCenter);

        verticalLayout_28->addWidget(M_C_I_6_13);

        M_C_I_7_13 = new QLabel(layoutWidget7);
        M_C_I_7_13->setObjectName("M_C_I_7_13");
        M_C_I_7_13->setFont(font);
        M_C_I_7_13->setStyleSheet(QString::fromUtf8(""));
        M_C_I_7_13->setFrameShape(QFrame::Box);
        M_C_I_7_13->setAlignment(Qt::AlignCenter);

        verticalLayout_28->addWidget(M_C_I_7_13);

        layoutWidget8 = new QWidget(InterfaceTab);
        layoutWidget8->setObjectName("layoutWidget8");
        layoutWidget8->setGeometry(QRect(890, 200, 91, 276));
        verticalLayout_30 = new QVBoxLayout(layoutWidget8);
        verticalLayout_30->setObjectName("verticalLayout_30");
        verticalLayout_30->setContentsMargins(0, 0, 0, 0);
        M_CDB_1_2 = new QLabel(layoutWidget8);
        M_CDB_1_2->setObjectName("M_CDB_1_2");
        M_CDB_1_2->setFont(font);
        M_CDB_1_2->setStyleSheet(QString::fromUtf8(""));
        M_CDB_1_2->setFrameShape(QFrame::Box);
        M_CDB_1_2->setAlignment(Qt::AlignCenter);

        verticalLayout_30->addWidget(M_CDB_1_2);

        M_CDB_2_2 = new QLabel(layoutWidget8);
        M_CDB_2_2->setObjectName("M_CDB_2_2");
        M_CDB_2_2->setFont(font);
        M_CDB_2_2->setStyleSheet(QString::fromUtf8(""));
        M_CDB_2_2->setFrameShape(QFrame::Box);
        M_CDB_2_2->setAlignment(Qt::AlignCenter);

        verticalLayout_30->addWidget(M_CDB_2_2);

        M_CDB_3_2 = new QLabel(layoutWidget8);
        M_CDB_3_2->setObjectName("M_CDB_3_2");
        M_CDB_3_2->setFont(font);
        M_CDB_3_2->setStyleSheet(QString::fromUtf8(""));
        M_CDB_3_2->setFrameShape(QFrame::Box);
        M_CDB_3_2->setAlignment(Qt::AlignCenter);

        verticalLayout_30->addWidget(M_CDB_3_2);

        M_CDB_4_2 = new QLabel(layoutWidget8);
        M_CDB_4_2->setObjectName("M_CDB_4_2");
        M_CDB_4_2->setFont(font);
        M_CDB_4_2->setStyleSheet(QString::fromUtf8(""));
        M_CDB_4_2->setFrameShape(QFrame::Box);
        M_CDB_4_2->setAlignment(Qt::AlignCenter);

        verticalLayout_30->addWidget(M_CDB_4_2);

        M_CDB_5_2 = new QLabel(layoutWidget8);
        M_CDB_5_2->setObjectName("M_CDB_5_2");
        M_CDB_5_2->setFont(font);
        M_CDB_5_2->setStyleSheet(QString::fromUtf8(""));
        M_CDB_5_2->setFrameShape(QFrame::Box);
        M_CDB_5_2->setAlignment(Qt::AlignCenter);

        verticalLayout_30->addWidget(M_CDB_5_2);

        M_CDB_6_2 = new QLabel(layoutWidget8);
        M_CDB_6_2->setObjectName("M_CDB_6_2");
        M_CDB_6_2->setFont(font);
        M_CDB_6_2->setStyleSheet(QString::fromUtf8(""));
        M_CDB_6_2->setFrameShape(QFrame::Box);
        M_CDB_6_2->setAlignment(Qt::AlignCenter);

        verticalLayout_30->addWidget(M_CDB_6_2);

        M_CDB_7_2 = new QLabel(layoutWidget8);
        M_CDB_7_2->setObjectName("M_CDB_7_2");
        M_CDB_7_2->setFont(font);
        M_CDB_7_2->setStyleSheet(QString::fromUtf8(""));
        M_CDB_7_2->setFrameShape(QFrame::Box);
        M_CDB_7_2->setAlignment(Qt::AlignCenter);

        verticalLayout_30->addWidget(M_CDB_7_2);

        layoutWidget_2 = new QWidget(InterfaceTab);
        layoutWidget_2->setObjectName("layoutWidget_2");
        layoutWidget_2->setGeometry(QRect(810, 200, 71, 271));
        verticalLayout_31 = new QVBoxLayout(layoutWidget_2);
        verticalLayout_31->setObjectName("verticalLayout_31");
        verticalLayout_31->setContentsMargins(0, 0, 0, 0);
        label_161 = new QLabel(layoutWidget_2);
        label_161->setObjectName("label_161");

        verticalLayout_31->addWidget(label_161);

        label_162 = new QLabel(layoutWidget_2);
        label_162->setObjectName("label_162");

        verticalLayout_31->addWidget(label_162);

        label_163 = new QLabel(layoutWidget_2);
        label_163->setObjectName("label_163");

        verticalLayout_31->addWidget(label_163);

        label_164 = new QLabel(layoutWidget_2);
        label_164->setObjectName("label_164");

        verticalLayout_31->addWidget(label_164);

        label_165 = new QLabel(layoutWidget_2);
        label_165->setObjectName("label_165");

        verticalLayout_31->addWidget(label_165);

        label_166 = new QLabel(layoutWidget_2);
        label_166->setObjectName("label_166");

        verticalLayout_31->addWidget(label_166);

        label_167 = new QLabel(layoutWidget_2);
        label_167->setObjectName("label_167");

        verticalLayout_31->addWidget(label_167);

        layoutWidget_4 = new QWidget(InterfaceTab);
        layoutWidget_4->setObjectName("layoutWidget_4");
        layoutWidget_4->setGeometry(QRect(990, 200, 71, 271));
        verticalLayout_33 = new QVBoxLayout(layoutWidget_4);
        verticalLayout_33->setObjectName("verticalLayout_33");
        verticalLayout_33->setContentsMargins(0, 0, 0, 0);
        label_168 = new QLabel(layoutWidget_4);
        label_168->setObjectName("label_168");

        verticalLayout_33->addWidget(label_168);

        label_169 = new QLabel(layoutWidget_4);
        label_169->setObjectName("label_169");

        verticalLayout_33->addWidget(label_169);

        label_170 = new QLabel(layoutWidget_4);
        label_170->setObjectName("label_170");

        verticalLayout_33->addWidget(label_170);

        label_171 = new QLabel(layoutWidget_4);
        label_171->setObjectName("label_171");

        verticalLayout_33->addWidget(label_171);

        label_172 = new QLabel(layoutWidget_4);
        label_172->setObjectName("label_172");

        verticalLayout_33->addWidget(label_172);

        label_173 = new QLabel(layoutWidget_4);
        label_173->setObjectName("label_173");

        verticalLayout_33->addWidget(label_173);

        label_174 = new QLabel(layoutWidget_4);
        label_174->setObjectName("label_174");

        verticalLayout_33->addWidget(label_174);

        layoutWidget_3 = new QWidget(InterfaceTab);
        layoutWidget_3->setObjectName("layoutWidget_3");
        layoutWidget_3->setGeometry(QRect(1070, 200, 101, 276));
        verticalLayout_32 = new QVBoxLayout(layoutWidget_3);
        verticalLayout_32->setObjectName("verticalLayout_32");
        verticalLayout_32->setContentsMargins(0, 0, 0, 0);
        M_CDB_1_3 = new QLabel(layoutWidget_3);
        M_CDB_1_3->setObjectName("M_CDB_1_3");
        M_CDB_1_3->setFont(font);
        M_CDB_1_3->setStyleSheet(QString::fromUtf8(""));
        M_CDB_1_3->setFrameShape(QFrame::Box);
        M_CDB_1_3->setAlignment(Qt::AlignCenter);

        verticalLayout_32->addWidget(M_CDB_1_3);

        M_CDB_2_3 = new QLabel(layoutWidget_3);
        M_CDB_2_3->setObjectName("M_CDB_2_3");
        M_CDB_2_3->setFont(font);
        M_CDB_2_3->setStyleSheet(QString::fromUtf8(""));
        M_CDB_2_3->setFrameShape(QFrame::Box);
        M_CDB_2_3->setAlignment(Qt::AlignCenter);

        verticalLayout_32->addWidget(M_CDB_2_3);

        M_CDB_3_3 = new QLabel(layoutWidget_3);
        M_CDB_3_3->setObjectName("M_CDB_3_3");
        M_CDB_3_3->setFont(font);
        M_CDB_3_3->setStyleSheet(QString::fromUtf8(""));
        M_CDB_3_3->setFrameShape(QFrame::Box);
        M_CDB_3_3->setAlignment(Qt::AlignCenter);

        verticalLayout_32->addWidget(M_CDB_3_3);

        M_CDB_4_3 = new QLabel(layoutWidget_3);
        M_CDB_4_3->setObjectName("M_CDB_4_3");
        M_CDB_4_3->setFont(font);
        M_CDB_4_3->setStyleSheet(QString::fromUtf8(""));
        M_CDB_4_3->setFrameShape(QFrame::Box);
        M_CDB_4_3->setAlignment(Qt::AlignCenter);

        verticalLayout_32->addWidget(M_CDB_4_3);

        M_CDB_5_3 = new QLabel(layoutWidget_3);
        M_CDB_5_3->setObjectName("M_CDB_5_3");
        M_CDB_5_3->setFont(font);
        M_CDB_5_3->setStyleSheet(QString::fromUtf8(""));
        M_CDB_5_3->setFrameShape(QFrame::Box);
        M_CDB_5_3->setAlignment(Qt::AlignCenter);

        verticalLayout_32->addWidget(M_CDB_5_3);

        M_CDB_6_3 = new QLabel(layoutWidget_3);
        M_CDB_6_3->setObjectName("M_CDB_6_3");
        M_CDB_6_3->setFont(font);
        M_CDB_6_3->setStyleSheet(QString::fromUtf8(""));
        M_CDB_6_3->setFrameShape(QFrame::Box);
        M_CDB_6_3->setAlignment(Qt::AlignCenter);

        verticalLayout_32->addWidget(M_CDB_6_3);

        M_CDB_7_3 = new QLabel(layoutWidget_3);
        M_CDB_7_3->setObjectName("M_CDB_7_3");
        M_CDB_7_3->setFont(font);
        M_CDB_7_3->setStyleSheet(QString::fromUtf8(""));
        M_CDB_7_3->setFrameShape(QFrame::Box);
        M_CDB_7_3->setAlignment(Qt::AlignCenter);

        verticalLayout_32->addWidget(M_CDB_7_3);

        layoutWidget_5 = new QWidget(InterfaceTab);
        layoutWidget_5->setObjectName("layoutWidget_5");
        layoutWidget_5->setGeometry(QRect(1180, 200, 61, 271));
        verticalLayout_34 = new QVBoxLayout(layoutWidget_5);
        verticalLayout_34->setObjectName("verticalLayout_34");
        verticalLayout_34->setContentsMargins(0, 0, 0, 0);
        label_175 = new QLabel(layoutWidget_5);
        label_175->setObjectName("label_175");

        verticalLayout_34->addWidget(label_175);

        label_176 = new QLabel(layoutWidget_5);
        label_176->setObjectName("label_176");

        verticalLayout_34->addWidget(label_176);

        label_177 = new QLabel(layoutWidget_5);
        label_177->setObjectName("label_177");

        verticalLayout_34->addWidget(label_177);

        label_178 = new QLabel(layoutWidget_5);
        label_178->setObjectName("label_178");

        verticalLayout_34->addWidget(label_178);

        label_179 = new QLabel(layoutWidget_5);
        label_179->setObjectName("label_179");

        verticalLayout_34->addWidget(label_179);

        label_180 = new QLabel(layoutWidget_5);
        label_180->setObjectName("label_180");

        verticalLayout_34->addWidget(label_180);

        label_181 = new QLabel(layoutWidget_5);
        label_181->setObjectName("label_181");

        verticalLayout_34->addWidget(label_181);

        layoutWidget_6 = new QWidget(InterfaceTab);
        layoutWidget_6->setObjectName("layoutWidget_6");
        layoutWidget_6->setGeometry(QRect(1250, 200, 111, 276));
        verticalLayout_35 = new QVBoxLayout(layoutWidget_6);
        verticalLayout_35->setObjectName("verticalLayout_35");
        verticalLayout_35->setContentsMargins(0, 0, 0, 0);
        M_CDB_1_8 = new QLabel(layoutWidget_6);
        M_CDB_1_8->setObjectName("M_CDB_1_8");
        M_CDB_1_8->setFont(font);
        M_CDB_1_8->setStyleSheet(QString::fromUtf8(""));
        M_CDB_1_8->setFrameShape(QFrame::Box);
        M_CDB_1_8->setAlignment(Qt::AlignCenter);

        verticalLayout_35->addWidget(M_CDB_1_8);

        M_CDB_2_8 = new QLabel(layoutWidget_6);
        M_CDB_2_8->setObjectName("M_CDB_2_8");
        M_CDB_2_8->setFont(font);
        M_CDB_2_8->setStyleSheet(QString::fromUtf8(""));
        M_CDB_2_8->setFrameShape(QFrame::Box);
        M_CDB_2_8->setAlignment(Qt::AlignCenter);

        verticalLayout_35->addWidget(M_CDB_2_8);

        M_CDB_3_8 = new QLabel(layoutWidget_6);
        M_CDB_3_8->setObjectName("M_CDB_3_8");
        M_CDB_3_8->setFont(font);
        M_CDB_3_8->setStyleSheet(QString::fromUtf8(""));
        M_CDB_3_8->setFrameShape(QFrame::Box);
        M_CDB_3_8->setAlignment(Qt::AlignCenter);

        verticalLayout_35->addWidget(M_CDB_3_8);

        M_CDB_4_8 = new QLabel(layoutWidget_6);
        M_CDB_4_8->setObjectName("M_CDB_4_8");
        M_CDB_4_8->setFont(font);
        M_CDB_4_8->setStyleSheet(QString::fromUtf8(""));
        M_CDB_4_8->setFrameShape(QFrame::Box);
        M_CDB_4_8->setAlignment(Qt::AlignCenter);

        verticalLayout_35->addWidget(M_CDB_4_8);

        M_CDB_5_8 = new QLabel(layoutWidget_6);
        M_CDB_5_8->setObjectName("M_CDB_5_8");
        M_CDB_5_8->setFont(font);
        M_CDB_5_8->setStyleSheet(QString::fromUtf8(""));
        M_CDB_5_8->setFrameShape(QFrame::Box);
        M_CDB_5_8->setAlignment(Qt::AlignCenter);

        verticalLayout_35->addWidget(M_CDB_5_8);

        M_CDB_6_8 = new QLabel(layoutWidget_6);
        M_CDB_6_8->setObjectName("M_CDB_6_8");
        M_CDB_6_8->setFont(font);
        M_CDB_6_8->setStyleSheet(QString::fromUtf8(""));
        M_CDB_6_8->setFrameShape(QFrame::Box);
        M_CDB_6_8->setAlignment(Qt::AlignCenter);

        verticalLayout_35->addWidget(M_CDB_6_8);

        M_CDB_7_8 = new QLabel(layoutWidget_6);
        M_CDB_7_8->setObjectName("M_CDB_7_8");
        M_CDB_7_8->setFont(font);
        M_CDB_7_8->setStyleSheet(QString::fromUtf8(""));
        M_CDB_7_8->setFrameShape(QFrame::Box);
        M_CDB_7_8->setAlignment(Qt::AlignCenter);

        verticalLayout_35->addWidget(M_CDB_7_8);

        label_153 = new QLabel(InterfaceTab);
        label_153->setObjectName("label_153");
        label_153->setGeometry(QRect(680, 90, 81, 16));
        M_UPCOUT = new QLabel(InterfaceTab);
        M_UPCOUT->setObjectName("M_UPCOUT");
        M_UPCOUT->setGeometry(QRect(760, 60, 81, 21));
        M_UPCOUT->setFont(font);
        M_UPCOUT->setStyleSheet(QString::fromUtf8(""));
        M_UPCOUT->setFrameShape(QFrame::Box);
        M_UPCOUT->setAlignment(Qt::AlignCenter);
        M_I_PLC_2 = new QLabel(InterfaceTab);
        M_I_PLC_2->setObjectName("M_I_PLC_2");
        M_I_PLC_2->setGeometry(QRect(760, 90, 81, 21));
        M_I_PLC_2->setFont(font);
        M_I_PLC_2->setStyleSheet(QString::fromUtf8(""));
        M_I_PLC_2->setFrameShape(QFrame::Box);
        M_I_PLC_2->setAlignment(Qt::AlignCenter);
        label_130 = new QLabel(InterfaceTab);
        label_130->setObjectName("label_130");
        label_130->setGeometry(QRect(680, 60, 81, 16));
        DutTestTabs->addTab(InterfaceTab, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName("tab_3");
        groupBox_3 = new QGroupBox(tab_3);
        groupBox_3->setObjectName("groupBox_3");
        groupBox_3->setGeometry(QRect(10, 20, 171, 261));
        pushButton_11 = new QPushButton(groupBox_3);
        pushButton_11->setObjectName("pushButton_11");
        pushButton_11->setGeometry(QRect(10, 110, 75, 24));
        pushButton_12 = new QPushButton(groupBox_3);
        pushButton_12->setObjectName("pushButton_12");
        pushButton_12->setGeometry(QRect(10, 20, 75, 24));
        pushButton_13 = new QPushButton(groupBox_3);
        pushButton_13->setObjectName("pushButton_13");
        pushButton_13->setGeometry(QRect(10, 80, 75, 24));
        pushButton_14 = new QPushButton(groupBox_3);
        pushButton_14->setObjectName("pushButton_14");
        pushButton_14->setGeometry(QRect(10, 50, 75, 24));
        pushButton_15 = new QPushButton(groupBox_3);
        pushButton_15->setObjectName("pushButton_15");
        pushButton_15->setGeometry(QRect(10, 230, 75, 24));
        pushButton_16 = new QPushButton(groupBox_3);
        pushButton_16->setObjectName("pushButton_16");
        pushButton_16->setGeometry(QRect(10, 200, 75, 24));
        pushButton_17 = new QPushButton(groupBox_3);
        pushButton_17->setObjectName("pushButton_17");
        pushButton_17->setGeometry(QRect(10, 170, 75, 24));
        pushButton_18 = new QPushButton(groupBox_3);
        pushButton_18->setObjectName("pushButton_18");
        pushButton_18->setGeometry(QRect(10, 140, 75, 24));
        progressBar_9 = new QProgressBar(groupBox_3);
        progressBar_9->setObjectName("progressBar_9");
        progressBar_9->setGeometry(QRect(90, 20, 81, 23));
        progressBar_9->setValue(24);
        progressBar_10 = new QProgressBar(groupBox_3);
        progressBar_10->setObjectName("progressBar_10");
        progressBar_10->setGeometry(QRect(90, 50, 81, 23));
        progressBar_10->setValue(24);
        progressBar_11 = new QProgressBar(groupBox_3);
        progressBar_11->setObjectName("progressBar_11");
        progressBar_11->setGeometry(QRect(90, 110, 81, 23));
        progressBar_11->setValue(24);
        progressBar_12 = new QProgressBar(groupBox_3);
        progressBar_12->setObjectName("progressBar_12");
        progressBar_12->setGeometry(QRect(90, 80, 81, 23));
        progressBar_12->setValue(24);
        progressBar_13 = new QProgressBar(groupBox_3);
        progressBar_13->setObjectName("progressBar_13");
        progressBar_13->setGeometry(QRect(90, 170, 81, 23));
        progressBar_13->setValue(24);
        progressBar_14 = new QProgressBar(groupBox_3);
        progressBar_14->setObjectName("progressBar_14");
        progressBar_14->setGeometry(QRect(90, 200, 81, 23));
        progressBar_14->setValue(24);
        progressBar_15 = new QProgressBar(groupBox_3);
        progressBar_15->setObjectName("progressBar_15");
        progressBar_15->setGeometry(QRect(90, 140, 81, 23));
        progressBar_15->setValue(24);
        progressBar_16 = new QProgressBar(groupBox_3);
        progressBar_16->setObjectName("progressBar_16");
        progressBar_16->setGeometry(QRect(90, 230, 81, 23));
        progressBar_16->setValue(24);
        DutTestTabs->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName("tab_4");
        groupBox = new QGroupBox(tab_4);
        groupBox->setObjectName("groupBox");
        groupBox->setGeometry(QRect(10, 10, 171, 261));
        pushButton_6 = new QPushButton(groupBox);
        pushButton_6->setObjectName("pushButton_6");
        pushButton_6->setGeometry(QRect(10, 110, 75, 24));
        pushButton_3 = new QPushButton(groupBox);
        pushButton_3->setObjectName("pushButton_3");
        pushButton_3->setGeometry(QRect(10, 20, 75, 24));
        pushButton_5 = new QPushButton(groupBox);
        pushButton_5->setObjectName("pushButton_5");
        pushButton_5->setGeometry(QRect(10, 80, 75, 24));
        pushButton_4 = new QPushButton(groupBox);
        pushButton_4->setObjectName("pushButton_4");
        pushButton_4->setGeometry(QRect(10, 50, 75, 24));
        pushButton_10 = new QPushButton(groupBox);
        pushButton_10->setObjectName("pushButton_10");
        pushButton_10->setGeometry(QRect(10, 230, 75, 24));
        pushButton_8 = new QPushButton(groupBox);
        pushButton_8->setObjectName("pushButton_8");
        pushButton_8->setGeometry(QRect(10, 200, 75, 24));
        pushButton_9 = new QPushButton(groupBox);
        pushButton_9->setObjectName("pushButton_9");
        pushButton_9->setGeometry(QRect(10, 170, 75, 24));
        pushButton_7 = new QPushButton(groupBox);
        pushButton_7->setObjectName("pushButton_7");
        pushButton_7->setGeometry(QRect(10, 140, 75, 24));
        progressBar = new QProgressBar(groupBox);
        progressBar->setObjectName("progressBar");
        progressBar->setGeometry(QRect(90, 20, 81, 23));
        progressBar->setValue(24);
        progressBar_2 = new QProgressBar(groupBox);
        progressBar_2->setObjectName("progressBar_2");
        progressBar_2->setGeometry(QRect(90, 50, 81, 23));
        progressBar_2->setValue(24);
        progressBar_3 = new QProgressBar(groupBox);
        progressBar_3->setObjectName("progressBar_3");
        progressBar_3->setGeometry(QRect(90, 110, 81, 23));
        progressBar_3->setValue(24);
        progressBar_4 = new QProgressBar(groupBox);
        progressBar_4->setObjectName("progressBar_4");
        progressBar_4->setGeometry(QRect(90, 80, 81, 23));
        progressBar_4->setValue(24);
        progressBar_5 = new QProgressBar(groupBox);
        progressBar_5->setObjectName("progressBar_5");
        progressBar_5->setGeometry(QRect(90, 170, 81, 23));
        progressBar_5->setValue(24);
        progressBar_6 = new QProgressBar(groupBox);
        progressBar_6->setObjectName("progressBar_6");
        progressBar_6->setGeometry(QRect(90, 200, 81, 23));
        progressBar_6->setValue(24);
        progressBar_7 = new QProgressBar(groupBox);
        progressBar_7->setObjectName("progressBar_7");
        progressBar_7->setGeometry(QRect(90, 140, 81, 23));
        progressBar_7->setValue(24);
        progressBar_8 = new QProgressBar(groupBox);
        progressBar_8->setObjectName("progressBar_8");
        progressBar_8->setGeometry(QRect(90, 230, 81, 23));
        progressBar_8->setValue(24);
        DutTestTabs->addTab(tab_4, QString());
        tab = new QWidget();
        tab->setObjectName("tab");
        label_65 = new QLabel(tab);
        label_65->setObjectName("label_65");
        label_65->setGeometry(QRect(350, 80, 521, 231));
        QFont font1;
        font1.setPointSize(65);
        font1.setBold(true);
        label_65->setFont(font1);
        label_65->setFrameShape(QFrame::Box);
        label_65->setAlignment(Qt::AlignCenter);
        DutTestTabs->addTab(tab, QString());
        Power = new QGroupBox(centralwidget);
        Power->setObjectName("Power");
        Power->setGeometry(QRect(400, 30, 191, 81));
        PBTesterOFF = new QPushButton(Power);
        PBTesterOFF->setObjectName("PBTesterOFF");
        PBTesterOFF->setGeometry(QRect(10, 50, 75, 24));
        PBTesterON = new QPushButton(Power);
        PBTesterON->setObjectName("PBTesterON");
        PBTesterON->setGeometry(QRect(10, 20, 75, 24));
        L_TesterON = new QLabel(Power);
        L_TesterON->setObjectName("L_TesterON");
        L_TesterON->setGeometry(QRect(100, 30, 81, 41));
        L_TesterON->setFont(font);
        L_TesterON->setStyleSheet(QString::fromUtf8(""));
        L_TesterON->setFrameShape(QFrame::Box);
        L_TesterON->setAlignment(Qt::AlignCenter);
        TestSelect = new QGroupBox(centralwidget);
        TestSelect->setObjectName("TestSelect");
        TestSelect->setGeometry(QRect(600, 30, 201, 81));
        PBINTFtest = new QPushButton(TestSelect);
        PBINTFtest->setObjectName("PBINTFtest");
        PBINTFtest->setGeometry(QRect(110, 20, 75, 24));
        PBCONTtest = new QPushButton(TestSelect);
        PBCONTtest->setObjectName("PBCONTtest");
        PBCONTtest->setGeometry(QRect(10, 20, 75, 24));
        PBSignalsReset = new QPushButton(TestSelect);
        PBSignalsReset->setObjectName("PBSignalsReset");
        PBSignalsReset->setGeometry(QRect(60, 50, 75, 24));
        label_63 = new QLabel(centralwidget);
        label_63->setObjectName("label_63");
        label_63->setGeometry(QRect(470, 10, 41, 16));
        M_V24V2_I = new QLabel(centralwidget);
        M_V24V2_I->setObjectName("M_V24V2_I");
        M_V24V2_I->setGeometry(QRect(510, 10, 81, 21));
        M_V24V2_I->setFont(font);
        M_V24V2_I->setStyleSheet(QString::fromUtf8(""));
        M_V24V2_I->setFrameShape(QFrame::Box);
        M_V24V2_I->setAlignment(Qt::AlignCenter);
        MainWindow->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName("statusbar");
        MainWindow->setStatusBar(statusbar);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 1524, 22));
        menuMain = new QMenu(menubar);
        menuMain->setObjectName("menuMain");
        MainWindow->setMenuBar(menubar);

        menubar->addAction(menuMain->menuAction());

        retranslateUi(MainWindow);

        DutTestTabs->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actiondada->setText(QCoreApplication::translate("MainWindow", "dada", nullptr));
        ProgramName->setText(QCoreApplication::translate("MainWindow", "BBB CDB Tester", nullptr));
        Ethernet->setTitle(QCoreApplication::translate("MainWindow", "Ethernet", nullptr));
        PBConnect->setText(QCoreApplication::translate("MainWindow", "Connect", nullptr));
        PBDisconnect->setText(QCoreApplication::translate("MainWindow", "Disconnect", nullptr));
#if QT_CONFIG(tooltip)
        TextIP->setToolTip(QCoreApplication::translate("MainWindow", "<html><head/><body><p>IP of BBB</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        TextIP->setText(QCoreApplication::translate("MainWindow", "192.168.137.95", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Port", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "IP", nullptr));
#if QT_CONFIG(tooltip)
        TextPort->setToolTip(QCoreApplication::translate("MainWindow", "<html><head/><body><p>IP of BBB</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        TextPort->setText(QCoreApplication::translate("MainWindow", "12345", nullptr));
        L_Connected->setText(QString());
        label_48->setText(QCoreApplication::translate("MainWindow", "C_PLC", nullptr));
        label_55->setText(QCoreApplication::translate("MainWindow", "To 5V", nullptr));
        C_PLC_1->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        N->setText(QCoreApplication::translate("MainWindow", "N", nullptr));
        T->setText(QCoreApplication::translate("MainWindow", "T", nullptr));
        S->setText(QCoreApplication::translate("MainWindow", "S", nullptr));
        R->setText(QCoreApplication::translate("MainWindow", "R", nullptr));
        label_56->setText(QCoreApplication::translate("MainWindow", "To 24V", nullptr));
        PBCONTOn->setText(QCoreApplication::translate("MainWindow", "CONT ON", nullptr));
        label_58->setText(QCoreApplication::translate("MainWindow", "C_I", nullptr));
        label_60->setText(QCoreApplication::translate("MainWindow", "To 5V", nullptr));
        C_I_1->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        label_59->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        C_I_1213->setText(QCoreApplication::translate("MainWindow", "12, 13", nullptr));
        label_61->setText(QCoreApplication::translate("MainWindow", "Give 24V", nullptr));
        C_I_1516->setText(QCoreApplication::translate("MainWindow", "15, 16", nullptr));
        C_I_1718->setText(QCoreApplication::translate("MainWindow", "17, 18", nullptr));
        label_54->setText(QCoreApplication::translate("MainWindow", "LS", nullptr));
        label_62->setText(QCoreApplication::translate("MainWindow", "To LS1", nullptr));
        LS_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        LS_10->setText(QCoreApplication::translate("MainWindow", "10", nullptr));
        LS_11->setText(QCoreApplication::translate("MainWindow", "11", nullptr));
        LS_14->setText(QCoreApplication::translate("MainWindow", "14", nullptr));
        LS_15->setText(QCoreApplication::translate("MainWindow", "15", nullptr));
        label_53->setText(QCoreApplication::translate("MainWindow", "To LS1", nullptr));
        LS_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        LS_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        LS_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        LS_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        LS_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        label_57->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        LS_1617->setText(QCoreApplication::translate("MainWindow", "1617", nullptr));
        label_50->setText(QCoreApplication::translate("MainWindow", "ES1", nullptr));
        label_49->setText(QCoreApplication::translate("MainWindow", "To ES1_1", nullptr));
        ES1_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        ES1_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        ES1_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        ES1_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        ES1_6->setText(QCoreApplication::translate("MainWindow", "6", nullptr));
        ES1_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        ES1_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        ES1_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        label_51->setText(QCoreApplication::translate("MainWindow", "ES2", nullptr));
        label_52->setText(QCoreApplication::translate("MainWindow", "To ES2_1", nullptr));
        ES2_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        ES2_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        ES2_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        ES2_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        ES2_6->setText(QCoreApplication::translate("MainWindow", "6", nullptr));
        ES2_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        ES2_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        ES2_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        PBCONTOff->setText(QCoreApplication::translate("MainWindow", "CONT OFF", nullptr));
        L_CONTON->setText(QString());
        label_64->setText(QCoreApplication::translate("MainWindow", "24V UPS OUT", nullptr));
        M_UPSOUT->setText(QString());
        label_142->setText(QCoreApplication::translate("MainWindow", "C_PLC_5", nullptr));
        label_143->setText(QCoreApplication::translate("MainWindow", "C_PLC_7", nullptr));
        label_138->setText(QCoreApplication::translate("MainWindow", "C_PLC_3", nullptr));
        label_144->setText(QCoreApplication::translate("MainWindow", "C_PLC_9", nullptr));
        label_145->setText(QCoreApplication::translate("MainWindow", "C_PLC_10", nullptr));
        label_140->setText(QCoreApplication::translate("MainWindow", "C_PLC_2", nullptr));
        label_137->setText(QCoreApplication::translate("MainWindow", "C_PLC_6", nullptr));
        label_139->setText(QCoreApplication::translate("MainWindow", "C_PLC_8", nullptr));
        label_141->setText(QCoreApplication::translate("MainWindow", "C_PLC_4", nullptr));
        M_C_PLC_2->setText(QString());
        M_C_PLC_3->setText(QString());
        M_C_PLC_4->setText(QString());
        M_C_PLC_5->setText(QString());
        M_C_PLC_6->setText(QString());
        M_C_PLC_7->setText(QString());
        M_C_PLC_8->setText(QString());
        M_C_PLC_9->setText(QString());
        M_C_PLC_10->setText(QString());
        label_146->setText(QCoreApplication::translate("MainWindow", "MOT2", nullptr));
        label_147->setText(QCoreApplication::translate("MainWindow", "MOT3", nullptr));
        label_148->setText(QCoreApplication::translate("MainWindow", "MOT6", nullptr));
        label_149->setText(QCoreApplication::translate("MainWindow", "400V_OUT2", nullptr));
        label_150->setText(QCoreApplication::translate("MainWindow", "400V_OUT3", nullptr));
        label_151->setText(QCoreApplication::translate("MainWindow", "400V_OUT5", nullptr));
        label_152->setText(QCoreApplication::translate("MainWindow", "400V_OUT6", nullptr));
        M_MOT_2->setText(QString());
        M_MOT_3->setText(QString());
        M_MOT_6->setText(QString());
        M_400V_OUT_2->setText(QString());
        M_400V_OUT_3->setText(QString());
        M_400V_OUT_5->setText(QString());
        M_400V_OUT_6->setText(QString());
        label_129->setText(QCoreApplication::translate("MainWindow", "CI_2", nullptr));
        label_131->setText(QCoreApplication::translate("MainWindow", "CI_3", nullptr));
        label_132->setText(QCoreApplication::translate("MainWindow", "CI_4", nullptr));
        label_133->setText(QCoreApplication::translate("MainWindow", "CI_5", nullptr));
        label_134->setText(QCoreApplication::translate("MainWindow", "CI_7", nullptr));
        label_135->setText(QCoreApplication::translate("MainWindow", "CI_8", nullptr));
        label_136->setText(QCoreApplication::translate("MainWindow", "CI_9", nullptr));
        M_CI_2->setText(QString());
        M_CI_3->setText(QString());
        M_CI_4->setText(QString());
        M_CI_5->setText(QString());
        M_CI_7->setText(QString());
        M_CI_8->setText(QString());
        M_CI_9->setText(QString());
        DutTestTabs->setTabText(DutTestTabs->indexOf(ControlTab), QCoreApplication::translate("MainWindow", "Tab 1", nullptr));
        label_34->setText(QCoreApplication::translate("MainWindow", "I_PLC", nullptr));
        label_35->setText(QCoreApplication::translate("MainWindow", "To 5V", nullptr));
        I_PLC_1->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        label_36->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        I_PLC_0506->setText(QCoreApplication::translate("MainWindow", "5,6", nullptr));
        I_PLC_0708->setText(QCoreApplication::translate("MainWindow", "7,8", nullptr));
        I_PLC_0910->setText(QCoreApplication::translate("MainWindow", "9,10", nullptr));
        I_PLC_1112->setText(QCoreApplication::translate("MainWindow", "11,12", nullptr));
        I_PLC_1314->setText(QCoreApplication::translate("MainWindow", "13,14", nullptr));
        I_PLC_1516->setText(QCoreApplication::translate("MainWindow", "15,16", nullptr));
        I_PLC_1718->setText(QCoreApplication::translate("MainWindow", "17,18", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "C_I1", nullptr));
        label_41->setText(QCoreApplication::translate("MainWindow", "To C_I1_1", nullptr));
        C_I1_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        C_I1_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        C_I1_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        C_I1_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        C_I1_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        C_I1_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        C_I1_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        C_I1_12->setText(QCoreApplication::translate("MainWindow", "12", nullptr));
        label_25->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        C_I1_1516->setText(QCoreApplication::translate("MainWindow", "15,16", nullptr));
        C_I1_1718->setText(QCoreApplication::translate("MainWindow", "17,18", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindow", "C_I2", nullptr));
        label_42->setText(QCoreApplication::translate("MainWindow", "To C_I1_1", nullptr));
        C_I2_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        C_I2_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        C_I2_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        C_I2_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        C_I2_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        C_I2_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        C_I2_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        C_I2_12->setText(QCoreApplication::translate("MainWindow", "12", nullptr));
        label_26->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        C_I2_1516->setText(QCoreApplication::translate("MainWindow", "15,16", nullptr));
        C_I2_1718->setText(QCoreApplication::translate("MainWindow", "17,18", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "C_I3", nullptr));
        label_43->setText(QCoreApplication::translate("MainWindow", "To C_I1_1", nullptr));
        C_I3_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        C_I3_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        C_I3_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        C_I3_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        C_I3_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        C_I3_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        C_I3_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        C_I3_12->setText(QCoreApplication::translate("MainWindow", "12", nullptr));
        label_27->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        C_I3_1516->setText(QCoreApplication::translate("MainWindow", "15,16", nullptr));
        C_I3_1718->setText(QCoreApplication::translate("MainWindow", "17,18", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "C_I4", nullptr));
        label_46->setText(QCoreApplication::translate("MainWindow", "To C_I1_1", nullptr));
        C_I4_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        C_I4_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        C_I4_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        C_I4_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        C_I4_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        C_I4_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        C_I4_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        C_I4_12->setText(QCoreApplication::translate("MainWindow", "12", nullptr));
        label_39->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        C_I4_1516->setText(QCoreApplication::translate("MainWindow", "15,16", nullptr));
        C_I4_1718->setText(QCoreApplication::translate("MainWindow", "17,18", nullptr));
        label_7->setText(QCoreApplication::translate("MainWindow", "C_I5", nullptr));
        label_44->setText(QCoreApplication::translate("MainWindow", "To C_I1_1", nullptr));
        C_I5_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        C_I5_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        C_I5_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        C_I5_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        C_I5_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        C_I5_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        C_I5_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        C_I5_12->setText(QCoreApplication::translate("MainWindow", "12", nullptr));
        label_37->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        C_I5_1516->setText(QCoreApplication::translate("MainWindow", "15,16", nullptr));
        C_I5_1718->setText(QCoreApplication::translate("MainWindow", "17,18", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", "C_I6", nullptr));
        label_45->setText(QCoreApplication::translate("MainWindow", "To C_I1_1", nullptr));
        C_I6_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        C_I6_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        C_I6_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        C_I6_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        C_I6_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        C_I6_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        C_I6_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        C_I6_12->setText(QCoreApplication::translate("MainWindow", "12", nullptr));
        label_38->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        C_I6_1516->setText(QCoreApplication::translate("MainWindow", "15,16", nullptr));
        C_I6_1718->setText(QCoreApplication::translate("MainWindow", "17,18", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "C_I7", nullptr));
        label_47->setText(QCoreApplication::translate("MainWindow", "To C_I1_1", nullptr));
        C_I7_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        C_I7_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        C_I7_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        C_I7_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        C_I7_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        C_I7_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        C_I7_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        C_I7_12->setText(QCoreApplication::translate("MainWindow", "12", nullptr));
        label_40->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        C_I7_1516->setText(QCoreApplication::translate("MainWindow", "15,16", nullptr));
        C_I7_1718->setText(QCoreApplication::translate("MainWindow", "17,18", nullptr));
        PBINTFOn->setText(QCoreApplication::translate("MainWindow", "INTF ON", nullptr));
        DOOR->setText(QCoreApplication::translate("MainWindow", "SK DOOR", nullptr));
        STOP->setText(QCoreApplication::translate("MainWindow", "SK STOP", nullptr));
        label_10->setText(QCoreApplication::translate("MainWindow", "CDB1", nullptr));
        label_11->setText(QCoreApplication::translate("MainWindow", "To 5V", nullptr));
        CDB1_0104->setText(QCoreApplication::translate("MainWindow", "1, 4", nullptr));
        CDB1_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        label_12->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        CDB1_0506->setText(QCoreApplication::translate("MainWindow", "5,6", nullptr));
        label_13->setText(QCoreApplication::translate("MainWindow", "CDB2", nullptr));
        label_14->setText(QCoreApplication::translate("MainWindow", "To 5V", nullptr));
        CDB2_0104->setText(QCoreApplication::translate("MainWindow", "1, 4", nullptr));
        CDB2_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        label_15->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        CDB2_0506->setText(QCoreApplication::translate("MainWindow", "5,6", nullptr));
        label_16->setText(QCoreApplication::translate("MainWindow", "CDB3", nullptr));
        label_17->setText(QCoreApplication::translate("MainWindow", "To 5V", nullptr));
        CDB3_0104->setText(QCoreApplication::translate("MainWindow", "1, 4", nullptr));
        CDB3_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        label_18->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        CDB3_0506->setText(QCoreApplication::translate("MainWindow", "5,6", nullptr));
        label_19->setText(QCoreApplication::translate("MainWindow", "CDB4", nullptr));
        label_20->setText(QCoreApplication::translate("MainWindow", "To 5V", nullptr));
        CDB4_0104->setText(QCoreApplication::translate("MainWindow", "1, 4", nullptr));
        CDB4_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        label_21->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        CDB4_0506->setText(QCoreApplication::translate("MainWindow", "5,6", nullptr));
        label_22->setText(QCoreApplication::translate("MainWindow", "CDB5", nullptr));
        label_23->setText(QCoreApplication::translate("MainWindow", "To 5V", nullptr));
        CDB5_0104->setText(QCoreApplication::translate("MainWindow", "1, 4", nullptr));
        CDB5_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        label_24->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        CDB5_0506->setText(QCoreApplication::translate("MainWindow", "5,6", nullptr));
        label_28->setText(QCoreApplication::translate("MainWindow", "CDB6", nullptr));
        label_29->setText(QCoreApplication::translate("MainWindow", "To 5V", nullptr));
        CDB6_0104->setText(QCoreApplication::translate("MainWindow", "1, 4", nullptr));
        CDB6_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        label_30->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        CDB6_0506->setText(QCoreApplication::translate("MainWindow", "5,6", nullptr));
        label_31->setText(QCoreApplication::translate("MainWindow", "CDB7", nullptr));
        label_32->setText(QCoreApplication::translate("MainWindow", "To 5V", nullptr));
        CDB7_0104->setText(QCoreApplication::translate("MainWindow", "1, 4", nullptr));
        CDB7_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        label_33->setText(QCoreApplication::translate("MainWindow", "Bridge", nullptr));
        CDB7_0506->setText(QCoreApplication::translate("MainWindow", "5,6", nullptr));
        PBINTFOff->setText(QCoreApplication::translate("MainWindow", "INTF OFF", nullptr));
        L_INTFON->setText(QString());
        label_154->setText(QCoreApplication::translate("MainWindow", "C_I_1_13", nullptr));
        label_155->setText(QCoreApplication::translate("MainWindow", "C_I_2_13", nullptr));
        label_156->setText(QCoreApplication::translate("MainWindow", "C_I_3_13", nullptr));
        label_157->setText(QCoreApplication::translate("MainWindow", "C_I_4_13", nullptr));
        label_158->setText(QCoreApplication::translate("MainWindow", "C_I_5_13", nullptr));
        label_159->setText(QCoreApplication::translate("MainWindow", "C_I_6_13", nullptr));
        label_160->setText(QCoreApplication::translate("MainWindow", "C_I_7_13", nullptr));
        M_C_I_1_13->setText(QString());
        M_C_I_2_13->setText(QString());
        M_C_I_3_13->setText(QString());
        M_C_I_4_13->setText(QString());
        M_C_I_5_13->setText(QString());
        M_C_I_6_13->setText(QString());
        M_C_I_7_13->setText(QString());
        M_CDB_1_2->setText(QString());
        M_CDB_2_2->setText(QString());
        M_CDB_3_2->setText(QString());
        M_CDB_4_2->setText(QString());
        M_CDB_5_2->setText(QString());
        M_CDB_6_2->setText(QString());
        M_CDB_7_2->setText(QString());
        label_161->setText(QCoreApplication::translate("MainWindow", "CDB_1_2", nullptr));
        label_162->setText(QCoreApplication::translate("MainWindow", "CDB_2_2", nullptr));
        label_163->setText(QCoreApplication::translate("MainWindow", "CDB_3_2", nullptr));
        label_164->setText(QCoreApplication::translate("MainWindow", "CDB_4_2", nullptr));
        label_165->setText(QCoreApplication::translate("MainWindow", "CDB_5_2", nullptr));
        label_166->setText(QCoreApplication::translate("MainWindow", "CDB_6_2", nullptr));
        label_167->setText(QCoreApplication::translate("MainWindow", "CDB_7_2", nullptr));
        label_168->setText(QCoreApplication::translate("MainWindow", "CDB_1_3", nullptr));
        label_169->setText(QCoreApplication::translate("MainWindow", "CDB_2_3", nullptr));
        label_170->setText(QCoreApplication::translate("MainWindow", "CDB_3_3", nullptr));
        label_171->setText(QCoreApplication::translate("MainWindow", "CDB_4_3", nullptr));
        label_172->setText(QCoreApplication::translate("MainWindow", "CDB_5_3", nullptr));
        label_173->setText(QCoreApplication::translate("MainWindow", "CDB_6_3", nullptr));
        label_174->setText(QCoreApplication::translate("MainWindow", "CDB_7_3", nullptr));
        M_CDB_1_3->setText(QString());
        M_CDB_2_3->setText(QString());
        M_CDB_3_3->setText(QString());
        M_CDB_4_3->setText(QString());
        M_CDB_5_3->setText(QString());
        M_CDB_6_3->setText(QString());
        M_CDB_7_3->setText(QString());
        label_175->setText(QCoreApplication::translate("MainWindow", "CDB_1_8", nullptr));
        label_176->setText(QCoreApplication::translate("MainWindow", "CDB_2_8", nullptr));
        label_177->setText(QCoreApplication::translate("MainWindow", "CDB_3_8", nullptr));
        label_178->setText(QCoreApplication::translate("MainWindow", "CDB_4_8", nullptr));
        label_179->setText(QCoreApplication::translate("MainWindow", "CDB_5_8", nullptr));
        label_180->setText(QCoreApplication::translate("MainWindow", "CDB_6_8", nullptr));
        label_181->setText(QCoreApplication::translate("MainWindow", "CDB_7_8", nullptr));
        M_CDB_1_8->setText(QString());
        M_CDB_2_8->setText(QString());
        M_CDB_3_8->setText(QString());
        M_CDB_4_8->setText(QString());
        M_CDB_5_8->setText(QString());
        M_CDB_6_8->setText(QString());
        M_CDB_7_8->setText(QString());
        label_153->setText(QCoreApplication::translate("MainWindow", "I_PLC_2", nullptr));
        M_UPCOUT->setText(QString());
        M_I_PLC_2->setText(QString());
        label_130->setText(QCoreApplication::translate("MainWindow", "24 V UPC OUT", nullptr));
        DutTestTabs->setTabText(DutTestTabs->indexOf(InterfaceTab), QCoreApplication::translate("MainWindow", "Tab 2", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("MainWindow", "CONTROL", nullptr));
        pushButton_11->setText(QCoreApplication::translate("MainWindow", "Test4", nullptr));
        pushButton_12->setText(QCoreApplication::translate("MainWindow", "Test1", nullptr));
        pushButton_13->setText(QCoreApplication::translate("MainWindow", "Test3", nullptr));
        pushButton_14->setText(QCoreApplication::translate("MainWindow", "Test2", nullptr));
        pushButton_15->setText(QCoreApplication::translate("MainWindow", "Test8", nullptr));
        pushButton_16->setText(QCoreApplication::translate("MainWindow", "Test7", nullptr));
        pushButton_17->setText(QCoreApplication::translate("MainWindow", "Test6", nullptr));
        pushButton_18->setText(QCoreApplication::translate("MainWindow", "Test5", nullptr));
        DutTestTabs->setTabText(DutTestTabs->indexOf(tab_3), QCoreApplication::translate("MainWindow", "Page", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "INTERFACE", nullptr));
        pushButton_6->setText(QCoreApplication::translate("MainWindow", "Test4", nullptr));
        pushButton_3->setText(QCoreApplication::translate("MainWindow", "Test1", nullptr));
        pushButton_5->setText(QCoreApplication::translate("MainWindow", "Test3", nullptr));
        pushButton_4->setText(QCoreApplication::translate("MainWindow", "Test2", nullptr));
        pushButton_10->setText(QCoreApplication::translate("MainWindow", "Test8", nullptr));
        pushButton_8->setText(QCoreApplication::translate("MainWindow", "Test7", nullptr));
        pushButton_9->setText(QCoreApplication::translate("MainWindow", "Test6", nullptr));
        pushButton_7->setText(QCoreApplication::translate("MainWindow", "Test5", nullptr));
        DutTestTabs->setTabText(DutTestTabs->indexOf(tab_4), QCoreApplication::translate("MainWindow", "Page", nullptr));
        label_65->setText(QCoreApplication::translate("MainWindow", "Welcome", nullptr));
        DutTestTabs->setTabText(DutTestTabs->indexOf(tab), QCoreApplication::translate("MainWindow", "Page", nullptr));
        Power->setTitle(QCoreApplication::translate("MainWindow", "Power", nullptr));
        PBTesterOFF->setText(QCoreApplication::translate("MainWindow", "Tester OFF", nullptr));
        PBTesterON->setText(QCoreApplication::translate("MainWindow", "Tester ON", nullptr));
        L_TesterON->setText(QString());
        TestSelect->setTitle(QCoreApplication::translate("MainWindow", "Test select", nullptr));
        PBINTFtest->setText(QCoreApplication::translate("MainWindow", "INTERFACE", nullptr));
        PBCONTtest->setText(QCoreApplication::translate("MainWindow", "CONTROL", nullptr));
        PBSignalsReset->setText(QCoreApplication::translate("MainWindow", "Signal reset", nullptr));
        label_63->setText(QCoreApplication::translate("MainWindow", "24V2 I", nullptr));
        M_V24V2_I->setText(QString());
        menuMain->setTitle(QCoreApplication::translate("MainWindow", "Main", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
