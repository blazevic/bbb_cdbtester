/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.5.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../CDB_GUI/mainwindow.h"
#include <QtCore/qmetatype.h>

#if __has_include(<QtCore/qtmochelpers.h>)
#include <QtCore/qtmochelpers.h>
#else
QT_BEGIN_MOC_NAMESPACE
#endif


#include <memory>

#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.5.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
QT_WARNING_DISABLE_GCC("-Wuseless-cast")
namespace {

#ifdef QT_MOC_HAS_STRINGDATA
struct qt_meta_stringdata_CLASSMainWindowENDCLASS_t {};
static constexpr auto qt_meta_stringdata_CLASSMainWindowENDCLASS = QtMocHelpers::stringData(
    "MainWindow",
    "update_data",
    "",
    "handlePB_Connect",
    "handlePB_Disconnect",
    "handlePBC_ContOn",
    "handlePBC_ContOff",
    "handlePBI_IntfOn",
    "handlePBI_IntfOff",
    "handlePB_TesterOn",
    "handlePB_TesterOff",
    "handlePBI_IntfTest",
    "handlePBC_ContTest",
    "handlePB_SignalsReset",
    "RefreshUI",
    "onTimeout"
);
#else  // !QT_MOC_HAS_STRING_DATA
struct qt_meta_stringdata_CLASSMainWindowENDCLASS_t {
    uint offsetsAndSizes[32];
    char stringdata0[11];
    char stringdata1[12];
    char stringdata2[1];
    char stringdata3[17];
    char stringdata4[20];
    char stringdata5[17];
    char stringdata6[18];
    char stringdata7[17];
    char stringdata8[18];
    char stringdata9[18];
    char stringdata10[19];
    char stringdata11[19];
    char stringdata12[19];
    char stringdata13[22];
    char stringdata14[10];
    char stringdata15[10];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(sizeof(qt_meta_stringdata_CLASSMainWindowENDCLASS_t::offsetsAndSizes) + ofs), len 
Q_CONSTINIT static const qt_meta_stringdata_CLASSMainWindowENDCLASS_t qt_meta_stringdata_CLASSMainWindowENDCLASS = {
    {
        QT_MOC_LITERAL(0, 10),  // "MainWindow"
        QT_MOC_LITERAL(11, 11),  // "update_data"
        QT_MOC_LITERAL(23, 0),  // ""
        QT_MOC_LITERAL(24, 16),  // "handlePB_Connect"
        QT_MOC_LITERAL(41, 19),  // "handlePB_Disconnect"
        QT_MOC_LITERAL(61, 16),  // "handlePBC_ContOn"
        QT_MOC_LITERAL(78, 17),  // "handlePBC_ContOff"
        QT_MOC_LITERAL(96, 16),  // "handlePBI_IntfOn"
        QT_MOC_LITERAL(113, 17),  // "handlePBI_IntfOff"
        QT_MOC_LITERAL(131, 17),  // "handlePB_TesterOn"
        QT_MOC_LITERAL(149, 18),  // "handlePB_TesterOff"
        QT_MOC_LITERAL(168, 18),  // "handlePBI_IntfTest"
        QT_MOC_LITERAL(187, 18),  // "handlePBC_ContTest"
        QT_MOC_LITERAL(206, 21),  // "handlePB_SignalsReset"
        QT_MOC_LITERAL(228, 9),  // "RefreshUI"
        QT_MOC_LITERAL(238, 9)   // "onTimeout"
    },
    "MainWindow",
    "update_data",
    "",
    "handlePB_Connect",
    "handlePB_Disconnect",
    "handlePBC_ContOn",
    "handlePBC_ContOff",
    "handlePBI_IntfOn",
    "handlePBI_IntfOff",
    "handlePB_TesterOn",
    "handlePB_TesterOff",
    "handlePBI_IntfTest",
    "handlePBC_ContTest",
    "handlePB_SignalsReset",
    "RefreshUI",
    "onTimeout"
};
#undef QT_MOC_LITERAL
#endif // !QT_MOC_HAS_STRING_DATA
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_CLASSMainWindowENDCLASS[] = {

 // content:
      11,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    0,   98,    2, 0x08,    1 /* Private */,
       3,    0,   99,    2, 0x08,    2 /* Private */,
       4,    0,  100,    2, 0x08,    3 /* Private */,
       5,    0,  101,    2, 0x08,    4 /* Private */,
       6,    0,  102,    2, 0x08,    5 /* Private */,
       7,    0,  103,    2, 0x08,    6 /* Private */,
       8,    0,  104,    2, 0x08,    7 /* Private */,
       9,    0,  105,    2, 0x08,    8 /* Private */,
      10,    0,  106,    2, 0x08,    9 /* Private */,
      11,    0,  107,    2, 0x08,   10 /* Private */,
      12,    0,  108,    2, 0x08,   11 /* Private */,
      13,    0,  109,    2, 0x08,   12 /* Private */,
      14,    0,  110,    2, 0x08,   13 /* Private */,
      15,    0,  111,    2, 0x08,   14 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

Q_CONSTINIT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_CLASSMainWindowENDCLASS.offsetsAndSizes,
    qt_meta_data_CLASSMainWindowENDCLASS,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_CLASSMainWindowENDCLASS_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<MainWindow, std::true_type>,
        // method 'update_data'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'handlePB_Connect'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'handlePB_Disconnect'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'handlePBC_ContOn'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'handlePBC_ContOff'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'handlePBI_IntfOn'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'handlePBI_IntfOff'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'handlePB_TesterOn'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'handlePB_TesterOff'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'handlePBI_IntfTest'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'handlePBC_ContTest'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'handlePB_SignalsReset'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'RefreshUI'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'onTimeout'
        QtPrivate::TypeAndForceComplete<void, std::false_type>
    >,
    nullptr
} };

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->update_data(); break;
        case 1: _t->handlePB_Connect(); break;
        case 2: _t->handlePB_Disconnect(); break;
        case 3: _t->handlePBC_ContOn(); break;
        case 4: _t->handlePBC_ContOff(); break;
        case 5: _t->handlePBI_IntfOn(); break;
        case 6: _t->handlePBI_IntfOff(); break;
        case 7: _t->handlePB_TesterOn(); break;
        case 8: _t->handlePB_TesterOff(); break;
        case 9: _t->handlePBI_IntfTest(); break;
        case 10: _t->handlePBC_ContTest(); break;
        case 11: _t->handlePB_SignalsReset(); break;
        case 12: _t->RefreshUI(); break;
        case 13: _t->onTimeout(); break;
        default: ;
        }
    }
    (void)_a;
}

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CLASSMainWindowENDCLASS.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 14;
    }
    return _id;
}
QT_WARNING_POP
